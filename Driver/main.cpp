#include <iostream>
#include <errno.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

using namespace std;

static int xioctl(int fd, int request, void *arg)
{
	int r;

	int retry = 0;
	do {
		r = ioctl(fd, request, arg);
		if (-1 == r) {
			sleep(1);
		}
		retry += 1;
	}
	while (-1 == r && (EINTR == errno || retry < 5));

	return r;
}

int print_caps(int fd)
{
	struct v4l2_capability caps = {};
	if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &caps))
	{
		perror("Querying Capabilities");
		return 1;
	}

	printf("Driver Caps:\n"
		"  Driver: \"%s\"\n"
		"  Card: \"%s\"\n"
		"  Bus: \"%s\"\n"
		"  Version: %d.%d\n"
		"  Capabilities: %08x\n",
		caps.driver,
		caps.card,
		caps.bus_info,
		(caps.version >> 16) && 0xff,
		(caps.version >> 24) && 0xff,
		caps.capabilities);


	struct v4l2_cropcap cropcap = { 0 };
	cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == xioctl(fd, VIDIOC_CROPCAP, &cropcap))
	{
		perror("Querying Cropping Capabilities");
		return 2;
	}

	printf("Camera Cropping:\n"
		"  Bounds: %dx%d+%d+%d\n"
		"  Default: %dx%d+%d+%d\n"
		"  Aspect: %d/%d\n",
		cropcap.bounds.width, cropcap.bounds.height, cropcap.bounds.left, cropcap.bounds.top,
		cropcap.defrect.width, cropcap.defrect.height, cropcap.defrect.left, cropcap.defrect.top,
		cropcap.pixelaspect.numerator, cropcap.pixelaspect.denominator);

	int support_grbg10 = 0;

	struct v4l2_fmtdesc fmtdesc = { 0 };
	fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	char fourcc[5] = { 0 };
	char c, e;
	printf("  FMT : CE Desc\n--------------------\n");
	while (0 == xioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc))
	{
		strncpy(fourcc, (char *)&fmtdesc.pixelformat, 4);
		if (fmtdesc.pixelformat == V4L2_PIX_FMT_SGRBG10)
			support_grbg10 = 1;
		c = fmtdesc.flags & 1 ? 'C' : ' ';
		e = fmtdesc.flags & 2 ? 'E' : ' ';
		printf("  %s: %c%c %s\n", fourcc, c, e, fmtdesc.description);
		fmtdesc.index++;
	}

	if (!support_grbg10)
	{
		printf("Doesn't support GRBG10.\n");
	}
}

struct Cam
{
	int fd;
	void * buffer;
	struct v4l2_buffer buf = { 0 };
};

#include <vector>
std::vector<Cam> cams;

extern "C" {
	int open_cam(char *file, int width, int height);
	void* get_cam_buf(int handle);
	int get_cam_buf_len(int handle);
	int capture_cam(int handle);
	int close_cam(int handle);
	int set_ctrl(int handle, int id, int value);
	void scan(char *data, int stride, int width, int height);
	int scanResX();
	int scanResY();
}

int _scanRes[2];

int scanResX()
{
	return _scanRes[0];
}

int scanResY()
{
	return _scanRes[1];
}

void scan(char *data, int stride, int width, int height)
{
	_scanRes[0] = -1;
	_scanRes[1] = -1;

	for (int i = height - 1; i >= 0; i--)
	{
		for (int j = width - 1; j >= 0; j--)
		{
			if (data[i*stride+j] != 0) {
				for (int k = i - 3; k >= 0; k--)
				{
					for (int l = width - 1; l >= 0; l--)
					{
						if (data[k*stride + l] != 0) {
							_scanRes[0] = l;
							_scanRes[1] = k;
							return;
						}
					}
				}

			}
		}
	}
}

int open_cam(char *file, int width, int height)
{
	Cam cam;
	cam.fd = open(file, O_RDWR);

	struct v4l2_format fmt = { 0 };
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width = width;
	fmt.fmt.pix.height = height;
	fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_BGR24;
	fmt.fmt.pix.field = V4L2_FIELD_NONE;

	if (-1 == xioctl(cam.fd, VIDIOC_S_FMT, &fmt))
	{
		perror("Setting Pixel Format");
		return -1;
	}

	printf("\n\n");
	printf("Pixel format is set\n");


	struct v4l2_requestbuffers req = { 0 };
	req.count = 1;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;

	if (-1 == xioctl(cam.fd, VIDIOC_REQBUFS, &req))
	{
		perror("Requesting Buffer");
		return -1;
	}

	cam.buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	cam.buf.memory = V4L2_MEMORY_MMAP;
	cam.buf.index = 0;
	if (-1 == xioctl(cam.fd, VIDIOC_QUERYBUF, &cam.buf))
	{
		perror("Querying Buffer");
		return -1;
	}

	cam.buffer = mmap(NULL, cam.buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, cam.fd, cam.buf.m.offset);
	cams.push_back(cam);
	return cams.size() - 1;
}

int set_ctrl(int handle, int id, int value)
{
	Cam cam = cams[handle];

	v4l2_control ctl;
	memset(&ctl, 0, sizeof(ctl));
	ctl.id = id;
	ctl.value = value;

	if (-1 == xioctl(cam.fd, VIDIOC_S_CTRL, &ctl)) {
		perror("Set Control");
		return -1;
		//exit(-1);
	}
	return 0;
}

int close_cam(int handle)
{
	Cam cam = cams[handle];

	/*struct v4l2_buffer buf = { 0 };
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	buf.index = 0;
	if (-1 == xioctl(cam.fd, VIDIOC_QBUF, &buf))
	{
		perror("Stop Capture: Query Buffer");
		return 1;
	}

	if (-1 == xioctl(cam.fd, VIDIOC_STREAMOFF, &buf.type))
	{
		perror("Stop Capture");
		return 1;
	}

	struct v4l2_requestbuffers req = { 0 };
	req.count = 0;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;

	if (-1 == xioctl(cam.fd, VIDIOC_REQBUFS, &req))
	{
		perror("Releasing Buffer");
		return -1;
	}*/
	if (0 != munmap(cam.buffer, cam.buf.length)) {
		perror("Unmapping buffer\n");
		return -1;
	}

	close(cam.fd);
	printf("Camera closed\n");
	return 0;
}

void* get_cam_buf(int handle)
{
	return cams[handle].buffer;
}

int get_cam_buf_len(int handle)
{
	return cams[handle].buf.length;
}

int capture_cam(int handle)
{
	Cam cam = cams[handle];
	struct v4l2_buffer buf = { 0 };
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	buf.index = 0;
	if (-1 == xioctl(cam.fd, VIDIOC_QBUF, &buf))
	{
		perror("Query Buffer");
		return 1;
	}
	
	if (-1 == xioctl(cam.fd, VIDIOC_STREAMON, &buf.type))
	{
		perror("Start Capture");
		return 1;
	}

	printf("Wait for frame\n");
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(cam.fd, &fds);
	struct timeval tv = { 0 };
	tv.tv_sec = 2;
	int r = select(cam.fd+1, &fds, NULL, NULL, &tv);
	if (-1 == r)
	{
		perror("Waiting for Frame");
		return 1;
	}

	if (-1 == xioctl(cam.fd, VIDIOC_DQBUF, &buf))
	{
		perror("Retrieving Frame");
		return 1;
	}
	printf("Frame received\n");
}

void dump_buffer(void *buffer)
{
	for (int i = 0; i < 20; i++) {
		printf("%02x ", ((char*)buffer)[i]);
	}
	printf("\n");
}

int main(int argc, char **argv)
{
	int hcam = open_cam("/dev/video0", 320, 240);
	dump_buffer(get_cam_buf(hcam));
	while (1) {
		capture_cam(hcam);
		dump_buffer(get_cam_buf(hcam));
	}
	return 0;
}