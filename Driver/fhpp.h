#pragma once

/* CCON Register Bit Masks */
#define ENABLE			(1<<0)
#define STOP			(1<<1)
#define BRAKE   		(1<<2)
#define RESET   		(1<<3)
#define LOCK    		(1<<5)
#define OPM_MASK   		(3<<6)
#define OPM_RECSEL   	(0<<6)
#define OPM_DIRECT   	(1<<6)

/* CPOS Register Bit Masks */
#define HALT            (1<<0)
#define START           (1<<1)
#define HOM             (1<<2)
#define JOGP            (1<<3)
#define JOGN            (1<<4)
#define TEACH           (1<<5)
#define CLEAR           (1<<6)

/* CDIR Register Bit Masks */
#define ABS             (1<<0)
#define COM1            (1<<1)
#define COM2            (1<<2)
#define CONT            (1<<3)
#define CTOG            (1<<4)
#define XLIM            (1<<5)
#define FAST            (1<<6)
#define FUNC            (1<<7)

/* SPOS Register Bit Masks */
#define ACK           (1<<1)
#define MC             (1<<2)
