#pragma once

extern "C" 
{
	int SpiOpenPort(int spi_device, int speed_hz);

	int SpiClosePort(int spi_device);

	int SpiWriteAndRead(int spi_device, unsigned char * data, int length);
}
