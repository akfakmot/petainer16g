from machine import Binary
from wiringPiIO import WpiBinaryInput, WpiBinaryOutput
from enum import Enum


class WpiBinaryType(Enum):
    Input = 0
    Output = 1


class WpiBinaryPinConfig:
    def __init__(self, type=WpiBinaryType.Input, pin=None, invert=False, pullup=False, description=None):
        self.type = type
        self.pin = pin
        self.invert = invert
        self.pullup = pullup
        self.description = description

    def serialize(self):
        graph = '%s %s' % (self.type.name.upper(), self.pin)
        if self.invert: graph += ' INVERTED'
        if self.pullup: graph += ' PULLUP'
        if self.description is not None:
            graph += ' "' + self.description
        return graph

    def deserialize(self, graph):
        fields = graph.split(' ')
        self.type = WpiBinaryType[fields[0][0].upper() + fields[0][1:].lower()]
        self.pin = int(fields[1])
        
        for i in range(2,len(fields)):
            field = fields[i]
            if field.startswith('"'):
                self.description = ' '.join(fields[i:])[1:]
                break
            elif field.upper() == 'INVERTED': self.invert = True
            elif field.upper() == 'PULLUP': self.pullup = True


class WpiBinaryConfig:
    def __init__(self, definitions=dict()):
        assert all(isinstance(d[0], str) and isinstance(d[1], WpiBinaryPinConfig) for d in definitions.items())
        self.definitions = definitions
    
    def serialize(self):
        graph = ''
        for d in sorted(self.definitions.items(), key=lambda x:x[0]):
            graph += d[0] + ' ' + d[1].serialize() + '\n'
        return graph

    def deserialize(self, graph):
        lines = graph.split('\n')
        for line in lines:
            if len(line.strip()) == 0: continue
            fields = line.split(' ')
            config = WpiBinaryPinConfig()
            config.deserialize(' '.join(fields[1:]))
            self.definitions[fields[0]] = config


class WpiBinary(Binary):
    def __init__(self, config):
        super().__init__()
        self._config = config
        self._inputs = dict()
        self._outputs = dict()

    def setup(self):
        for pin in self._config.definitions.items():
            if pin[1].type == WpiBinaryType.Output:
                device = WpiBinaryOutput(pin[1].pin, pin[1].invert, pin[1].description)
                device.setup()
                self._outputs[pin[0]] = device
            elif pin[1].type == WpiBinaryType.Input:
                device = WpiBinaryInput(pin[1].pin, pin[1].invert, pin[1].pullup, pin[1].description)
                device.setup()
                self._inputs[pin[0]] = device
            else:
                raise NotImplementedError()

        super().setup()
                

    def setupFinished(self):
        return True

    def output(self, name):
        return self._outputs[name]

    def input(self, name):
        return self._inputs[name]
    
    def outputList(self):
        return self._outputs.keys()

    def inputList(self):
        return self._inputs.keys()

