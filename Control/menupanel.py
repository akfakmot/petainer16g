from autologging import *
from MenuPanelView import MenuPanelView
from observablecollections.observablelist import ObservableList
from panel import Panel


@traced
@logged
class MenuItem:
    def __init__(self, title=None, action=None):
        self.title = title
        self.action = action


@traced
@logged
class MenuPanelViewModel:
    def __init__(self):
        self.items = ObservableList()
        self.itemSelected = None

@traced
@logged
class MenuPanel(Panel):
    
    def __init__(self):
        super().__init__()
        self._items = []
        self._viewModel = MenuPanelViewModel()
        self._viewModel.itemSelected = self.itemSelected
        self._view = MenuPanelView()
        self._view.setModel(self._viewModel)

    @property
    def items(self):
        return self._items
    
    @items.setter
    def items(self, value):
        assert all(isinstance(item, MenuItem) for item in value), 'items must be a sequence of MenuItem'

        self._items = value
        self._viewModel.items.clear()
        self._viewModel.items.extend(item.title for item in value)
        
    def view(self):
        return self._view

    def itemSelected(self, i):
        if self._items[i].action is not None:
            self._items[i].action()
