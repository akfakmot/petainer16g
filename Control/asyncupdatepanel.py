from cancellationtoken import *
from PyQt5 import QtCore
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from threading import *
from panel import Panel

class AsyncUpdatePanel(Panel):

    _updateSignal = pyqtSignal(tuple, dict)
    _updateSignalLater = pyqtSignal(tuple, dict)

    def __init__(self):
        super().__init__()
        self.loops = []
        self.cancellationTokens = []
        self._threads = []
        self._updateSignal.connect(self._updateSlot, type=Qt.BlockingQueuedConnection)
        self._updateSignalLater.connect(self._updateSlotLater, type=Qt.QueuedConnection)

    def invoke(self, method, *args, **kwargs):
        print('invoke', method)
        self._updateMethod = method
        self._updateSignal.emit(args, kwargs)
        #QMetaObject.invokeMethod(self, '_invokeUpdate', Qt.BlockingQueuedConnection, Q_ARG(QList, args), Q_ARG(QMap, kwargs))

    def invokeLater(self, method, *args, **kwargs):
        print('invoke', method)
        self._updateMethodLater = method
        self._updateSignalLater.emit(args, kwargs)

    def supressableThread(self, cancellationToken, loopBodyIndex):
        while not cancellationToken.isCancelled():
            self.loops[loopBodyIndex]()

    def addLoop(self, loopBody):
        print('adding loop to', self.loops)
        self.loops.append(loopBody)

    def _loop(self, loopBody, cancellationToken):
        while not cancellationToken.isCancelled():
            print('loop body entry')
            loopBody()
            print('loop body finished')

        print('loop cancelled')

    def activate(self):
        if self.activated:
            return

        print('........................')
        print('........................')
        print('........................')
        print('........................')
        print('........................')
        print('........................')
        print('........................')
        print('........................')
        print('........................')
        print('ASYNCUPDATEPANEL.activate',self)
        
        super().activate()
        self.cancellationTokens.clear()
        self._threads.clear()

        for loop in self.loops:
            token = CancellationToken()
            self.cancellationTokens.append(token)
            thread = Thread(target=self._loop, args=(loop, token))
            self._threads.append(thread)
            thread.start()
        self.activated = True
    
    def deactivate(self):
        print('deactivating')
        for token in self.cancellationTokens:
            token.cancel()
        self.activated = False
    @pyqtSlot(tuple, dict)
    def _updateSlot(self, args, kwargs):
        self._updateMethod(*args, **kwargs)

    @pyqtSlot(tuple, dict)
    def _updateSlotLater(self, args, kwargs):
        self._updateMethodLater(*args, **kwargs)
        
    


