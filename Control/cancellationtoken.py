from threading import Lock


class CancellationToken(object):
    def __init__(self):
        self.lock = Lock()
        self.value = False
    
    def cancel(self):
        with self.lock:
            self.value = True

    def isCancelled(self):
        with self.lock:
            return self.value



