import sys
import argparse
import logging
from logargument import logLevelArg, logLevelChoice
from _version import __description__, __version__

""" Print metadata """
print(__description__)
print('v%s' % __version__)

""" Parse arguments """
argparser = argparse.ArgumentParser(description='TCELE Petainer Control Software')
logLevelArg(argparser)
argparser.add_argument('-g', '--debug', default=False, action='store_true', help='Run with ptvsd debug server and wait for debugger attach')
argparser.add_argument('-s', '--scratch', default=False, action='store_true', help='After bootstrap, import scratch.py')
args = argparser.parse_args()

""" Setup logging """
logging.basicConfig(level=logLevelChoice(args), format="%(levelname)s: %(name)s::%(funcName)s: %(message)s", stream=sys.stdout)
logger = logging.getLogger(__name__)

""" Debug server """
if args.debug:
    logger.info('Running with ptvsd debug server')
    import ptvsd
    ptvsd.enable_attach('my_secret')
    logger.info('Waiting for attach ...')
    for handler in logger.handlers:
        handler.flush()
    ptvsd.wait_for_attach()
    print('Attached')

""" Application """
if not args.scratch:
    from PyQt5.QtWidgets import *
    qapp = QApplication(sys.argv)

    from containers import Application
    Application.exec()
else:
    import scratch