import jsonpickle
import json

class JSONSerializer:
    def serialize(self, obj :object) -> bytes:
        return self.prettify(jsonpickle.dumps(obj)).encode('utf8')

    def deserialize(self, graph :bytes) -> object:
        return jsonpickle.loads(graph.decode('utf8'))

    def prettify(self, jsonText :str, indent :int =4) -> str:
        return json.dumps(json.loads(jsonText), indent=indent)


