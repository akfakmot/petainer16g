from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from asyncupdatepanel import AsyncUpdatePanel
from widgets.choice import *
from quantity import *
import jsonpickle
import copy
from measurebottle import *
from threading import *
from autologging import *
import exportdb
import os


m = jsonpickle.loads(r'{"perpend": {"_name": null, "_value": 0, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}, "rim": {"_samples": [{"py/tuple": [0, {"_name": null, "_value": 2217, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [4, {"_name": null, "_value": 2216, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [8, {"_name": null, "_value": 2215, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [12, {"_name": null, "_value": 2215, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [16, {"_name": null, "_value": 2214, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [20, {"_name": null, "_value": 2213, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [24, {"_name": null, "_value": 2212, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [28, {"_name": null, "_value": 2211, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [32, {"_name": null, "_value": 2211, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [36, {"_name": null, "_value": 2210, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [40, {"_name": null, "_value": 2209, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [44, {"_name": null, "_value": 2210, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [48, {"_name": null, "_value": 2209, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [52, {"_name": null, "_value": 2208, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [56, {"_name": null, "_value": 2206, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [60, {"_name": null, "_value": 2205, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [64, {"_name": null, "_value": 2203, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [68, {"_name": null, "_value": 2197, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [72, {"_name": null, "_value": 2195, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [76, {"_name": null, "_value": 2194, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [80, {"_name": null, "_value": 2193, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [84, {"_name": null, "_value": 2191, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [88, {"_name": null, "_value": 2189, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [92, {"_name": null, "_value": 2187, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [96, {"_name": null, "_value": 2185, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [100, {"_name": null, "_value": 2183, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [104, {"_name": null, "_value": 2179, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [108, {"_name": null, "_value": 2176, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [112, {"_name": null, "_value": 2173, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [116, {"_name": null, "_value": 2171, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [120, {"_name": null, "_value": 2169, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [124, {"_name": null, "_value": 2167, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [128, {"_name": null, "_value": 2165, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [132, {"_name": null, "_value": 2163, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [136, {"_name": null, "_value": 2161, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [140, {"_name": null, "_value": 2159, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [144, {"_name": null, "_value": 2158, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [148, {"_name": null, "_value": 2157, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [152, {"_name": null, "_value": 2156, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [156, {"_name": null, "_value": 2155, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [160, {"_name": null, "_value": 2154, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [164, {"_name": null, "_value": 2153, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [168, {"_name": null, "_value": 2152, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [172, {"_name": null, "_value": 2151, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [176, {"_name": null, "_value": 2149, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [180, {"_name": null, "_value": 2147, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [184, {"_name": null, "_value": 2146, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [188, {"_name": null, "_value": 2146, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [192, {"_name": null, "_value": 2146, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [196, {"_name": null, "_value": 2146, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [200, {"_name": null, "_value": 2146, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [204, {"_name": null, "_value": 2146, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [208, {"_name": null, "_value": 2146, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [212, {"_name": null, "_value": 2147, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [216, {"_name": null, "_value": 2149, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [220, {"_name": null, "_value": 2150, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [224, {"_name": null, "_value": 2152, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [228, {"_name": null, "_value": 2155, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [232, {"_name": null, "_value": 2156, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [236, {"_name": null, "_value": 2158, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [240, {"_name": null, "_value": 2159, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [244, {"_name": null, "_value": 2161, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [248, {"_name": null, "_value": 2163, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [252, {"_name": null, "_value": 2166, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [256, {"_name": null, "_value": 2168, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [260, {"_name": null, "_value": 2170, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [264, {"_name": null, "_value": 2173, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [268, {"_name": null, "_value": 2175, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [272, {"_name": null, "_value": 2178, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [276, {"_name": null, "_value": 2180, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [280, {"_name": null, "_value": 2183, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [284, {"_name": null, "_value": 2185, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [288, {"_name": null, "_value": 2188, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [292, {"_name": null, "_value": 2190, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [296, {"_name": null, "_value": 2192, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [300, {"_name": null, "_value": 2193, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [304, {"_name": null, "_value": 2195, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [308, {"_name": null, "_value": 2197, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [312, {"_name": null, "_value": 2199, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [316, {"_name": null, "_value": 2200, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [320, {"_name": null, "_value": 2202, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [324, {"_name": null, "_value": 2203, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [328, {"_name": null, "_value": 2204, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [332, {"_name": null, "_value": 2205, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [336, {"_name": null, "_value": 2206, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [340, {"_name": null, "_value": 2209, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [344, {"_name": null, "_value": 2217, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [348, {"_name": null, "_value": 2217, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [352, {"_name": null, "_value": 2218, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [356, {"_name": null, "_value": 2218, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}], "calibrateHeight": {"_name": null, "_value": 1000, "_unit": {"py/type": "quantity.Millimeter"}, "py/object": "quantity.QuantityLiteral"}, "py/object": "section.Rim"}, "sections": [{"_height": {"_name": null, "_value": 30.0, "_unit": {"py/type": "quantity.Millimeter"}, "py/object": "quantity.QuantityLiteral"}, "_samples": [{"py/tuple": [0, {"_name": null, "_value": 41873, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [4.0, {"_name": null, "_value": 41853, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [8.0, {"_name": null, "_value": 41822, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [12.0, {"_name": null, "_value": 41790, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [16.0, {"_name": null, "_value": 41736, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [20.0, {"_name": null, "_value": 41690, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [24.0, {"_name": null, "_value": 41616, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [28.0, {"_name": null, "_value": 41493, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [32.0, {"_name": null, "_value": 41430, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [36.0, {"_name": null, "_value": 41347, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [40.0, {"_name": null, "_value": 41259, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [44.0, {"_name": null, "_value": 41170, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [48.0, {"_name": null, "_value": 41065, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [52.0, {"_name": null, "_value": 41006, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [56.0, {"_name": null, "_value": 40933, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [60.0, {"_name": null, "_value": 40863, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [64.0, {"_name": null, "_value": 40798, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [68.0, {"_name": null, "_value": 40752, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [72.0, {"_name": null, "_value": 40664, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [76.0, {"_name": null, "_value": 40605, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [80.0, {"_name": null, "_value": 40558, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [84.0, {"_name": null, "_value": 40499, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [88.0, {"_name": null, "_value": 40440, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [92.0, {"_name": null, "_value": 40378, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [96.0, {"_name": null, "_value": 40275, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [100.0, {"_name": null, "_value": 40238, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [104.0, {"_name": null, "_value": 40165, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [108.0, {"_name": null, "_value": 40092, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [112.0, {"_name": null, "_value": 40039, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [116.0, {"_name": null, "_value": 39941, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [120.0, {"_name": null, "_value": 39898, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [124.0, {"_name": null, "_value": 39846, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [128.0, {"_name": null, "_value": 39796, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [132.0, {"_name": null, "_value": 39747, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [136.0, {"_name": null, "_value": 39700, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [140.0, {"_name": null, "_value": 39647, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [144.0, {"_name": null, "_value": 39608, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [148.0, {"_name": null, "_value": 39582, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [152.0, {"_name": null, "_value": 39546, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [156.0, {"_name": null, "_value": 39521, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [160.0, {"_name": null, "_value": 39504, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [164.0, {"_name": null, "_value": 39481, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [168.0, {"_name": null, "_value": 39469, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [172.0, {"_name": null, "_value": 39466, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [176.0, {"_name": null, "_value": 39470, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [180.0, {"_name": null, "_value": 39483, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [184.0, {"_name": null, "_value": 39506, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [188.0, {"_name": null, "_value": 39545, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [192.0, {"_name": null, "_value": 39573, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [196.0, {"_name": null, "_value": 39612, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [200.0, {"_name": null, "_value": 39644, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [204.0, {"_name": null, "_value": 39701, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [208.0, {"_name": null, "_value": 39752, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [212.0, {"_name": null, "_value": 39805, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [216.0, {"_name": null, "_value": 39859, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [220.0, {"_name": null, "_value": 39916, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [224.0, {"_name": null, "_value": 39976, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [228.0, {"_name": null, "_value": 40021, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [232.0, {"_name": null, "_value": 40095, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [236.0, {"_name": null, "_value": 40158, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [240.0, {"_name": null, "_value": 40228, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [244.0, {"_name": null, "_value": 40296, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [248.0, {"_name": null, "_value": 40363, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [252.0, {"_name": null, "_value": 40414, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [256.0, {"_name": null, "_value": 40499, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [260.0, {"_name": null, "_value": 40547, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [264.0, {"_name": null, "_value": 40624, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [268.0, {"_name": null, "_value": 40675, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [272.0, {"_name": null, "_value": 40767, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [276.0, {"_name": null, "_value": 40825, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [280.0, {"_name": null, "_value": 40926, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [284.0, {"_name": null, "_value": 41010, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [288.0, {"_name": null, "_value": 41094, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [292.0, {"_name": null, "_value": 41179, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [296.0, {"_name": null, "_value": 41262, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [300.0, {"_name": null, "_value": 41343, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [304.0, {"_name": null, "_value": 41424, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [308.0, {"_name": null, "_value": 41505, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [312.0, {"_name": null, "_value": 41581, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [316.0, {"_name": null, "_value": 41633, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [320.0, {"_name": null, "_value": 41694, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [324.0, {"_name": null, "_value": 41757, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [328.0, {"_name": null, "_value": 41797, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [332.0, {"_name": null, "_value": 41826, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [336.0, {"_name": null, "_value": 41842, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [340.0, {"_name": null, "_value": 41857, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [344.0, {"_name": null, "_value": 41865, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [348.0, {"_name": null, "_value": 41874, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [352.0, {"_name": null, "_value": 41871, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [356.0, {"_name": null, "_value": 41861, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}], "calibrateDiam": {"_name": null, "_value": 0.55, "_unit": {"py/type": "quantity.Millimeter"}, "py/object": "quantity.QuantityLiteral"}, "py/object": "section.Section"}, {"_height": {"_name": null, "_value": 150.0, "_unit": {"py/type": "quantity.Millimeter"}, "py/object": "quantity.QuantityLiteral"}, "_samples": [{"py/tuple": [0, {"_name": null, "_value": 41046, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [4.0, {"_name": null, "_value": 41030, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [8.0, {"_name": null, "_value": 41006, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [12.0, {"_name": null, "_value": 40968, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [16.0, {"_name": null, "_value": 40930, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [20.0, {"_name": null, "_value": 40845, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [24.0, {"_name": null, "_value": 40756, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [28.0, {"_name": null, "_value": 40646, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [32.0, {"_name": null, "_value": 40519, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [36.0, {"_name": null, "_value": 40380, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [40.0, {"_name": null, "_value": 40271, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [44.0, {"_name": null, "_value": 40058, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [48.0, {"_name": null, "_value": 39966, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [52.0, {"_name": null, "_value": 39865, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [56.0, {"_name": null, "_value": 39793, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [60.0, {"_name": null, "_value": 39746, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [64.0, {"_name": null, "_value": 39720, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [68.0, {"_name": null, "_value": 39710, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [72.0, {"_name": null, "_value": 39708, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [76.0, {"_name": null, "_value": 39705, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [80.0, {"_name": null, "_value": 39694, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [84.0, {"_name": null, "_value": 39681, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [88.0, {"_name": null, "_value": 39649, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [92.0, {"_name": null, "_value": 39608, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [96.0, {"_name": null, "_value": 39555, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [100.0, {"_name": null, "_value": 39507, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [104.0, {"_name": null, "_value": 39413, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [108.0, {"_name": null, "_value": 39349, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [112.0, {"_name": null, "_value": 39229, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [116.0, {"_name": null, "_value": 39129, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [120.0, {"_name": null, "_value": 39029, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [124.0, {"_name": null, "_value": 38930, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [128.0, {"_name": null, "_value": 38858, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [132.0, {"_name": null, "_value": 38764, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [136.0, {"_name": null, "_value": 38656, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [140.0, {"_name": null, "_value": 38575, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [144.0, {"_name": null, "_value": 38500, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [148.0, {"_name": null, "_value": 38449, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [152.0, {"_name": null, "_value": 38379, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [156.0, {"_name": null, "_value": 38347, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [160.0, {"_name": null, "_value": 38306, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [164.0, {"_name": null, "_value": 38292, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [168.0, {"_name": null, "_value": 38289, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [172.0, {"_name": null, "_value": 38293, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [176.0, {"_name": null, "_value": 38305, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [180.0, {"_name": null, "_value": 38330, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [184.0, {"_name": null, "_value": 38357, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [188.0, {"_name": null, "_value": 38392, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [192.0, {"_name": null, "_value": 38422, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [196.0, {"_name": null, "_value": 38463, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [200.0, {"_name": null, "_value": 38502, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [204.0, {"_name": null, "_value": 38550, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [208.0, {"_name": null, "_value": 38589, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [212.0, {"_name": null, "_value": 38617, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [216.0, {"_name": null, "_value": 38664, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [220.0, {"_name": null, "_value": 38694, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [224.0, {"_name": null, "_value": 38740, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [228.0, {"_name": null, "_value": 38799, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [232.0, {"_name": null, "_value": 38836, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [236.0, {"_name": null, "_value": 38888, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [240.0, {"_name": null, "_value": 38943, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [244.0, {"_name": null, "_value": 38984, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [248.0, {"_name": null, "_value": 39041, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [252.0, {"_name": null, "_value": 39117, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [256.0, {"_name": null, "_value": 39185, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [260.0, {"_name": null, "_value": 39241, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [264.0, {"_name": null, "_value": 39320, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [268.0, {"_name": null, "_value": 39405, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [272.0, {"_name": null, "_value": 39537, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [276.0, {"_name": null, "_value": 39603, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [280.0, {"_name": null, "_value": 39690, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [284.0, {"_name": null, "_value": 39775, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [288.0, {"_name": null, "_value": 39841, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [292.0, {"_name": null, "_value": 39937, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [296.0, {"_name": null, "_value": 40061, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [300.0, {"_name": null, "_value": 40162, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [304.0, {"_name": null, "_value": 40258, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [308.0, {"_name": null, "_value": 40350, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [312.0, {"_name": null, "_value": 40434, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [316.0, {"_name": null, "_value": 40506, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [320.0, {"_name": null, "_value": 40572, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [324.0, {"_name": null, "_value": 40634, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [328.0, {"_name": null, "_value": 40696, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [332.0, {"_name": null, "_value": 40755, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [336.0, {"_name": null, "_value": 40809, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [340.0, {"_name": null, "_value": 40858, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [344.0, {"_name": null, "_value": 40899, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [348.0, {"_name": null, "_value": 40932, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [352.0, {"_name": null, "_value": 40950, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}, {"py/tuple": [356.0, {"_name": null, "_value": 40961, "_unit": {"py/type": "quantity.Micrometer"}, "py/object": "quantity.QuantityLiteral"}]}], "calibrateDiam": {"_name": null, "_value": 0.55, "_unit": {"py/type": "quantity.Millimeter"}, "py/object": "quantity.QuantityLiteral"}, "py/object": "section.Section"}]}')

class ProductionPanel(AsyncUpdatePanel):
    usesMachine = True

    def __init__(self, hw, session, filesystem, prototype_repository, config_manager, config_id):
        super().__init__()
        self.hw = hw
        self.session = session
        self.filesystem = filesystem
        self.prototype_repository = prototype_repository
        self.config_manager = config_manager
        self.config_id = config_id
        self.table = QTableWidget()
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.table.horizontalHeader().hide()
        self.table.verticalHeader().hide()
        self.table.setFrameStyle(QFrame.NoFrame)
        
        l = QVBoxLayout()
        l.setContentsMargins(0,0,0,0)
        l.setSpacing(0)
        self.setLayout(l)
        frame = QHBoxLayout()
        self.clearButton = QPushButton(text='Nová série')
        self.clearButton.clicked.connect(self.clearSerie)
        self.decrementButton = QPushButton(text='Smazat poslední\nměření')
        self.decrementButton.clicked.connect(self.decrementSerie)
        topLayout = QHBoxLayout()
        self.typeLabel = QLabel()
        self.typeLabel.setAlignment(Qt.AlignLeft | Qt.AlignCenter)
        self.typeLabel.setContentsMargins(10,10,10,10)
        topLayout.addWidget(self.typeLabel)
        topLayout.addWidget(self.clearButton)
        topLayout.addWidget(self.decrementButton)
        #topLayout.addItem(QSpacerItem(5,5,QSizePolicy.Expanding, QSizePolicy.Minimum))
        self.timeLabel = QLabel()
        self.timeLabel.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self.timeLabel.setAlignment(Qt.AlignRight | Qt.AlignCenter)
        self.timeLabel.setContentsMargins(10,10,10,10)
        topLayout.addWidget(self.timeLabel)

        for button in (self.clearButton, self.decrementButton,):
            button.setMinimumSize(150,50)
        #topLayout.addItem(QSpacerItem(0,0,QSizePolicy.Expanding,QSizePolicy.Minimum))
        self.layout().addLayout(topLayout, 0)
        self.layout().addLayout(frame, 1)
        btmLayout = QHBoxLayout()
        self.status = QLabel()
        self.status.setFrameShape(QFrame.Panel)
        self.status.setFrameShadow(QFrame.Sunken)

        btmLayout.addWidget(self.status, 1)
        self.measureButton = QPushButton(text='Měřit')
        self.measureButton.setMinimumSize(150,50)
        self.measureButton.clicked.connect(self.measureButton_clicked)
        self.measuring = False
        btmLayout.addWidget(self.measureButton, 0)
        self.layout().addLayout(btmLayout)

        for button in (self.clearButton, self.decrementButton,):
            button.setMinimumSize(150,60)
        
        self.layout().addLayout(btmLayout, 0)

        self.avgTable = QTableWidget()
        self.avgTable.horizontalHeader().hide()
        self.avgTable.verticalHeader().hide()
        self.avgTable.setFrameStyle(QFrame.NoFrame)
        self.avgTable.setFixedWidth(80)
        self.leftTable = QTableWidget()
        self.leftTable.horizontalHeader().hide()
        self.leftTable.verticalHeader().hide()
        self.leftTable.setFrameStyle(QFrame.NoFrame)
        self.leftTable.setFixedWidth(100)
        sa = QScrollArea()
        sa.setFrameStyle(QFrame.NoFrame)
        #sa.move(100,0)
        #sa.resize(600,350)
        sa.setWidget(self.table)
        sa.verticalScrollBar().hide()
        sa.setStyleSheet('QScrollBar:horizontal {height: 60px}')
        self.table.horizontalScrollBar().hide()
        self.table.verticalScrollBar().hide()
        self.table.setParent(sa)

        self.leftTable.setStyleSheet('font: 10pt Tahoma')
        self.table.setStyleSheet('font: 10pt Tahoma')
        self.avgTable.setStyleSheet('font: 10pt Tahoma')

        #self.leftTable.move(0,0)
        #self.leftTable.resize(120,350)
        
        self.sa = sa
        frame.addWidget(self.leftTable)
        frame.addWidget(sa)
        frame.addWidget(self.avgTable)
        #self.avgTable.move(700,0)
        #self.avgTable.resize(100,350)
        #self.layout().addItem(QSpacerItem(0,0,QSizePolicy.Expanding, QSizePolicy.Minimum))
        #self.table.setRowCount(10)
        #self.table.setColumnCount(10)
        self.samples = []
        self.setStyleSheet('font: 12pt Tahoma')
        
        self.previewLabel = QLabel()
        self.previewLabel.setParent(self)
        self.previewLabel.move(0,0)
        self.previewLabel.resize(500,300)
        self.previewLabel.hide()
        
        self.timer = QTimer()
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.timer_timeout)

    def timer_timeout(self, *args, **kwargs):
        self.elapsedSeconds += 1
        self.timeLabel.setText('Doba měření: %d:%02d' % (self.elapsedSeconds / 60, self.elapsedSeconds % 60))

    def measureThread(self):
        def retreat(source='request'):
            self.hw.machine.binary.outputs.vacuum.clear()
            self.hw.machine.binary.outputs.hgauge.clear()
            self.measureButton.setText('Měřit')
            self.measuring = False
            if source == 'condition':
                self.navigator.messageBox('Měření bylo zrušeno. Během měření byla otevřena dvířka')
            elif source != 'request':
                self.navigator.messageBox(source)
            hwmonitor.abortCondition = lambda: False
            self.showPrompt()
            self.previewLabel.hide()

        try:
            def showImg(src):
                self.invoke(lambda: self.previewLabel.setPixmap(QPixmap.fromImage(QImage(src.data, src.shape[1], src.shape[0], src.strides[0], QImage.Format_RGB888))) )
                self.previewLabel.show()

            mb = MeasureBottle(self.hw, self.prototype, showImg, lambda: self.invoke(lambda: self.previewLabel.hide()))

            with hwmonitor.HardwareAbortContext(self.hw):
                self.invoke(lambda: self.status.setText('Zavřete dvířka'))
                while not self.hw.machine.binary.inputs.door.state():
                    hwmonitor.check()

                for i in range(60):    # door guard time
                    time.sleep(0.010)
                    hwmonitor.check()
    
                hwmonitor.abortCondition = lambda: not self.hw.machine.binary.inputs.door.state()
                self.invoke(self.timerStart)
                try:
                    measurement = mb.exec(lambda msg: self.invoke(lambda: self.status.setText('Probíhá měření ... ' + msg)))
                finally:
                    self.invoke(self.timerStop)

                if len(self.samples) >= self.config['batch']:
                    self.clearSerie()
                self.samples.append(measurement)
                if len(self.samples) == self.config['batch']:
                    self.actOnAverage()
                self.actOnNewMeasurement()
                self.invoke(lambda: self.refreshTable())
                self.invoke(retreat)
                
        except hwmonitor.HardwareAbortException as e:
            self.invoke(lambda: retreat(e.source))
        #except Exception as e:
        #    self.invoke(lambda: retreat(str(e)))
    
    def timerStart(self):
        self.elapsedSeconds = 0
        self.timer.start()

    def timerStop(self):
        self.timer.stop()
        self.timeLabel.setText('Doba posledního měření: %d:%02d' % (self.elapsedSeconds / 60, self.elapsedSeconds % 60))

    def actOnAverage(self):
        if not self.config['export.all']:
            recs = []
            for sample in self.samples:
                print('processing sample', sample)
                rec = dict(
                    operator=self.session.user and self.session.user.pin, 
                    station_name=self.config['stationName'],
                    program=self.prototype['name'], 
                    height=sample['rim'].max().amount(Millimeter), 
                    diams=[(s.averageDiam().amount(Millimeter), s.ovality().amount(Millimeter), s.height().amount(Millimeter)) for s in sample['sections']],
                    perpend=sample['perpend'].amount(Millimeter), 
                    slant=sample['rim'].finishSlant().amount(Millimeter))
                recs.append(rec)
            
            def average(seq):
                return sum(seq) / float(len(list(seq)))

            def averageTupleSeq(tupSeq):
                return tuple(average(list(tup[i] for tup in tupSeq)) for i in range(len(tupSeq[0])))

            print('recs',recs)
            print('recs-height', list(map(lambda x: x['height'], recs)))
            print('recs-height', average(list(map(lambda x: x['height'], recs))))

            rec = dict(
                    operator=self.session.user and self.session.user.pin, 
                    station_name=self.config['stationName'],
                    program=self.prototype['name'], 
                    height=average(list(map(lambda x: x['height'], recs))), 
                    diams=[averageTupleSeq(list(rec['diams'][i] for rec in recs)) for i in range(len(recs[0]['diams']))],
                    perpend=average(list(map(lambda x: x['perpend'], recs))), 
                    slant=average(list(map(lambda x: x['slant'], recs))))
            self.record(**rec)

    def actOnNewMeasurement(self):
        if self.config['export.all']:
            sample = self.samples[-1]
            rec = dict(
                operator=self.session.user and self.session.user.pin, 
                station_name=self.config['stationName'],
                program=self.prototype['name'], 
                height=sample['rim'].max().amount(Millimeter), 
                diams=[(s.averageDiam().amount(Millimeter), s.ovality().amount(Millimeter) if self.prototype['sections'][sample['sections'].index(s)]['oval'][2] is not None else None, s.height().amount(Millimeter)) for s in sample['sections']],
                perpend=sample['perpend'].amount(Millimeter), 
                slant=sample['rim'].finishSlant().amount(Millimeter))
            self.record(**rec)

    def record(self, operator, station_name, program, height, diams, perpend, slant):
        print(dict(operator=operator, station_name=station_name, program=program, height=height, diams=diams, perpend=perpend, slant=slant))
        #return
        try:
            if self.filesystem.exists('/mnt/hard/record.txt') and os.path.getsize('/mnt/hard/record.txt') >= 30000000:
                self.driver.message('Úložiště pro záznamy je plné. Smažte jej v menu adminstrátora')
            else:
                with open('/mnt/hard/record.txt', 'a') as f:
                    f.write('%.2f;' % height)
                    for i in range(0, len(diams)):
                        f.write('%.2f;' % diams[i][0])
                    for i in range(0, len(diams)):
                        if diams[i][1] is not None:
                            f.write('%.2f;' % diams[i][1])
                    f.write('%.2f;' % perpend)
                    f.write('%.2f;' % slant)
                    f.write('\n')

        except Exception as e:
            print('RECORD LOCAL FAILED')
            print(e)

        if self.config['export.on']:
            try:
                conn = exportdb.connection(self.config['export.server'], self.config['export.user'], self.config['export.password'])
                cursor = conn.cursor()
                cursor.execute('USE [%s];' % self.config['export.scheme'])
                cursor.execute(
                    'INSERT INTO RECORDS([DATE], OPERATOR, HEIGHT, FINISHSLANT, PERPEND, STATION, XLSFILE, XLSLIST, XLSHEIGHT, XLSFINISHSLANT, XLSPERPEND, PROGRAM) ' +
                    ' VALUES (GETDATE(),%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s); SELECT SCOPE_IDENTITY();',
                    (operator, height, slant, perpend, station_name, '', '', '',
                     '', '', program))
                record_id = int((cursor.fetchall()[0][0]))
                for i in range(0,len(diams)):
                    cursor.execute('INSERT INTO RADIUSES(RECORD, HEIGHT, DIAMETER, OVALITY, XLSRADIUS, XLSOVALITY) VALUES (%s,%s,%s,%s,%s,%s);',
                                   (record_id, diams[i][2], diams[i][0], diams[i][1], '', ''))
                conn.commit()
                conn.close()
            except Exception as e:
                print(e)
                self.invoke(lambda: self.navigator.messageBox('Nebylo možné zapsat výsledky do databáze. Zkontrolujte nastavení exportu'))

    def showPrompt(self):
        self.status.setText('Vložte vzorek č.{0}, stikněte Měřit a zavřete dvířka'.format(((len(self.samples)) % self.config['batch'])+1))

    def measureButton_clicked(self, *args, **kwargs):
        if not self.hw.calibrate_height(self.prototype['height'][1]):
            self.navigator.messageBox('Není k dispozici kalibrace výšky')
            return
        if not self.measuring:
            self.measureButton.setText('Zastavit')
            self.measuring = True
            Thread(target=self.measureThread).start()
        else:
            hwmonitor.abort()
        #self.samples.append(self.randomizeMeasurement(copy.deepcopy(m)))

    def clearSerie(self):
        self.samples.clear()
        self.refreshTable()

    def decrementSerie(self):
        if len(self.samples) != 0:
            self.samples.pop()
            self.refreshTable()

    def selectPrototype(self):
        c = [x.data for x in self.prototype_repository.all() if x.data['name'] == self.config['type']]
        if len(c) == 0:
            self.navigator.messageBox('Přednastavený typ není k dispozici')
            self.navigator.redirect('empty')
        else:
            self.prototype = c[0]
            self.typeLabel.setText(c[0]['name'])
            self.refreshTable()
        """list = ChoiceList()
        list.setItems(sorted(map(lambda x: x.data, self.prototype_repository.all()), key=lambda x:x['name']), lambda x: x['name'])
        list.itemSelected.connect(self.typeSelected)
        list.canceled.connect(lambda: self.navigator.unstack() or self.navigator.redirect('empty'))
        self.navigator.stack(list)
        """
    def horizontalHeaderItem(self, text):
        item = QTableWidgetItem(text)
        item.setBackground(QColor(255,255,255))
        return item

    def verticalHeaderItem(self, text):
        item = QTableWidgetItem(text)
        item.setBackground(QColor(255,255,255))
        return item

    def contentItem(self, num, range):
        item = QTableWidgetItem('%.2f' % num.amount(Millimeter))
        red = QColor(255,0,0)
        green = QColor(0,255,0)

        if range[0] and num < range[0]:
            item.setBackground(red)
        elif range[2] and num > range[2]:
            item.setBackground(red)
        else:
            item.setBackground(green)
        return item

    def refreshTable(self):
        self.showPrompt()
        self.table.setRowCount(0)
        
        headerItems = [
            QTableWidgetItem(), 
            self.horizontalHeaderItem('Height')
            ]
        for section in self.prototype['sections']:
            headerItems.append(self.horizontalHeaderItem('Diam %.2f' % section['height'].amount(Millimeter)))
        for section in self.prototype['sections']:
            if section['oval'][2] is not None:
                headerItems.append(self.horizontalHeaderItem('Oval %.2f' % section['height'].amount(Millimeter)))
        headerItems = headerItems + [self.horizontalHeaderItem(x) for x in ('Perpend', 'Finish slant',)]

        self.leftTable.setRowCount(0)
        self.leftTable.setRowCount(len(headerItems))
        self.leftTable.setColumnCount(1)

        self.table.setRowCount(len(headerItems))
        for i in range(len(headerItems)):
            self.leftTable.setItem(i,0,headerItems[i])
        
        self.table.setColumnCount(len(self.samples))
        for i in range(len(self.samples)):
            self.table.setItem(0,i,self.verticalHeaderItem(str(i+1)))
        
        self.avgTable.setRowCount(0)
        self.avgTable.setRowCount(len(headerItems))
        self.avgTable.setColumnCount(1)
        self.avgTable.setItem(0,0,self.verticalHeaderItem('AVG'))

        if len(self.samples) > 0:
            avg = dict()
            avg['height'] = 0
            avg['diams'] = [0] * len(self.prototype['sections'])
            avg['ovals'] = [0] * len(self.prototype['sections'])
            avg['perpend'] = 0
            avg['finishslant'] = 0

        for i in range(len(self.samples)):
            sample = self.samples[i]
            items = []
            height = sample['rim'].max()
            items.append( self.contentItem(height, self.prototype['height']) )
            avg['height'] += height
            
            for j in range(len(sample['sections'])):
                d = sample['sections'][j].averageDiam()
                o = sample['sections'][j].ovality()
                items.append( self.contentItem(d, self.prototype['sections'][j]['diam']) )
                if self.prototype['sections'][j]['oval'][2] is not None:
                    items.append( self.contentItem(o, self.prototype['sections'][j]['oval']) )
                avg['diams'][j] += d
                avg['ovals'][j] += o
            
            perpend = sample['perpend']
            items.append( self.contentItem(perpend, self.prototype['perpend']) )
            avg['perpend'] += perpend

            finishslant = sample['rim'].finishSlant()
            items.append( self.contentItem(finishslant, self.prototype['finishslant']) )
            avg['finishslant'] += finishslant

            for j in range(len(items)):
                self.table.setItem(1+j, i, items[j])
            #print('sample',i,'processed')

        if len(self.samples) > 0:
            items = []
            avg['height'] = avg['height'] / float(len(self.samples))
            items.append(self.contentItem(avg['height'], self.prototype['height']))
            for j in range(len(sample['sections'])):
                avg['diams'][j] = avg['diams'][j] / float(len(self.samples))
                items.append(self.contentItem(avg['diams'][j], self.prototype['sections'][j]['diam']))

                avg['ovals'][j] = avg['ovals'][j] / float(len(self.samples))
                if self.prototype['sections'][j]['oval'][2] is not None:
                    items.append(self.contentItem(avg['ovals'][j], self.prototype['sections'][j]['oval']))
            avg['perpend'] = avg['perpend'] / float(len(self.samples))
            items.append(self.contentItem(avg['perpend'], self.prototype['perpend']))
            avg['finishslant'] = avg['finishslant'] / float(len(self.samples))
            items.append(self.contentItem(avg['finishslant'], self.prototype['finishslant']))

            for i in range(len(items)):
                self.avgTable.setItem(1+i,0,items[i])

        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
        for i in range(self.table.columnCount()):
            self.table.setColumnWidth(i, 56)
        
        #self.sa.horizontalScrollBar().setValue(self.sa.horizontalScrollBar().minimum())
        width = self.table.columnCount() - 1 + self.table.verticalHeader().width()
        print(self.table.columnCount(), 'columns')
        for i in range(self.table.columnCount()):
            width = width + self.table.columnWidth(i)
        self.table.setFixedWidth(width)
        self.table.setMinimumHeight(1000)
        self.sa.horizontalScrollBar().setValue(self.sa.horizontalScrollBar().maximum())

    def randomizeMeasurement(self, m):
        import random
        offset = QuantityLiteral(random.uniform(-500,500), Millimeter)
        for i in range(len(m['rim'].samples())):
           m['rim'].samples()[i] = (m['rim'].samples()[i][0], m['rim'].samples()[i][1]+offset)
        return m

    def typeSelected(self, type):
        self.prototype = type
        self.navigator.unstack()
        self.refreshTable()
        #self.table.horizontalScrollBar().setValue(self.table.horizontalScrollBar().maximum())

    def activate(self):
        configGraph = self.config_manager.load(self.config_id)
        if configGraph is None:
            self.navigator.redirect('error', text='Není k dispozici konfigurace')
            return True
        self.config = jsonpickle.loads(configGraph.decode('utf8'))

        self.selectPrototype()