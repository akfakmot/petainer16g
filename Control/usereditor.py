from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from sectioneditor import *
from collectioneditor import *
from widgets.minmax import MinMax
from user import *
from numericinput import *


@loadUi('ui/usereditor.ui')
class UserEditor(QWidget):
    def __init__(self):
        super().__init__()
        self.pin = None

    def initUi(self):
        self.role.setItems(list(UserRole), lambda x: x.displayString())
        self.pinButton.clicked.connect(self.pinButton_clicked)
        
    def pinButton_clicked(self, *args, **kwargs):
        keyboard = NumericInput(hash_char=' ', star_char=' ')
        if keyboard.exec():
            self.pin = keyboard.value()
            self.pinLabel.setText('Zadáno')

    def fill(self, data):
        self.name.setText(data and data.name)
        self.pinLabel.setText('Zadáno' if (data and data.pin) else 'Není zadán')
        self.pin = data and data.pin
        self.pinButton.show()
        self.role.setSelected(data and data.role)

    def read(self):
        return User(name=self.name.text(), pin=self.pin, role=self.role.selected())

    def clear(self):
        self.name.setText(None)
        self.pinLabel.setText(None)
        self.pinButton.hide()
        self.role.setSelected(None)