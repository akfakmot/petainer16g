from quantity import *
import numpy as np
import cv2
import math
from utility import modulo


class Rim(object):
    def __init__(self, samples, calibrateHeight):
        self._samples = samples
        self.calibrateHeight = calibrateHeight

    def samples(self):
        return self._samples

    def max(self):
        return max((self.calibrateHeight+s[1]) for s in self._samples)

    def min(self):
        return min((self.calibrateHeight+s[1]) for s in self._samples)

    def finishSlant(self):
        return self.max() - self.min()

from cachedcalculation import *

class Section(object):
    def __init__(self, samples, calibrateDiam, height=None):
        self._samples = samples
        self.calibrateDiam = calibrateDiam
        self._height = height

    def samples(self):
        return self._samples

    def calibrate(self, raw):
        res = self.calibrateDiam.calibrate(self._height, raw)
        print('$$calibrating raw',raw,'to res',res)
        return res

    @cachedCalculation('diams')
    def diams(self):
        half = int(len(self._samples)/2)
        diams = []
        for i in range(half):
            #if abs(self._samples[i][0] - 90) >= 15:
            diams.append( (self._samples[i][0], self.calibrate(self._samples[i][1] + self._samples[i+half][1])) )
        diams = sorted(diams, key=lambda x: x[1].amount(Millimeter))[3:-3]
        diams = diams + list((x[0]+180.0, x[1]) for x in diams)
        return diams

    @cachedCalculation('averageDiam')
    def averageDiam(self):
        diams = list(x[1] for x in self.diams())
        print(sum(diams))
        return sum(diams) / float(len(diams))

    @cachedCalculation('ovality')
    def ovality(self):
        d = list(x[1] for x in self.diams())
        return max(d) - min(d)

    @cachedCalculation('center')
    def center(self):
        ctr = cv2.fitEllipse(self.pointsToContour(self.cartesianSamples()))[0]
        return (QuantityLiteral(ctr[0], Millimeter), QuantityLiteral(ctr[1], Millimeter))

    @cachedCalculation('centerPolar')
    def centerPolar(self):
        cart = tuple(x.amount(Millimeter) for x in self.center())
        mod = math.sqrt(cart[0] ** 2 + cart[1] ** 2)
        arg = math.atan2(cart[1], cart[0]) / math.pi * 180
        return (QuantityLiteral(mod, Millimeter), QuantityLiteral(arg, Degrees))        

    def diamAt(self, angle):
        d = list((x[0], x[1].amount(Millimeter)) for x in self.diams())
        d.append(d[0])
        return QuantityLiteral(modulo.interpolate(angle, d, 360), Millimeter)
    
    def sampleAt(self, angle):
        d = list((x[0], x[1].amount(Millimeter)) for x in self.samples())
        d.append(d[0])
        return QuantityLiteral(modulo.interpolate(angle, d, 360), Millimeter)
    
    def pointsToContour(self, points):
        return np.array(list(list(list(x)) for x in points), dtype=np.float32)

    def cartesianSamples(self):
        def toRad(deg):
            return deg / 180.0 * math.pi
        return (((math.cos(toRad(p[0])) * p[1].amount(Millimeter)), (math.sin(toRad(p[0])) * p[1].amount(Millimeter))) for p in self.samples())

    def maxPeak(self):
        return max(self.samples(), key=lambda x: x[1].amount(Millimeter))[0]

    def height(self):
        return self._height
