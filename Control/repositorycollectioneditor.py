from collectioneditor import *

class RepositoryEditAdapter:
    def __init__(self, repository, editAdapter):
        self.repository = repository
        self.adapter = editAdapter

    def prepareEdit(self, repoObj):
        res = self.adapter.prepareEdit(repoObj.data)
        return res
            
    def finishEdit(self, repoObj, data):
        self.adapter.finishEdit(repoObj.data, data)
                
    def create(self):
        obj = self.repository.new() 
        obj.data = self.adapter.create()
        return obj


class RepositoryCollectionEditor(CollectionEditor):
    def __init__(self, editorWidget, messageChannel, titleProjector, repository, editAdapter, sortKey=None, title=None, customActions=[]):
        self.repository = repository
        super().__init__(
            editorWidget, messageChannel, titleProjector, 
            RepositoryEditAdapter(repository, editAdapter),
            sortKey=sortKey,
            title=title,
            customActions=customActions)
        self.setData(repository.all())
        self.itemChanged.connect(self.itemChangedHandler)
        self.itemDeleted.connect(self.itemDeletedHandler)

    def refresh(self):
        self.setData(self.repository.all())

    def itemChangedHandler(self, item):
        self.repository.save(item)

    def itemDeletedHandler(self, item):
        self.repository.delete(item.primaryKey)

