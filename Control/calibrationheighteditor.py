from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from sectioneditor import *
from collectioneditor import *
from widgets.minmax import MinMax
from user import *
from numericinput import *
import hwmonitor
from quantity import *
import time
from functools import *
from threading import *
from cancellationtoken import *


@loadUi('ui/heightcalibration.ui')
class CalibrationHeightEditor(QWidget):
    def __init__(self, hw):
        super().__init__()
        self.hw = hw

    def withLiftPermission(self, method):
        def wrapper():
            with self.hw.lift.unlock():
                method()
        return wrapper

    def initUi(self):
        self.up.pressed.connect(self.withLiftPermission(partial(self.hw.lift.jogp,10)) )
        self.up.released.connect(self.withLiftPermission(self.hw.lift.jogcancel))
        self.down.pressed.connect(self.withLiftPermission(partial(self.hw.lift.jogn,10))  )
        self.down.released.connect(self.withLiftPermission(self.hw.lift.jogcancel))
        self.upSlow.pressed.connect(self.withLiftPermission(partial(self.hw.lift.jogp,3))  )
        self.upSlow.released.connect(self.withLiftPermission(self.hw.lift.jogcancel))
        self.downSlow.pressed.connect(self.withLiftPermission(partial(self.hw.lift.jogn,3))  )
        self.downSlow.released.connect(self.withLiftPermission(self.hw.lift.jogcancel))
        self.extend.clicked.connect(self.withLiftPermission(self.extend_clicked))

    def activate(self):
        self.hw.vgauge.deactivate()
        self.extended = False
        self.monitor()

    def monitor(self):
        self.cancelToken = CancellationToken()
        Thread(target=self.monitorContact, args=(self.cancelToken,)).start()

    def deactivate(self):
        self.hw.vgauge.deactivate()
        self.extended = False
        self.cancelToken.cancel()

    def extend_clicked(self, *args, **kwargs):
        if not self.extended:
            with self.hw.lift.unlock():
                self.hw.lift.reconfigureSpecial('BelowFixedVerticalSensor')
                while not self.hw.lift.done():
                    pass

            self.hw.vgauge.activate()

            with self.hw.lift.unlock():
                self.hw.lift.reconfigureSpecial('Max')
                while not self.hw.lift.done():
                    pass

            time.sleep(self.hw.machine.specs['vfixed']['wait_seconds'])
            self.fixed = self.hw.machine.slider.auxSensor()
            print('fixed',self.fixed)
            self.extend.setText('Zatáhnout měřák')
            self.extended = True
        else:
            self.hw.vgauge.deactivate()
            self.extend.setText('Vytáhnout měřák')
            self.extended = False
    
    def child_clicked(self, *args, **kwargs):
        if self.sender().property('numeric'):
            from numericinput import NumericInput
            input = NumericInput(hash_char='.')
            input.set_value(self.sender().text())
            if input.exec():
                self.sender().setText(str(input.value()))
        else:
            from widgets.textinput import TextInput
            input = TextInput()
            input.setValue(self.sender().text())
            if input.exec():
                self.sender().setText(input.value())

    def monitorContact(self, cancel):
        while not cancel.isCancelled():
            try:
                if (not self.extended) or (self.hw.lift.position() > self.hw.lift.specialPosition('BelowFixedVerticalSensor') - QuantityLiteral(10, Millimeter)):
                    time.sleep(0.1)
                    continue
            
                v = self.hw.machine.vsensor.value()
                if v is not None:
                    if v.amount(Millimeter) > 1.5:
                        self.hw.lift.jogcancel()
                        time.sleep(1)
                        v = None
                        while v is None:
                            v = self.hw.machine.vsensor.value()
                        self.statv = v
                        self.statliftPos = self.hw.lift.position()
                        with self.hw.lift.unlock():
                            self.hw.lift.reconfigure(QuantityLiteral(5, Millimeter), relative=True)
                        while not self.hw.lift.done():
                            pass

                        def showContactForm():
                            def do():
                                contactForm = QWidget()
                                uic.loadUi('ui/heightcontact.ui', contactForm)
                                from widgets.mylineedit import MyLineEdit
                                for child in contactForm.findChildren(MyLineEdit):
                                    child.clicked.connect(self.child_clicked)
                        

                                val = QuantityLiteral( self.constant , Millimeter) + self.fixed + self.statv + self.statliftPos
                                print('self.constant',QuantityLiteral( self.constant , Millimeter), 'self.fixed', self.fixed, 'v', self.statv, 'liftPos', self.statliftPos)
                                print('val', val)
                                contactForm.lastCalib.setText('{0:.2f}'.format(val.amount(Millimeter)))
                                def destroy():
                                    self.invokeLater(lambda: self.navigator.unstack())
                                    contactForm.deleteLater()
                                    #self.monitor()

                                contactForm.close.clicked.connect(destroy)
                                def recalibrate():
                                    try:
                                        newVal = QuantityLiteral( float(contactForm.newCalib.text()), Millimeter)
                                        self.height = self.statliftPos.amount(Millimeter)
                                        print('self.height', self.height)
                                        self.constant = (newVal - self.fixed - self.statv - self.statliftPos).amount(Millimeter)
                                        self.calibrated = (QuantityLiteral( self.constant , Millimeter) + self.fixed + self.statv + self.statliftPos).amount(Millimeter)
                                
                                        print('newVal',newVal,'self.fixed',self.fixed,'v',self.statv,'liftPos',self.statliftPos)
                                        print('self.constant', QuantityLiteral( self.constant , Millimeter))
                                        destroy()
                                    except ValueError:
                                        self.invoke(lambda: self.navigator.messageBox('Hodnota musí být číslo'))
                                contactForm.recalibrate.clicked.connect(recalibrate)
                                self.navigator.stack(contactForm)
                            self.invoke(do)
                        print('scf entry')
                        showContactForm()
                        print('scf exit')

            except:
                pass 

    """def move(self, act0, act180):
        try:
            self.hw.lift.reconfigure(QuantityLiteral(self.parse(self.height.text(), 'výška'), Millimeter))
            while not self.hw.lift.done():
                pass
            self.hw.glue.attach()
            self.hw.hgauge.activate()
            act0()
            self.hw.machine.plate.set(QuantityLiteral(180, Degrees), speed=20, relative=True)
            while self.hw.machine.plate.isMoving():
                pass
            time.sleep(1)
            act180()
            self.hw.hgauge.deactivate()
            self.hw.glue.detach()
            self.hw.machine.plate.set(QuantityLiteral(-180, Degrees), speed=50, relative=True)
            while self.hw.machine.plate.isMoving():
                pass
        finally:
            self.hw.hgauge.deactivate()
            self.hw.glue.detach()

    def sample_clicked(self, *args, **kwargs):
        try:
            diamCalibrated = QuantityLiteral( self.parse(self.calibrated.text(), 'kalibrovaná hodnota'), Millimeter)
            res = {0:0, 1:0}

            def getValue():
                x = None
                while x is None:
                    x = self.hw.hgauge.value()
                return x

            def act0():
                res[0] += getValue()

            def act180():
                res[1] += getValue()

            SAMPLES = 4
            for i in range(SAMPLES):
                self.move(act0, act180)

            for key in res:
                res[key] = res[key] / float(SAMPLES)
            
            diamRaw = res[0] + res[1]
            constant = diamCalibrated - diamRaw
            self.constant.setText('{0:.3f}'.format(constant.amount(Millimeter)))
        except ValueError as e:
            self.messageChannel()(str(e))
       
    def test_clicked(self, *args, **kwargs):
        try:
            const = QuantityLiteral( self.parse(self.constant.text(), 'kalibrační konstanta'), Millimeter)
            res = dict()
            def getValue():
                x = None
                while x is None:
                    x = self.hw.hgauge.value()
                return x

            def act0():
                res[0] = getValue()

            def act180():
                res[1] = getValue()

            self.move(act0, act180)

            diamRaw = res[0] + res[1]
            diamCalibrated = diamRaw + const
            self.messageChannel()('Naměřená zkalibrovaná hodnota:\n{0:.2f} mm'.format(diamCalibrated.amount(Millimeter)))

        except ValueError as e:
            self.messageChannel()(str(e))
"""
    def fill(self, data):
        self.constant = data['constant']
        self.height = data['height']
        self.calibrated = data['calibrated']
        print('self.constant',self.constant)
        print('self.height',self.height)

    def read(self):
        res = {'height':self.height, 'constant':self.constant, 'calibrated':self.calibrated}
        print('returning',res)
        return res

    def clear(self):
        pass

    def parse(self, text, name):
        try:
            return float(text)
        except ValueError:
            raise ValueError('Parametr {0} musí být číslo'.format(name))

            