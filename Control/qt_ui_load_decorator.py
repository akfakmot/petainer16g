import PyQt5.uic as uic


def loadUi(uiFilepath):
    def decorator(cls):
        class UILoadDecoratedClass(cls):
            def __init__(self, *args, **kwargs):
                self.uiFilepath = uiFilepath
                super().__init__(*args, **kwargs)
                uic.loadUi(self.uiFilepath, self)
                if hasattr(self, 'initUi'):
                    self.initUi()

        return UILoadDecoratedClass
    return decorator
