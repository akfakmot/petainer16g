﻿from usbpath import *
import jsonpickle
from subprocess import getoutput
from functools import partial
import os


class Flash(object):
    def __init__(self, config_manager, config_id, diskByPath):
        self.diskByPath = diskByPath
        self.config_manager = config_manager
        self.config_id = config_id

    def eject(self, showMessage):
        if self.flashpath() is None:
            showMessage('Flashdisk není přítomen')
        else:
            showMessage('Operace vysunutí disku může trvat až minutu. Stiskněte OK a vyčkejte prosím')
            #getoutput('sudo umount ' + self.flashpath())
            try:
                os.system('umount /mnt/usb')    #usb_flashdisk_unmount(self.flashpath())
            except:
                pass
            showMessage('Nyní můžete flashdisk odebrat')

    def loadConfig(self):
        configGraph = self.config_manager.load(self.config_id)
        if configGraph is None:
            self.navigator.redirect('error', text='Není k dispozici konfigurace')
            return True
        return jsonpickle.loads(configGraph.decode('utf8'))

    def flashpath(self):
        if not os.path.exists('/dev/disk/by-path/'+self.diskByPath):
            return None
        try:
            if not os.path.ismount('/mnt/usb'):
                os.system("mount %s /mnt/usb" % ('/dev/disk/by-path/'+self.diskByPath))
        except:
            pass
        if os.path.ismount('/mnt/usb'):
            print('flashpath=/mnt/usb')
            return '/mnt/usb'
        else:
            print('flashpath=None')
            return None
        #return usb_flashdisk_mount(self.diskByPath)


    def prompt_flash_removal(self, showMessage):
        showMessage('Po ukončení práce nezapomeňte vysunout flashdisk (pomocí položky v menu)')


    def transfer_to_flash(self, showMessage, local_filename, remote_extension, message, extension='.json', isFolder=False):
        fileOrFolder = 'souboru' if not isFolder else 'složky'
        
        flash = self.flashpath()
            
        if flash is None:
            showMessage('Paměťové médium nebylo nalezeno')
        else:
            from widgets.textinput import TextInput
            keyboard = TextInput()
            keyboard.setValue(self.loadConfig()['stationName'] + remote_extension)
            keyboard.setPrompt('Název '+fileOrFolder+':')
                
            if keyboard.exec():
                exportfile = os.path.join(flash, keyboard.value() + extension)
                cmd = 'rm -rf "' + exportfile + '"'
                print(getoutput(cmd))
                if not isFolder:
                    cmd = 'cp -r '+local_filename+' "' + exportfile + '"'
                else:
                    cmd = 'tar -C "' + os.path.dirname(local_filename) + '" -cf "' + exportfile + '" "' + local_filename + '"'

                print(cmd)
                print(getoutput(cmd))
                showMessage(message+' do ' + fileOrFolder + ' ' + keyboard.value() + extension)
            else:
                showMessage('Nebyl zadán název '+fileOrFolder+'. Přenos dat nebude proveden')
            self.prompt_flash_removal(showMessage)


    def transfer_from_flash(self, showMessage, local_filename, header, requiredExtension, isFolder=False):
        if not os.path.exists(local_filename):
            showMessage('Nebyla nalezena žádná data')
        else:
            flash = self.flashpath()
            
            if flash is None:
                showMessage('Paměťové médium nebylo nalezeno')
            else:
                def op(file):
                    if file is None:
                        showMessage('Nebyl vybrán soubor. Přenos dat nebude proveden')
                        self.prompt_flash_removal(showMessage)
                    elif not file.endswith(requiredExtension):
                        showMessage('Vybraný soubor nemá správnou příponu. Je vyžadována přípona "%s". Přenos dat nebude proveden' % requiredExtension)
                        self.prompt_flash_removal(showMessage)
                    else:
                        #with open(file) as f:
                        #    parsed = list(sorted(jsonclass.loads(f.read()), key=lambda x: x.name))
                        #    if not all([type(x) == object_type for x in parsed]):
                        #        raise Exception()

                        cmd = 'rm -rf "' + local_filename + '"'

                        print(getoutput(cmd))
                        if not isFolder:
                            cmd = 'cp -r "'+file+'" "' + local_filename + '"'
                        else:
                            cmd = 'tar -xf "' + file + '" -C /'
                        print(cmd)
                        print(getoutput(cmd))

                        #getoutput('cp "' + file + '" /mnt/hard/' + local_filename)
                        showMessage('Import byl proveden')
                        self.prompt_flash_removal(showMessage)
                        
                self.selectfile(flash, op, header, False)

    def selectfile(self, dir, callback, header, isFolder):
        from selectdialog import SelectDialog
        
        self.dialog = SelectDialog(prompt=header)
        def op(res, file):
            if res:
                if type(file) == tuple:
                    self.selectfile_internal(file[1], allow_up=(dir != file[1]), header=header)
                elif os.path.isfile(file):
                    callback(file)
                    return True
                elif os.path.isdir(file):
                    if isFolder:
                        callback(file)
                        return True
                    else:
                        self.selectfile_internal(file, allow_up=True, header=header)
                else:
                    callback(None)
            else:
                callback(None)
                
        self.selectfile_internal(dir, allow_up=False, header=header)
        def callbackWrapper(*args):
            if op(True, *args):
                self.dialog.close()
        self.dialog.item_selected.connect(callbackWrapper)
        self.dialog.cancelled.connect(lambda: partial(op, False, None) and self.dialog.close())
        self.dialog.exec()
          
    def selectfile_internal(self, dir, allow_up=True, header=''):
        if allow_up:
            items = [('..', (-1, os.path.dirname(dir))),]
        else:
            items = []
        items = items + [(x,os.path.join(dir,x)) for x in os.listdir(dir)]
        
        self.dialog.set_items(items)
        

    def selectfile_callback(self, callback, res, file):
        if action.id == 'item_selected':
            callback(True, action.value)
        elif action.id == 'cancelled':
            callback(False, None)