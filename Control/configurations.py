from menupanel import MenuPanel
from hardware import HardwareConfiguration
from filerepository import FileRepositoryConfig


hardwareConfig = HardwareConfiguration()
usersRepoConfig = FileRepositoryConfig(folder='/mnt/hard/repos/users')

