from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from sectioneditor import *
from collectioneditor import *
from widgets.minmax import MinMax
from user import *
from numericinput import *
import hwmonitor
from quantity import *
import time


@loadUi('ui/radiuscalibration.ui')
class CalibrationRadiusEditor(QWidget):
    def __init__(self, hw):
        super().__init__()
        self.hw = hw

    def initUi(self):
        self.sample.clicked.connect(self.sample_clicked)
        self.test.clicked.connect(self.test_clicked)

    def move(self, act0, act180):
        try:
            self.hw.lift.reconfigure(QuantityLiteral(self.parse(self.height.text(), 'výška'), Millimeter))
            while not self.hw.lift.done():
                pass
            self.hw.glue.attach()
            self.hw.hgauge.activate()
            act0()
            self.hw.machine.plate.set(QuantityLiteral(180, Degrees), speed=20, relative=True)
            while self.hw.machine.plate.isMoving():
                pass
            time.sleep(1)
            act180()
            self.hw.hgauge.deactivate()
            self.hw.glue.detach()
            self.hw.machine.plate.set(QuantityLiteral(-180, Degrees), speed=50, relative=True)
            while self.hw.machine.plate.isMoving():
                pass
        finally:
            self.hw.hgauge.deactivate()
            self.hw.glue.detach()

    def sample_clicked(self, *args, **kwargs):
        try:
            diamCalibrated = QuantityLiteral( self.parse(self.calibrated.text(), 'kalibrovaná hodnota'), Millimeter)
            res = {0:0, 1:0}

            def getValue():
                x = None
                while x is None:
                    x = self.hw.hgauge.value()
                return x

            def act0():
                res[0] += getValue()

            def act180():
                res[1] += getValue()

            SAMPLES = 4
            for i in range(SAMPLES):
                self.move(act0, act180)

            for key in res:
                res[key] = res[key] / float(SAMPLES)
            
            diamRaw = res[0] + res[1]
            constant = diamCalibrated - diamRaw
            self.constant.setText('{0:.3f}'.format(constant.amount(Millimeter)))
        except ValueError as e:
            self.messageChannel()(str(e))
       
    def test_clicked(self, *args, **kwargs):
        try:
            const = QuantityLiteral( self.parse(self.constant.text(), 'kalibrační konstanta'), Millimeter)
            res = dict()
            def getValue():
                x = None
                while x is None:
                    x = self.hw.hgauge.value()
                return x

            def act0():
                res[0] = getValue()

            def act180():
                res[1] = getValue()

            self.move(act0, act180)

            diamRaw = res[0] + res[1]
            diamCalibrated = diamRaw + const
            f = 1-(diamCalibrated.amount(Millimeter)-65.0)/(93.0-65.0)
            diamCalibrated = QuantityLiteral(0.06, Millimeter)*f + diamCalibrated
            self.messageChannel()('Naměřená zkalibrovaná hodnota:\n{0:.2f} mm'.format(diamCalibrated.amount(Millimeter)))

        except ValueError as e:
            self.messageChannel()(str(e))

    def fill(self, data):
        self.height.setText('{0:.2f}'.format(data['height']))
        self.calibrated.setText('{0:.2f}'.format(data['calibrated']))
        self.constant.setText('{0:.3f}'.format(data['constant']))

    def read(self):
        height = self.parse(self.height.text(), 'výška')
        calibrated = self.parse(self.calibrated.text(), 'kalibrovaná hodnota')
        constant = self.parse(self.constant.text(), 'kalibrační konstanta')
        return {'height':height, 'calibrated':calibrated, 'constant':constant}

    def clear(self):
        self.height.setText(None)
        self.calibrated.setText(None)
        self.constant.setText(None)

    def parse(self, text, name):
        try:
            return float(text)
        except ValueError:
            raise ValueError('Parametr {0} musí být číslo'.format(name))

    def activate(self):
        self.hw.vgauge.deactivate()
        self.extended = False

    def deactivate(self):
        self.hw.vgauge.deactivate()