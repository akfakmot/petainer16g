from utility.modulo import *
from quantity import *
import hwmonitor

class RadialSamplerConfig:
    def __init__(self, angleDirector, sampler, speed, continuous):
        self.angleDirector = angleDirector
        self.sampler = sampler
        self.speed = speed
        self.continuous = continuous


class RadialSampler(object):
    StartAngle = QuantityLiteral(-10, Degrees)
    def __init__(self, config, machine, unit_converter):
        self.sampler = config.sampler
        self.angleDirector = config.angleDirector
        self.speed = config.speed
        self.continuous = config.continuous
        self.machine = machine
        self.unit_converter = unit_converter

    def sample(self):
        while True:
            done = True
            collect = []
            self.machine.plate.set(self.StartAngle)
            print('radialsampler waiting for plate setup')
            while self.machine.plate.isMoving():
                hwmonitor.check()
        
            self.angleDirector.reset()
            angle = self.angleDirector.next()
            lastAngle = 0
            if self.continuous:
                self.machine.plate.set(QuantityLiteral(380, Degrees), speed=self.speed, relative=True)
            else:
                self.machine.plate.set(QuantityLiteral(angle, Degrees), speed=self.speed, relative=False)
            while angle is not None:
                hwmonitor.check()
                atPosition = False
                if not self.continuous:
                    atPosition = not self.machine.plate.isMoving()
                else:
                    curAngle = self.unit_converter.convertQuantity(self.machine.plate.position(), Degrees).value()
                    print('curAngle', curAngle, 'lastAngle', lastAngle, 'angle', angle)
                    atPosition = shortestDiff(curAngle, angle, 360) <= 0
                    print('shortestDiff(lastAngle, angle, 360)=', shortestDiff(lastAngle, angle, 360))
                    print('shortestDiff(curAngle, angle, 360)=', shortestDiff(curAngle, angle, 360))

                if atPosition:
                    s = self.sampler()
                    if s is None:
                        done=False
                        break
                    collect.append((angle, s))
                    lastAngle = curAngle
                    angle = self.angleDirector.next()
                    if not self.continuous:
                        self.machine.plate.set(QuantityLiteral(angle, Degrees), speed=self.speed, relative=False)
            if done:
                break
        return collect