abortRequest = False
abortCondition = lambda: False

def abort():
    global abortRequest
    abortRequest = True


class HardwareAbortException(Exception):
    def __init__(self, source):
        self.source = source


def check():
    global abortRequest, abortCondition
    if abortRequest:
        abortRequest = False
        abortCondition = lambda: False
        raise HardwareAbortException(source='request')
    if abortCondition():
        abortRequest = False
        abortCondition = lambda: False
        raise HardwareAbortException(source='condition')

class HardwareAbortContext(object):
    def __init__(self, hw):
        self.hw = hw

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type == HardwareAbortException:
            self.hw.lift.cancel()
            self.hw.machine.plate.cancel()
            self.hw.vgauge.deactivate()
            self.hw.hgauge.deactivate()
            self.hw.glue.detach()
            self.hw.machine.binary.outputs.light.clear()
            