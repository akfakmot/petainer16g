from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from asyncupdatepanel import *
import numpy as np
import time
import cv2
from cv2imageview import *


class ImagePanel(AsyncUpdatePanel):
    def __init__(self, cam):
        super().__init__()
        self.setLayout(QGridLayout())
        self.display = Cv2ImageView()
        self.display.setFixedSize(400,300)
        self.layout().addWidget(self.display)
        self.addLoop(self.refresh)
        self.cam = cam
        self.title = 'image'
        self.frameCounter = 0

    def refresh(self):
        frame = self.cam.capture().copy()
        self.invoke(self.update, frame)
        time.sleep(0.100)

    def update(self, img):
        self.display.setImage(img)