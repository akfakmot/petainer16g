from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from autologging import *

@traced
@logged
class CollectionEditor(QWidget):
    itemDeleted = pyqtSignal(object)
    itemChanged = pyqtSignal(object)
    editModeChanged = pyqtSignal(bool)

    def __init__(self, editorWidget, messageChannel, titleProjector, editAdapter, sortKey=None, title=None, customActions=[]):
        super().__init__()
        self.messageChannel = messageChannel
        self.titleProjector = titleProjector
        self.editAdapter = editAdapter
        print('self.editAdapter=',self.editAdapter)
        self.sortKey = sortKey

        topLayout = QHBoxLayout()
        topLayout.setContentsMargins(0,0,0,0)
        topLayout.setSpacing(6)
        self.setLayout(topLayout)
        self.setStyleSheet(':enabled { color:black } :disabled { color:#444 } QPushButton:disabled { color:#aaa }')

        leftColumn = QVBoxLayout()
        self.listButtons1 = QHBoxLayout()
        self.listButtons1.setContentsMargins(0,0,0,0)
        self.listButtons2 = QHBoxLayout()
        self.listButtons2.setContentsMargins(0,0,0,0)
        self.listButtons3 = QHBoxLayout()
        self.listButtons3.setContentsMargins(0,0,0,0)
        self.list = QListView(self)
        self.list.setStyleSheet(':enabled { background:white } :disabled { background:#DDD }')
        if title is not None:
            leftColumn.addWidget(QLabel(text=title), 0)
        leftColumn.addWidget(self.list, 1)
        leftColumn.addLayout(self.listButtons1, 0)
        leftColumn.addLayout(self.listButtons2, 0)
        leftColumn.addLayout(self.listButtons3, 0)

        rightColumn = QVBoxLayout()
        rightColumn.setContentsMargins(0,0,0,0)
        rightColumn.setSpacing(3)
        self.editor = editorWidget
        rightColumn.addWidget(self.editor)
        self.editor.blockQuit = self.blockEditorQuit
        self.editor.messageChannel = messageChannel
        self.editorButtons = QHBoxLayout()
        rightColumn.addLayout(self.editorButtons)

        self.layout().addLayout(leftColumn, 1)
        self.layout().addLayout(rightColumn, 3)
        
        def addButton(text, layout, handler):
            button = QPushButton(text=text)
            button.clicked.connect(handler)
            layout.addWidget(button)
            return button

        self.deleteButton = addButton('Smazat', self.listButtons1, self.deleteButton_clicked)
        self.deleteButton.setEnabled(False)
        self.newButton = addButton('Nový', self.listButtons1, self.newButton_clicked)
        self.editButton = addButton('Upravit', self.listButtons2, self.editButton_clicked)
        self.editButton.setEnabled(False)
        self.customButtons = []
        for action in customActions:
            btn = addButton(action[0], self.listButtons3, action[1])
            self.customButtons.append(btn)
        self.cancelButton = addButton('Zpět', self.editorButtons, self.cancelButton_clicked)
        self.saveButton = addButton('Uložit', self.editorButtons, self.saveButton_clicked)
        self.edit(False)
        self.currentRow = None
        self.setData([])
        self.mute = False

    def blockEditorQuit(self, block):
        self.saveButton.setVisible(not block)
        self.cancelButton.setVisible(not block)

    def newButton_clicked(self, *args, **kwargs):
        self.selectRow(None)
        self.editedObject = self.editAdapter.create()
        self.editor.fill(self.editAdapter.prepareEdit(self.editedObject))
        self.edit(True)

    def editButton_clicked(self, *args, **kwargs):
        self.edit(True)
    
    def deleteButton_clicked(self, *args, **kwargs):
        if self.currentRow is not None:
            self.mute = True
            self.itemDeleted.emit(self.currentItem().data())
            self.model.removeRows(self.currentRow, 1)
            if self.currentRow >= self.model.rowCount():
                if self.model.rowCount() == 0:
                    self.currentRow = None
                else:
                    self.currentRow = self.model.rowCount() - 1

            self.selectCurrent()
            self.refreshItem()
            self.mute = False

    def saveButton_clicked(self, *args, **kwargs):
        try:
            self.editAdapter.finishEdit(self.editedObject, self.editor.read())
        except Exception as e:
            self.messageChannel()(str(e))
            return

        if self.currentItem() is None:
            newItem = QStandardItem()
            newItem.setData(self.editedObject)
            self.model.appendRow(newItem)
            self.selectRow(self.model.rowCount() - 1)
        else:
            self.currentItem().setData(self.editedObject)

        self.itemChanged.emit(self.editedObject)

        self.setData(self.data(), resetSelection=False)
        self.edit(False)

    def cancelButton_clicked(self, *args, **kwargs):
        self.refreshItem()
        self.edit(False)

    def edit(self, on):
        self.editor.setEnabled(on)
        self.list.setEnabled(not on)
        self.editButton.setVisible(not on)
        self.newButton.setVisible(not on)
        self.deleteButton.setVisible(not on)
        for btn in self.customButtons:
            btn.setVisible(not on)
        self.saveButton.setVisible(on)
        self.cancelButton.setVisible(on)
        self.editModeChanged.emit(on)

    def itemFromObject(self, obj):
        item = QStandardItem(self.titleProjector(obj))
        item.setData(obj)
        return item

    def data(self):
        collection = []
        for i in range(self.model.rowCount()):
            collection.append(self.model.item(i).data())
        return collection

    def setData(self, data, resetSelection=True):
        if data is None:
            self.editor.clear()
        else:
            selectedObj = self.currentItem().data() if self.currentItem() is not None else None
            self.model = QStandardItemModel()
            row = 0
            for obj in (sorted(data, key=self.sortKey) if self.sortKey is not None else data):
                if selectedObj == obj:
                    self.currentRow = row
                self.model.appendRow(self.itemFromObject(obj))
                row += 1

            self.list.setModel(self.model)
            if not resetSelection:
                if self.currentRow is not None:
                    self.selectCurrent()
                else:
                    self.editButton.hide()
            else:
                self.selectRow(None)
                #if self.new:
                #    self.list.scrollTo(self.list.selectionModel().selectedIndexes()[0], QAbstractItemView.PositionAtBottom)
            self.list.selectionModel().currentChanged.connect(self.itemSelected)
            self.newItem = None

    def selectRow(self, row):
        self.currentRow = row
        self.selectCurrent()

    def selectCurrent(self):
        if self.currentRow is None:
            self.list.selectionModel().select(self.model.createIndex(0, 0), QItemSelectionModel.Clear)
            self.deleteButton.setEnabled(False)
            self.editButton.setEnabled(False)
        else:
            self.list.selectionModel().select(self.model.createIndex(self.currentRow, 0), QItemSelectionModel.ClearAndSelect)
            self.deleteButton.setEnabled(True)
            self.editButton.setEnabled(True)
        self.refreshItem()

    def refreshItem(self):
        if self.currentItem() is not None:
            self.editor.fill(self.editAdapter.prepareEdit(self.currentItem().data()))
        else:
            self.editor.clear()

    def itemSelected(self, cur, prev):
        self.deleteButton.setEnabled(True)
        self.editButton.setEnabled(True)

        if self.mute: return
        self.currentRow = self.list.selectionModel().currentIndex().row()
        self.editedObject = self.currentItem().data()
        self.editButton.show()
        self.refreshItem()

    def currentItem(self):
        if self.currentRow is None: 
            return None
        else: 
            item = self.model.item(self.currentRow)
            return item


class DictionaryEditAdapter(object):
    def __init__(self, provideEmptyData):
        self.provideEmptyData = provideEmptyData

    def prepareEdit(self, obj):
        return obj

    def finishEdit(self, obj, data):
        for key in obj:
            print('DictionaryEditAdapter: for', key, 'in obj')
            if key in data:
                print('data[key]=', data[key])
                obj[key] = data[key]
            else:
                print('data doesn\'t contain key', key)
        print('obj=',obj)

    def create(self):
        return self.provideEmptyData()
