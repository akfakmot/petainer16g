from XsecGeometry import XSecGeometry
from quantity import Quantity


class BottleGeometry:
    def __init__(self, name=None, xsecs=None, 
                    height=None, finishSlant=None, 
                    perpend=None):
        self.name = name
        self.xsecs = xsecs
        self.height = height
        self.finishSlant=finishSlant
        self.perpend=perpend
