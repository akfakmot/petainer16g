class ConfigManager(object):
    def __init__(self, filesystem, storage_folder):
        self.filesystem = filesystem
        self.storage_folder = storage_folder

    def filepath(self, config_id):
        return self.filesystem.combine(self.storage_folder, config_id)

    def load(self, config_id):
        if self.filesystem.exists(self.filepath(config_id)):
            print('reading',self.filepath(config_id))
            return self.filesystem.readall(self.filepath(config_id))
        else:
            return None

    def save(self, config_id, graph):
        self.filesystem.writeall(self.filepath(config_id), graph)


