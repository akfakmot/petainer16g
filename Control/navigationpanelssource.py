import functools
from PyQt5.QtWidgets import QApplication
from camv4l2 import *

class NavigationPanelsSource:
    singletonPanels = dict()

    def __init__(self, machine, lift):
        self.machine = machine
        self.lift = lift

    def singletonPanel(self, title):
        if title not in NavigationPanelsSource.singletonPanels:
            NavigationPanelsSource.singletonPanels[title] = self.createSingletonPanel(title)
        return NavigationPanelsSource.singletonPanels[title]

    def createSingletonPanel(self, title):
        from menupanel import MenuPanel, MenuItem
        from manualliftpanel import ManualLiftPanel
        from manualplatepanel import ManualPlatePanel
        from manualiopanel import ManualIOPanel
        from imagepanel import ImagePanel

        if title == 'Výtah':
            return ManualLiftPanel(self.machine, self.lift, title)
        elif title == 'Talíř':
            return ManualPlatePanel(self.machine, title)
        elif title == 'Vstupy && výstupy':
            return ManualIOPanel(self.machine, title)
        else:
            raise NotImplementedError

    def setup(self, switchToPanel):
        from menupanel import MenuPanel, MenuItem
        from manualliftpanel import ManualLiftPanel
        from manualplatepanel import ManualPlatePanel
        from manualiopanel import ManualIOPanel
        from imagepanel import ImagePanel

        self._switchToPanel = switchToPanel

        mainMenu = MenuPanel()
        mainMenu.isCloseable = False
        def singletonItem(title):
            return MenuItem(title, functools.partial(self.switchToPanel, self.singletonPanel(title)))

        mainMenu.items = [singletonItem('Výtah'), singletonItem('Talíř'), singletonItem('Vstupy && výstupy'),
                            MenuItem('Konec', QApplication.instance().exit)]
        mainMenu.title = 'Menu'
        """cam = CamV4L2('/dev/video0', 320, 240)
        cam.setup()
        mic = CamV4L2('/dev/video1', 640, 480)
        mic.setup()"""
        self.mainMenu = mainMenu
        self.panels = [mainMenu, ]
        self._activePanel = mainMenu

    def switchToPanel(self, panel):
        if panel not in self.panels:
            self.panels.append(panel)
        self._switchToPanel(panel)

    def closePanel(self, panel):
        self.panels.remove(panel)
        self._switchToPanel(self.mainMenu)

    def get(self):
        return self.panels
    
    def activePanel(self):
        return None #self._activePanel


class NavigationPanelsSourceFactory:
    def __init__(self, machine, lift):
        self.machine = machine
        self.lift = lift
        self.instance = NavigationPanelsSource(self.machine, self.lift)

    def create(self):
        return NavigationPanelsSource(self.machine, self.lift)