from typing import Sequence, Callable
from autologging import *


class Target:
    pass

class Watcher:
    def __init__(self, condition :Callable[[],bool], action :Callable[[],None]):
        self.condition = condition
        self.action = action

    """ returns True if the Watcher has finished """
    def watch(self) -> bool:
        if self.condition():
            self.action()
            return True
        else:
            return False

@traced
@logged
class Target:
    
    def __init__(self, preconditions :Sequence[Target] =[], weight :int =None):
        self.preconditions = preconditions
        self.weight = weight
        self._finished = False

    def hookEngine(self, spawnWatcher :Callable[[Watcher],None] =None):
        self.spawnWatcher = spawnWatcher
        
    def execute(self):
        allChildren = sorted(self.preconditions, key=lambda x: x.getWeight())
        readyChildren = filter(lambda x: not x.finished() and x.canExecute(), allChildren)
        try:
            child = next(readyChildren)
            child.execute()
        except StopIteration:
            pass

        self._finished = all(child.finished() for child in allChildren)
        if self._finished:
            self.__log.debug('Target %s was executed' % type(self))
        return self._finished
        
    def canExecute(self):
        return True

    def getWeight(self):
        if self.weight is None:
            return sum(p.getWeight() for p in self.preconditions)
        else:
            return self.weight
        
    def finished(self):
        return self._finished


class TestTarget(Target):
    def __init__(self, msg=None, delay=0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.msg = msg
        self._finished = False
        self.delay = delay

    def execute(self):
        if super().execute():
            print(self.msg)
            self._finished = True
    
    def finished(self):
        return self._finished

    def canExecute(self):
        return True


class RetardedTestTarget(TestTarget):
    def __init__(self, retard=0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.retard = retard

    def canExecute(self):
        if self.retard == 0:
            return True
        else:
            self.retard -= 1
            return False


class TargetEngine:
    def __init__(self):
        self.watchers = []

    def execute(self, target :Target):
        target.hookEngine(self.spawnWatcher)
        while not target.finished():
            if target.canExecute():
                target.execute()
                for watcher in self.watchers:
                    watcher.watch()
        
    def spawnWatcher(self, watcher :Watcher):
        self.watchers.append(watcher)
        
    