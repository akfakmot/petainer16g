from panel import Panel
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from camlist import camlist
from configpanel import *
import json


class CamConfigWidget(QWidget):
    def __init__(self, name, cams, showImage):
        super().__init__()
        self._showImage = showImage
        self.setLayout(QHBoxLayout())
        self.layout().addWidget(QLabel(text=name+':'))
        combo = QComboBox()
        self.cams = cams
        combo.addItem(None)
        for cam in list(cams.values()):
            combo.addItem(cam['ID_PATH'])
        self.combo = combo
        self.layout().addWidget(combo)
        #detailButton = QPushButton(text='Detail')
        #detailButton.clicked.connect(self.showDetail)
        #self.layout().addWidget(detailButton)
        imageButton = QPushButton(text='Obraz')
        imageButton.clicked.connect(self.showImage)
        self.layout().addWidget(imageButton)
        self.layout().addItem(QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum))
        
    def showImage(self):
        if self.combo.currentIndex() != 0:
            self._showImage(self.combo.currentIndex() - 1)

    def setIndex(self, index):
        self.combo.setCurrentIndex(index+1)

    def currentIndex(self):
        return self.combo.currentIndex()-1


from cv2imageview import *


class CamConfigPanel(ConfigPanel):
    usesMachine = True
    """ specs = tuple(key, header, resolution) """
    def __init__(self, config_manager, config_id, cam_stock, specs, resolution):
        self.cam_stock = cam_stock
        self.specs = specs
        self.resolution = resolution
        self.camlist = cam_stock.cameraList()
        super().__init__(config_manager, config_id)

    def buildUi(self, frame):
        frame.setLayout(QVBoxLayout())
        self.camWidgets = dict()
        for spec in self.specs:
            w = CamConfigWidget(spec[1], self.camlist, self.showImage)
            frame.layout().addWidget(w, 0)
            self.camWidgets[spec[0]] = w
        #self.layout().addWidget(CamConfigWidget('Výšková kamera', self.camlist, self.showImage, lambda message: self.navigator.messageBox(message)), 0)
        self.display = Cv2ImageView()
        self.display.setSizePolicy(QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding))
        displayLayout = QHBoxLayout()
        displayLayout.addWidget(self.display)
        frame.layout().addLayout(displayLayout, 1)
        
        #self.layout().addItem(QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding))
        frame.setStyleSheet('font: 16pt Tahoma')

    def fill(self, configGraph):
        if configGraph is not None:
            data = json.loads(configGraph.decode('utf8'))
            print('data=',data)
            l = list(self.camlist.values())
            for key in data:
                if key in self.camWidgets:
                    self.camWidgets[key].setIndex(-1)
                    for i in range(len(l)):
                        print('going through', l[i]['ID_PATH'])
                        if l[i]['ID_PATH'] == data[key]:
                            self.camWidgets[key].setIndex(i)
                            break
                    
    def dump(self):
        data = dict()
        for key in self.camWidgets:
            if self.camWidgets[key].currentIndex() == -1:
                data[key] = None
            else:
                data[key] = list(self.camlist.values())[self.camWidgets[key].currentIndex()]['ID_PATH']
        return json.dumps(data).encode('utf8')

    def showImage(self, camIndex):
        cam = self.cam_stock.camera(list(self.camlist.values())[camIndex]['ID_PATH'], self.resolution)
        self.display.setImage(cam.capture())