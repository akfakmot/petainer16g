from quantity import *
from threading import *
import time
from cancellationtoken import *
from asyncupdatepanel import AsyncUpdatePanel
from functools import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from autologging import *
from qt_ui_load_decorator import loadUi
from PyQt5.QtWidgets import *


@loadUi('ui/manualio.ui')
@traced
@logged
class ManualIOPanel(AsyncUpdatePanel):
    usesMachine = True
    def __init__(self, machine, title=''):
        super().__init__()
        self.machine = machine
        self.title = title
        self.addLoop(self.updateLoop)
            
    def initUi(self):
        self.binOutputs.setLayout(QGridLayout(self))
        self.binInputs.setLayout(QGridLayout(self))
        self._binInitialized = False

    def colourBinary(self, widget, state):
        widget.setStyleSheet('background:lime' if state else 'background:red')

    def toggleOutput(self, output, *args, **kwargs):
        output.write(not output.state())
        self.colourBinary(self.outputWidgets[output], output.state())
    
    def activate(self):
        super().activate()
        if not self._binInitialized:
            self.initPins()

            self._binInitialized = True

        super().activate()

    def updateLoop(self):
        errorText = 'Error'
        hsensor = errorText
        vsensor_fixed = errorText
        
        try:
            val = self.machine.hsensor.value()
            hsensor = '%.3f mm' % (val and val.amount(Millimeter))
        except:
            self.__log.warn('hsensor value couldn\'t be obtained')

        try:
            val = self.machine.vsensor.value()
            vsensor = '%.3f mm' % (val and val.amount(Millimeter))
        except:
            self.__log.warn('vsensor value couldn\'t be obtained')

        try:
            vsensor_fixed = '%.3f mm' % self.machine.slider.auxSensor().amount(Millimeter)
        except:
            self.__log.warn('vsensor_fixed value couldn\'t be obtained')
        
        inputStates = dict((widget, input.state()) for input, widget in self.inputWidgets.items())
        

        self.invoke(self.updateGUI, hsensor, vsensor, vsensor_fixed, inputStates)
        time.sleep(0.100)

    def updateGUI(self, hsensor, vsensor, vsensor_fixed, inputStates):
        self.hsensor.setText(hsensor)
        self.vsensor_fixed.setText(vsensor_fixed)
        self.vsensor.setText(vsensor)
        for widget, state in inputStates.items():
            self.colourBinary(widget, state)

    def initPins(self):
        i = 0
        self.outputWidgets = dict()
        for name in sorted(self.machine.binary.outputList(), key=lambda name: self.machine.binary.output(name).pin):
            output = self.machine.binary.output(name)
            button = QPushButton()
            button.setText(output.prettyString())
            button.setSizePolicy(QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding))
            button.clicked.connect(partial(self.toggleOutput, output))
            self.binOutputs.layout().addWidget(button, i, 0)
            self.outputWidgets[output] = button
            self.colourBinary(button, output.state())
            i += 1

        i = 0
        self.inputWidgets = dict()
        for name in self.machine.binary.inputList():
            input = self.machine.binary.input(name)
            label = QLabel()
            label.setText(input.prettyString())
            label.setMargin(20)
            label.setWordWrap(True)
            label.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
            self.binInputs.layout().addWidget(label, i, 0)
            self.inputWidgets[input] = label
            self.colourBinary(label, input.state())
            i += 1