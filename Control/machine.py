from enum import *
from quantity import *
import pinject


class MovingDirection(Enum):
    Zero = 0
    Positive = 1
    Negative = 2


class MachineComponent:
    def setup(self):
        pass

    def setupFinished(self):
        return True

    def release(self):
        pass


class LinearDrive(MachineComponent):
    def position(self) -> Quantity:
        raise NotImplementedError

    def movingDir(self) -> MovingDirection:
        raise NotImplementedError

    def isMoving(self):
        return self.movingDir() != MovingDirection.Zero

    def set(self, position, speed=100):
        raise NotImplementedError

    def setSync(self, position, speed=100):
        self.set(position, speed)
        while self.isMoving(): pass

    def cancel(self):
        raise NotImplementedError

    def setup(self):
        raise NotImplementedError
    
    def shut(self):
        raise NotImplementedError

    def ready(self):
        raise NotImplementedError

    def setupFinished(self):
        raise NotImplementedError


class RotaryDrive(MachineComponent):
    def position(self) -> Quantity:
        raise NotImplementedError

    def isMoving(self) -> bool:
        raise NotImplementedError

    def set(self, position, speed=100, relative=False):
        raise NotImplementedError
    
    def setSync(self, position, speed=100, relative=False):
        self.set(position, speed, relative)
        while self.isMoving(): pass

    """ Exposes smooth part of the plate to 'position'. Smooth part is a part without reference screw """
    def setSmoothSection(self, position):
        raise NotImplementedError

    def setup(self):
        raise NotImplementedError
    
    def shut(self):
        raise NotImplementedError

    def setupFinished(self):
        raise NotImplementedError

    def ready(self):
        raise NotImplementedError


class Sensor(MachineComponent):
    def value(self) -> Quantity:
        raise NotImplementedError
                

class BinaryOutput(MachineComponent):
    def state(self):
        raise NotImplementedError

    def set(self):
        raise NotImplementedError
    
    def clear(self):
        raise NotImplementedError

    def write(self, state):
        if state:
            self.set()
        else:
            self.clear()

    def prettyString(self):
        raise NotImplementedError


class BinaryInput(MachineComponent):
    def state():
        raise NotImplementedError
    
    def prettyString():
        raise NotImplementedError


class Slider(MachineComponent):
    def set(self, depth):
        raise NotImplementedError

    def home(self):
        raise NotImplementedError

    def done(self):
        raise NotImplementedError


class DotAccess:
    def __init__(self, mapping):
        self.__mapping__ = mapping

    def __getattr__(self, name):
        if name == '__mapping__':
            return self.__mapping__
        return self.__mapping__(name)


class Binary(MachineComponent):
    def __init__(self):
        def getOutput(name):
            return self.output(name)

        def getInput(name):
            return self.input(name)

        self.outputs = DotAccess(getOutput)
        self.inputs = DotAccess(getInput)

    """ list of output names """
    def outputList(self):
        raise NotImplementedError
    
    """ list of input names """
    def inputList(self):
        raise NotImplementedError
    
    """ returns BinaryOutput """
    def output(self, name):
        raise NotImplementedError

    """ returns BinaryInput """
    def input(self, name):
        raise NotImplementedError
    
    def setup(self):
        for pin in self.outputList():
            self.output(pin).clear()


from camlist import camlist

class Machine(MachineComponent):
    @pinject.copy_args_to_public_fields
    def __init__(self, specs=None, binary=None, liftDrive=None, plate=None, slider=None, hsensor=None, vsensor=None, cam_stock=None):
        self.components = ['binary', 'liftDrive', 'plate', 'slider', 'hsensor', 'vsensor']

    def forAll(self, method, targets):
        for targetName in targets:
            print('Calling {0} for target {1}'.format(method, targetName))
            component = getattr(self, targetName)
            if component is not None: 
                if hasattr(component, method):
                    getattr(component, method)()

    def setup(self):
        self.forAll('setup', self.components)
        
    def release(self, *args, **kwargs):
        for outputName in self.binary.outputList():
            self.binary.output(outputName).clear()
        self.forAll('release', ['liftDrive', 'plate'])
        
    def setupFinished(self):
        return all(component.setupFinished() for component 
                    in (getattr(self, name) for name in self.components))

    