def shortestDiff(minuend, subtrahend, moduloBase):
    def castToBaseSpan(x):
        while (x < -float(moduloBase)/2.0):
            x += moduloBase
        while (x > float(moduloBase)/2.0):
            x -= moduloBase
        return x

    diff1 = castToBaseSpan(-minuend - moduloBase + subtrahend)
    diff2 = castToBaseSpan(subtrahend - minuend)
    if abs(diff1) < abs(diff2):
        return diff1
    else:
        return diff2

def nearestEquivalent(current, target, modulus):
    currentOverlap = current % modulus
    targetOverlap = target % modulus
    target = current - currentOverlap + targetOverlap
    if abs((target - modulus) - current) < abs(target - current):
        target = target - modulus
    elif abs((target + modulus) - current) < abs(target - current):
        target = target + modulus
    return target


def between(left, x, right, base):
    return shortestDiff(left, x, base) >= 0 and shortestDiff(right, x, base) <= 0


def interpolate(x, samples, base):
    print('modulo.interpolate(',x,samples,base)
    x = x % 360
    d = samples
    for i in range(len(d)-1):
        print('between(', d[i][0], x, d[i+1][0], 360)
        if between(d[i][0], x, d[i+1][0], 360):
            left = d[i]
            right = d[i+1]
            print('interpolating between', left[0], 'and', right[0])
            f = shortestDiff(x,left[0],360) / shortestDiff(right[0],left[0],360)
            print('f=',f)
            interpolated = left[1] + f * (right[1]-left[1])
            return interpolated
    return None