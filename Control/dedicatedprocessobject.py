from autologging import *
import multiprocessing as mp
from functools import *


class DedicatedProcessObject:
    def __init__(self, objClass, publicMethods, *args, **kwargs):
        self.objClass = objClass
        self.qToServer = mp.Queue()
        self.qFromServer = mp.Queue()
        self.qToServer.put((args, kwargs,))
        self.process = mp.Process(target=self.server, args=(self.qToServer, self.qFromServer))
        for method in publicMethods:
            setattr(self, method, partial(self.objMethodCall, method))
        self.process.start()

    def server(self, qIn, qOut):
        args = qIn.get()
        obj = self.objClass(*args[0], **args[1])
        while True:
            cmd = qIn.get()
            if len(cmd) > 0:
                method = getattr(obj, cmd[0])
                res = method(*cmd[1], **cmd[2])
                qOut.put(res)
            
    def objMethodCall(self, methodName, *args, **kwargs):
        self.qToServer.put(obj=(methodName, args, kwargs))
        return self.qFromServer.get()        