from cv2imageview import *
from asyncupdatepanel import *
import time
from cvalgo import *


class CenteringCamPanel(AsyncUpdatePanel):
    usesMachine = True
    def __init__(self, config_manager, config_id, registered_cams, cv):
        super().__init__()
        self.registered_cams = registered_cams
        self.addLoop(self.loop)
        self.setLayout(QVBoxLayout())
        imgLayout = QHBoxLayout()
        self.displayRaw = Cv2ImageView()
        self.displayProc = Cv2ImageView()
        imgLayout.layout().addWidget(self.displayRaw)
        imgLayout.layout().addWidget(self.displayProc)
        self.layout().addLayout(imgLayout, 1)
        self.info = QLabel()
        self.layout().addWidget(self.info, 0)
        self.cv = cv
        self.cv.init()

    def loop(self):
        self.edge, _, self.rawFrame, self.frame = self.cv.process(True)
        self.invoke(self.update)
        time.sleep(0.100)

    def update(self):
        self.displayRaw.setImage(self.rawFrame)
        self.displayProc.setImage(self.frame)
        if self.edge is not None:
            self.info.setText('{0:.2f}'.format(self.edge))



