from machine import *
from festolineardrive import *
from festorotarydrive import *
from gtsensor import *
from wiringPiIO import *
from pinject import *
from quantity import *
from gtsensor import *
from wpibinary import *
from filesystem import *
from spislider import *
from machinetargets import *
from hgauge import *
from vgauge import *
from timeout import *
from glue import *
from lift import *
from calibrategtsensor import *
from samplesectionfactory import *
from dedicatedprocessobject import *
from petnavigationservice import *
from navigation import *
from camv4l2 import *
from camstock import *
from configmanager import *
from registeredcams import *
from cvalgo import *
import json
from mockcalibrator import *
from Hw import *
import jsonpickle
import uuid
from session import *
from flash import *
from radiuscalibrator import *


class GTSensorAggregator(object):
    def __init__(self, sensor, channel, abs):
        self.sensor = sensor
        self.channel = channel
        self.abs = abs

    def setup(self, *args, **kwargs):
        return self.sensor.setup(*args, **kwargs)

    def setupFinished(self, *args, **kwargs):
        return self.sensor.setupFinished(*args, **kwargs)

    def value(self):
        v = self.sensor.value(self.channel)
        return v and v + self.abs


class HardwareBindingSpec(BindingSpec):
    def __init__(self):
        self._machineInstance = None

    def provide_liftDrive(self, unit_converter):
        return FestoLinearDrive(config=FestoLinearDriveConfig(host='10.6.17.11', abs=16620, lin=1, minimumRaw=15000, maximumRaw=322900), unit_converter=unit_converter)

    def provide_plate(self, unit_converter):
        return FestoRotaryDrive(config=FestoRotaryDriveConfig(host='10.6.17.12', abs=-25000), unit_converter=unit_converter)

    def provide_gtsensor(self):
        return DedicatedProcessObject(GTSensor, ('setup','setupFinished','value'), config=GTSensorConfig(host='10.6.17.13', channel=0, abs=0))

    def provide_hsensor(self, gtsensor):
        return GTSensorAggregator(gtsensor, 0, 28870)
    
    def provide_vsensor(self, gtsensor):
        return GTSensorAggregator(gtsensor, 1, 0)

    def provide_hgauge(self, machine, timeout, lift):
        return HGauge(HGaugeConfig(0.5, 1, 2), machine, timeout, lift)
    
    def provide_vgauge(self, machine, timeout):
        return VGauge(VGaugeConfig(0.5, 1, 2), machine, timeout)

    def provide_binary(self, filesystem):
        config = WpiBinaryConfig()
        
        if filesystem.exists('/home/pi/binary.conf'):
            config.deserialize(filesystem.readall('/home/pi/binary.conf').decode('utf8'))
                
        return WpiBinary(config)
    
    def provide_slider(self):
        config = SPISliderConfig(0, 1000, -80, 80, 1.25)
        return DedicatedProcessObject(SPISlider, ('setup','setupFinished','home','set','done','auxSensor'), config)

    def provide_calibrate_gtsensor(self, machine, hgauge, lift, plate, unit_converter):
        return DedicatedProcessObject(CalibrateGtsensor, ('value'), machine, hgauge, lift, plate, unit_converter)

    def provide_config_manager(self, filesystem):
        return ConfigManager(filesystem, '/mnt/hard/config')

    def provide_lift(self, machine, unit_converter, config_manager):
        return Lift(machine, unit_converter, config_manager, 'lift')

    def provide_machine(self, binary, liftDrive, plate, slider, hsensor, vsensor, cam_stock):
        specs = {   'plate' :
                        { 'radius' : QuantityLiteral(50, Millimeter),
                            'height' : QuantityLiteral(15, Millimeter) },
                    'vgauge' : 
                        { 'descend' : QuantityLiteral(5, Millimeter) },
                    
                    'vfixed' : 
                        { 'wait_seconds' : 2 }
                }
        return Machine(specs, binary, liftDrive, plate, slider, hsensor, vsensor, cam_stock)


    #def provide_navigation(self, navigation_panels_source_factory):
    #    return Navigation(navigation_panels_source_factory)
    
    def provide_glue(self, machine, timeout):
        return Glue(GlueConfig(0.3, 0.1), machine, timeout)

    def provide_main_window(self, navigation_service, session):
        import guiconfig
        return MainWindow(navigation_service, guiconfig.menuItems, (800,480), session)

    def provide_registered_cams(self, config_manager, cam_stock):
        return RegisteredCams(config_manager, 'cam_assignment', cam_stock)

    def provide_height_cv(self, registered_cams, config_manager):
        cv = Height(registered_cams.camera('height', (320,240)))
        graph = config_manager.load('height_cam')
        if graph is not None:
            cv.setParams(json.loads(graph.decode('utf8')))
        return cv

    def provide_neck_cv(self, registered_cams):
        cv = Neck(registered_cams.camera('centering', (640,480)))
        return cv

    @provides(in_scope=PROTOTYPE)
    def provide_calibrate_diam_factory(self, config_manager):
        graph = config_manager.load('radius_calib')
        if graph is not None:
            config = json.loads(graph.decode('utf8'))
            config = [{'height':QuantityLiteral( x['height'], Millimeter), 'corLin': x['corLin'], 'corAbs':QuantityLiteral(x['corAbs'], Millimeter)}
                      for x in config]
            return RadiusCalibrator(config)
        else:
            return None

        def create_calibrator(height):
            points = sorted([x.data for x in calibration_r_repository.all()], key=lambda point: point['height'])
            if len(points) < 2:
                return None
            elif height.amount(Millimeter) <= points[0]['height']:
                return QuantityLiteral(points[0]['constant'], Millimeter)
            elif height.amount(Millimeter) >= points[-1]['height']:
                return QuantityLiteral(points[-1]['constant'], Millimeter)
            else:
                for i in range(len(points)-1):
                    if points[i]['height'] <= height.amount(Millimeter) and height.amount(Millimeter) <= points[i+1]['height']:
                        f = (height.amount(Millimeter) - points[i]['height']) / float(points[i+1]['height']-points[i]['height'])
                        return QuantityLiteral( (1-f) * points[i]['constant'] + f * points[i+1]['constant'], Millimeter)
                return None
        return create_calibrator

    def provide_jsonpickle_serializer(self):
        class JSONPickleSerializer:
            def serialize(self, obj):
                return jsonpickle.dumps(obj).encode('utf8')

            def deserialize(self, graph):
                return jsonpickle.loads(graph.decode('utf8'))
        return JSONPickleSerializer()

    @inject(['filesystem', 'jsonpickle_serializer'])
    @provides(in_scope=PROTOTYPE)
    def provide_standard_file_repository(self, folder, filesystem, jsonpickle_serializer):
        return FileRepository(jsonpickle_serializer, FileRepositoryConfig(folder), filesystem, primaryKeyGenerator=lambda: str(uuid.uuid4()))

    def provide_prototype_repository(self, provide_standard_file_repository):
        return provide_standard_file_repository(folder='/mnt/hard/repos/prototypes')

    def provide_user_repository(self, provide_standard_file_repository):
        return provide_standard_file_repository(folder='/mnt/hard/repos/users')

    def provide_calibration_r_repository(self, provide_standard_file_repository):
        return provide_standard_file_repository(folder='/mnt/hard/repos/calibration_r')

    def provide_calibration_h_repository(self, provide_standard_file_repository):
        return provide_standard_file_repository(folder='/mnt/hard/repos/calibration_h')

    def provide_calibrate_height(self, calibration_h_repository):
        def create_calibrator(height):
            points = sorted([x.data for x in calibration_h_repository.all()], key=lambda point: point['height'])
            if len(points) < 1:
                return None
            elif len(points) == 1:
                return QuantityLiteral(points[0]['constant'], Millimeter)
            elif height.amount(Millimeter) <= points[0]['height']:
                return QuantityLiteral(points[0]['constant'], Millimeter)
            elif height.amount(Millimeter) >= points[-1]['height']:
                return QuantityLiteral(points[-1]['constant'], Millimeter)
            else:
                for i in range(len(points)-1):
                    if points[i]['height'] <= height.amount(Millimeter) and height.amount(Millimeter) <= points[i+1]['height']:
                        f = (height.amount(Millimeter) - points[i]['height']) / float(points[i+1]['height']-points[i]['height'])
                        return QuantityLiteral( (1-f) * points[i]['constant'] + f * points[i+1]['constant'], Millimeter)
                return None
        return create_calibrator

    def provide_flash(self, config_manager):
        return Flash(config_manager, 'production', 'platform-3f980000.usb-usb-0:1.3:1.0-scsi-0:0:0:0')

    @provides(in_scope=PROTOTYPE)
    def provide_hw(
        self, machine, lift, config_manager, registered_cams, height_cv, neck_cv, 
        hgauge, vgauge, glue, unit_converter, calibrate_diam_factory, calibrate_height):
        hw = Hw()
        hw.machine = machine
        hw.lift = lift
        hw.config_manager = config_manager
        hw.registered_cams = registered_cams
        hw.height_cv = height_cv
        hw.neck_cv = neck_cv
        hw.hgauge = hgauge
        hw.vgauge = vgauge
        hw.glue = glue
        hw.unit_converter = unit_converter
        hw.calibrate_diam_factory = calibrate_diam_factory
        hw.calibrate_height = calibrate_height
        return hw

    def provide_navigation_service(
        self, machine, lift, config_manager, registered_cams, height_cv, 
        neck_cv, hgauge, glue, unit_converter, calibrate_diam_factory, hw, 
        prototype_repository, user_repository, session, filesystem,
        calibration_r_repository, calibration_h_repository, flash):

        return PetNavigationService(
            machine, lift, config_manager, registered_cams, height_cv, 
            neck_cv, hgauge, glue, unit_converter, calibrate_diam_factory, hw, 
            prototype_repository, user_repository, session, filesystem,
            calibration_r_repository, calibration_h_repository, flash)


hardwareBindingSpec = HardwareBindingSpec()
obj_graph = new_object_graph(binding_specs=[hardwareBindingSpec])
