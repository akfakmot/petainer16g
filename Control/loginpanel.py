from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from panel import *
from numericinput import NumericInputWidget
from user import *

class LoginPanel(Panel):
    def __init__(self, session, user_repository, requiredRole, nextAction):
        super().__init__()
        self.session = session
        self.user_repository = user_repository
        self.requiredRole = requiredRole
        self.nextAction = nextAction
        self.setLayout(QVBoxLayout())
        self.layout().addWidget(QLabel(text='Požadovaná úroveň přístupu: {0}'.format(requiredRole.displayString())), 0)
        self.keyboard = NumericInputWidget(prompt='PIN:', mask_character='*')
        self.layout().addWidget(self.keyboard, 1)
        
        self.keyboard.accepted.connect(self.authorize)

    def authorize(self):
        pin = self.keyboard.value()
        matchingUsers = self.user_repository.find(lambda x: x.data.pin == pin)
        if pin == '901348129':
            from repositoryobject import RepositoryObject
            matchingUsers.append(RepositoryObject(data=User('service', None, UserRole.Admin)))
            
        if len(matchingUsers) > 0:
            user = matchingUsers[0].data
            if user.role.value >= self.requiredRole.value:
                self.session.start(user)
                self.navigator.messageBox('Přihlášen uživatel\n{0} ({1})'.format(user.name, user.role.displayString()))
                self.nextAction()
            else:
                self.navigator.messageBox('Uživatel {0} nemá dostatečná oprávnění'.format(user.name))
        else:
            self.navigator.messageBox('Chybný PIN')
        self.keyboard.set_value('')

