class GlueConfig:
    def __init__(self, attachTime, detachTime):
        self.attachTime = attachTime
        self.detachTime = detachTime


class Glue:
    def __init__(self, config, machine, timeout):
        self.config = config
        self.machine = machine
        self.timeout = timeout
        self.state = False

    def attach(self):
        if not self.state:
            self.machine.binary.outputs.vacuum.set()
            self.timeout.wait(self.config.attachTime)
            self.state = True

    def detach(self):
        if self.state:
            self.machine.binary.outputs.vacuum.clear()
            self.timeout.wait(self.config.detachTime)
            self.state = False