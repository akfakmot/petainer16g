from panel import Panel
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from repositorycollectioneditor import *
from usereditor import *
from repositoryobject import *
from user import *
from calibrationradiuseditor import *
from asyncupdatepanel import *
from quantity import *
from stdsamplesection import *
from radiuscalibrator import *
import json


class CalibrationRadiusPanel(AsyncUpdatePanel):
    usesMachine = True
    def __init__(self, config_manager, config_id, hw, flash):
        super().__init__()
        self.hw = hw
        self.config_manager = config_manager
        self.config_id = config_id
        self.flash = flash
        self.loadConfig()
        l = QGridLayout()
        l.setContentsMargins(0,0,0,0)
        self.setLayout(l)

        self.entry = QWidget(self)
        uic.loadUi('ui/calibrateradiusentry.ui', self.entry)
        self.layout().addWidget(self.entry)

        self.view = QWidget(self)
        uic.loadUi('ui/calibrateradius.ui', self.view)
        self.layout().addWidget(self.view)
        self.view.hide()

        self.entry.calibrate.clicked.connect(self.calibrate)
        self.entry.test.clicked.connect(self.entryTest)
        self.entry.importButton.clicked.connect(self.importButton_clicked)
        self.entry.exportButton.clicked.connect(self.exportButton_clicked)
        self.view.sample.clicked.connect(self.sample)
        self.view.test.clicked.connect(self.test)
        self.view.next.clicked.connect(lambda: self.calibrateN(self.calibrationNumber+1))
        self.view.prev.clicked.connect(lambda: self.calibrateN(self.calibrationNumber-1))
        self.view.cancel.clicked.connect(self.cancel_connect)
        self.view.save.clicked.connect(self.save_clicked)
        self.view.save.hide()
       
    def importButton_clicked(self):
        self.flash.transfer_from_flash(self.navigator.messageBox, '/mnt/hard/config/radius_calib', 'Soubor k importu', '.tcd')
        self.loadConfig()

    def exportButton_clicked(self):
        self.flash.transfer_to_flash(self.navigator.messageBox, '/mnt/hard/config/radius_calib', '_calib_diam', 'Kalibrace průměrů byla exportována', '.tcd')

    def entryTestHeight(self):
        try:
            num = float(self.entry.height.text())
        except ValueError:
            self.navigator.messageBox('Testovací výška musí být číslo')
            return None
        
        return QuantityLiteral(num, Millimeter)

    def entryTest(self):
        if self.totalCalibrator is None:
            self.navigator.messageBox('Zatím nebyla provedena kalibrace')
            return

        entryTestHeight = self.entryTestHeight()
        if entryTestHeight is not None:
            res = {0:0, 1:0}

            def getValue():
                x = None
                while x is None:
                    x = self.hw.hgauge.value()
                return x

            def act0():
                res[0] += getValue()

            def act180():
                res[1] += getValue()

            SAMPLES = 1
            for i in range(SAMPLES):
                self.move(act0, act180, entryTestHeight)
            for key in res:
                res[key] = res[key] / float(SAMPLES)
            
            diamRaw = res[0] + res[1]
        
            print('entryTestHeight',entryTestHeight)
            print('diamRaw',diamRaw)
        
            print('calibrate to',entryTestHeight)
            self.navigator.messageBox('%.2f mm' % self.totalCalibrator.calibrate(entryTestHeight, diamRaw).amount(Millimeter))
            
    def save_clicked(self):
        profile = [
            {
                'height':
                    (h * 51 + 15), 
                'corLin':
                    val['corLin'], 
                'corAbs':
                    val['corAbs'].amount(Millimeter) 
            } for h,val in self.profile.items()]
        self.config_manager.save(self.config_id, json.dumps(profile).encode('utf8'))
        self.cancel_connect()

    def loadConfig(self):
        graph = self.config_manager.load(self.config_id)
        if graph is not None:
            config = json.loads(graph.decode('utf8'))
            config = [{'height':QuantityLiteral( x['height'], Millimeter), 'corLin': x['corLin'], 'corAbs':QuantityLiteral(x['corAbs'], Millimeter)}
                      for x in config]
            self.totalCalibrator = RadiusCalibrator(config)
        else:
            self.totalCalibrator = None

    def cancel_connect(self, *args, **kwargs):
        self.view.hide()
        self.entry.show()
        self.loadConfig()

    def sample(self):
        
        res = {0:0, 1:0}

        def getValue():
            x = None
            while x is None:
                x = self.hw.hgauge.value()
            return x

        def act0():
            res[0] += getValue()

        def act180():
            res[1] += getValue()

        SAMPLES = 4
        for i in range(SAMPLES):
            self.move(act0, act180, self.calibrationHeight())

        for key in res:
            res[key] = res[key] / float(SAMPLES)
            
        bigDiamRaw = res[0] + res[1]

        # small calibration part
        res = {0:0,1:0}
        SAMPLES = 4
        for i in range(SAMPLES):
            self.move(act0, act180, self.calibrationHeight() + QuantityLiteral(15, Millimeter))
        for key in res:
            res[key] = res[key] / float(SAMPLES)
            
        diamRaw = res[0] + res[1]
        b = (diamRaw.amount(Millimeter), 65 - diamRaw.amount(Millimeter))
        a = (bigDiamRaw.amount(Millimeter), 93 - bigDiamRaw.amount(Millimeter))

        div = float(b[0]-a[0])
        if div != 0:
            lin = float(b[1] - a[1]) / div
        else:
            lin = 1
        abs = QuantityLiteral( b[1] - b[0] * lin, Millimeter)

        self.profile[self.calibrationNumber]['corAbs'] = abs
        self.profile[self.calibrationNumber]['corLin'] = lin
        self.profile[self.calibrationNumber]['done'] = True
        self.refreshCalibrator()
        self.calibrateN(self.calibrationNumber)
        #constant = diamCalibrated - diamRaw
        #self.constant.setText('{0:.3f}'.format(constant.amount(Millimeter)))

    def refreshCalibrator(self):
        self.calibrator = RadiusCalibrator(profile=[{'height':QuantityLiteral(self.calibrationNumber * 51 + 15, Millimeter), 'corLin':self.profile[self.calibrationNumber]['corLin'], 'corAbs':self.profile[self.calibrationNumber]['corAbs'] }])

    def testHeight(self):
        if self.view.rdbBig.isChecked():
            return self.calibrationHeight()
        elif self.view.rdbSmall.isChecked():
            return self.calibrationHeight() + QuantityLiteral(15, Millimeter)
        else:
            try:
                num = float(self.view.testHeight.text())
            except ValueError:
                self.navigator.messageBox('Testovací výška musí být číslo')
                return None
            if num > self.calibrationHeight().amount(Millimeter) + 51/2.0 \
                or num < self.calibrationHeight().amount(Millimeter) - 51/2.0:
                self.navigator.messageBox('Testovací výška není v pásmu výšek pro tento počet adaptérů')
                return None
            return QuantityLiteral(num, Millimeter)

    def test(self):
        testHeight = self.testHeight()
        if testHeight is not None:
            res = {0:0, 1:0}

            def getValue():
                x = None
                while x is None:
                    x = self.hw.hgauge.value()
                return x

            def act0():
                res[0] += getValue()

            def act180():
                res[1] += getValue()

            SAMPLES = 1
            for i in range(SAMPLES):
                self.move(act0, act180, self.testHeight())
            for key in res:
                res[key] = res[key] / float(SAMPLES)
            
            diamRaw = res[0] + res[1]
        
            print('testHeight',testHeight)
            print('diamRaw',diamRaw)
        
            self.navigator.messageBox('%.2f mm' % self.calibrator.calibrate(testHeight, diamRaw).amount(Millimeter))

    def calibrate(self):
        self.entry.hide()
        self.view.show()
        self.profile = { i: {'done':False, 'corAbs':QuantityLiteral(0,Millimeter), 'corLin':1}
                        for i in range(6) }
        self.calibrateN(sorted(self.profile.keys())[0])

    def calibrateN(self, n):
        n = min(5, n)
        n = max(0, n)
        self.calibrationNumber = n
        self.showAdapters(self.calibrationNumber)
        self.view.saveLabel.setText('Zbývá kalibrovat s následujícími počty adaptérů: ' + ','.join(str(i) for i in self.profile if not self.profile[i]['done']))
        if all(x['done'] for x in self.profile.values()):
            self.view.save.show()
            self.view.saveLabel.hide()
        else:
            self.view.save.hide()
            self.view.saveLabel.show()
        self.view.sampled.setVisible(self.profile[self.calibrationNumber]['done'])

        h = 'Kalibrace '
        if n == 0:
            h += 'bez adaptérů'
        else:
            if n in (1,5):
                h += 's '
            else:
                h += 'se '
            h += str(n) + ' '
            if n == 1:
                h += 'adaptérem'
            elif n in (2,3,4,5,6):
                h += 'adaptéry'
        h += '\npro pásmo výšek'
        h += '\n%.1f +/- %.1f mm' % (self.calibrationHeight().amount(Millimeter), 51/2.0)
        self.view.header.setText(h)
        self.refreshCalibrator()

    def calibrationHeight(self):
        return QuantityLiteral(7.5 + 52.1 * self.calibrationNumber, Millimeter)

    def move(self, act0, act180, height):
        try:
            self.hw.lift.reconfigure(height)
            while not self.hw.lift.done():
                pass
            self.hw.glue.attach()
            self.hw.hgauge.activate()
            act0()
            self.hw.hgauge.deactivate()
            self.hw.machine.plate.set(QuantityLiteral(180, Degrees), speed=20, relative=True)
            while self.hw.machine.plate.isMoving():
                pass
            self.hw.hgauge.activate()
            act180()
            self.hw.hgauge.deactivate()
            self.hw.glue.detach()
            self.hw.machine.plate.set(QuantityLiteral(-180, Degrees), speed=50, relative=True)
            while self.hw.machine.plate.isMoving():
                pass
        finally:
            self.hw.hgauge.deactivate()
            self.hw.glue.detach()

    def showAdapters(self, n):
        for i in range(6):
            adapterName = 'adapter%s'%i
            print(adapterName)
            adapter = self.view.findChild(QFrame, adapterName)
            gumName = 'gum%s'%i
            print(gumName)
            gum = self.view.findChild(QFrame, gumName)
            visible = i >= (6-n)
            adapter.setVisible(visible)
            gum.setVisible(visible)
        self.view.frame.move(0, (6-n-1)*61+40)
"""
class CalibrationRadiusPanel(Panel):
    usesMachine = True
    def __init__(self, repository, hw):
        super().__init__()
        self.repository = repository
        self.hw = hw
        self.setLayout(QGridLayout())
        self.editor = RepositoryCollectionEditor(
            CalibrationRadiusEditor(hw), 
            lambda: self.navigator.messageBox, 
            lambda x: '{0:.2f}'.format(x.data['height']), 
            repository,  
            DictionaryEditAdapter(self.provideEmptyData), 
            sortKey=lambda x: x.data['height'],
            title='Kalibrace průměru')

        self.layout().addWidget(self.editor)


    def provideEmptyData(self):
        return { 'height':0, 'calibrated':0, 'constant':0 }"""