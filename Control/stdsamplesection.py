from samplesection import *
from uniformangledirector import *


class StdSampleSection(SampleSection):
    def __init__(self, hw, height, sampleCount, speed):
        super().__init__(hw, SampleSectionConfig(UniformAngleDirector(360/float(sampleCount)), speed, True), height)


