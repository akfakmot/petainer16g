from panel import Panel
from functools import *
from quantity import *
from threading import Lock
from PyQt5 import QtCore
from qt_ui_load_decorator import loadUi
from PyQt5.QtWidgets import *
import json


@loadUi('ui/manuallift.ui')
class ManualLiftPanel(Panel):
    usesMachine = True
    def __init__(self, machine, lift, config_manager, config_id, title=''):
        super().__init__()
        self.machine = machine
        self.lift = lift
        self.title = title
        self.config_manager = config_manager
        self.config_id = config_id
        configGraph = config_manager.load(config_id)
        if configGraph is not None:
            self.config = json.loads(configGraph.decode('utf8'))
        else:
            self.config = dict()
            self.config['centeringCam'] = 0
            self.config['heightCam'] = 0

    def configSave(self):
        self.config_manager.save(self.config_id, json.dumps(self.config).encode('utf8'))

    def teachCenteringCam_clicked(self, *args, **kwargs):
        try:
            self.config['centeringCam'] = self.lift.position().amount(Millimeter) - self.navigator.floatInput('Výška založené lahve [mm]')
            self.configSave()
        except ValueError:
            self.navigator.messageBox('Chybně zadané číslo')
    
    def teachHeightCam_clicked(self, *args, **kwargs):
        try:
            self.config['heightCam'] = self.lift.position().amount(Millimeter) - self.navigator.floatInput('Výška založené lahve [mm]')
            self.configSave()
        except ValueError:
            self.navigator.messageBox('Chybně zadané číslo')
    
    def gotoCenteringCam_clicked(self, *args, **kwargs):
        try:
            self.lift.reconfigureSpecial('CenteringCam', bottleHeight=QuantityLiteral(self.navigator.floatInput('Výška založené lahve [mm]'), Millimeter))
        except ValueError:
            self.navigator.messageBox('Chybně zadané číslo')

    def gotoHeightCam_clicked(self, *args, **kwargs):
        try:
            self.lift.reconfigureSpecial('HeightCam', bottleHeight=QuantityLiteral(self.navigator.floatInput('Výška založené lahve [mm]'), Millimeter))
        except ValueError:
            self.navigator.messageBox('Chybně zadané číslo')

    def initUi(self):
        self.up.pressed.connect(partial(self.lift.jogp,10))  
        self.up.released.connect(self.lift.jogcancel)
        self.down.pressed.connect(partial(self.lift.jogn,10))  
        self.down.released.connect(self.lift.jogcancel)
        self.upSlow.pressed.connect(partial(self.lift.jogp,3))  
        self.upSlow.released.connect(self.lift.jogcancel)
        self.downSlow.pressed.connect(partial(self.lift.jogn,3))  
        self.downSlow.released.connect(self.lift.jogcancel)
        self.go.clicked.connect(self.goto)
        self.cancelMove.clicked.connect(self.lift.cancel)
        self.power.clicked.connect(self.power_clicked)
        self.teachCenteringCam.clicked.connect(self.teachCenteringCam_clicked)
        self.teachHeightCam.clicked.connect(self.teachHeightCam_clicked)
        self.gotoCenteringCam.clicked.connect(self.gotoCenteringCam_clicked)
        self.gotoHeightCam.clicked.connect(self.gotoHeightCam_clicked)

        from PyQt5.QtCore import QTimer
        self.timer = QTimer(self)
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.refresh, QtCore.Qt.QueuedConnection)

        self.lock = Lock()    

    def power_clicked(self):
        if self.power.isChecked():
            self.machine.liftDrive.setup()
        else:
            self.machine.liftDrive.shut()
        self.refreshPower()

    def goto(self):
        self.lift.reconfigure(QuantityLiteral(float( self.targetPosition.text()), Millimeter),
                                int(self.targetSpeed.text()))
        
    def activate(self):
        super().activate()
        self.refreshPower()
        self.timer.start()
        self.lift.vgauge_lock = False

    def deactivate(self):
        self.timer.stop()
        self.lift.vgauge_lock = True

    def refresh(self):
        if not hasattr(self, 'ctr'):
            self.ctr = 0
        self.ctr += 1
        self.position.setText('%.2f mm' % self.lift.position().amount(Millimeter))

    def refreshPower(self):
        self.power.setChecked(self.machine.liftDrive.ready())
        self.controlFrame.setEnabled(self.machine.liftDrive.ready())
