from panel import *
from widgets.choice import *
import jsonpickle


class ProductionTypePanel(Panel):
    def __init__(self, prototype_repository, config_manager, config_id):
        super().__init__()
        self.prototype_repository = prototype_repository
        self.config_manager = config_manager
        self.config_id = config_id
        self.list = ChoiceList()
        self.list.setItems(sorted(map(lambda x: x.data, self.prototype_repository.all()), key=lambda x:x['name']), lambda x: x['name'])
        self.list.itemSelected.connect(self.typeSelected)
        self.list.canceled.connect(lambda: self.navigator.redirect('empty'))
        self.setLayout(QGridLayout())
        self.layout().addWidget(self.list)

    def activate(self):
        super().activate()
        configGraph = self.config_manager.load(self.config_id)
        if configGraph is None:
            self.navigator.redirect('error', text='Není k dispozici konfigurace')
            self.navigator.redirect('empty')
        else:
            config = jsonpickle.loads(configGraph.decode('utf8'))
            self.navigator.messageBox('Aktuální typ = ' + config['type'] + '\nVyberte nový typ')

    def typeSelected(self, type):
        self.navigator.messageBox('Vybrán typ ' + type['name'])
        configGraph = self.config_manager.load(self.config_id)
        if configGraph is None:
            self.navigator.redirect('error', text='Není k dispozici konfigurace')
            self.navigator.redirect('empty')
        else:
            config = jsonpickle.loads(configGraph.decode('utf8'))
            config['type'] = type['name']
            self.config_manager.save(self.config_id, jsonpickle.dumps(config).encode('utf8'))
            self.navigator.redirect('empty')


