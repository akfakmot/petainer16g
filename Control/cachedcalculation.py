def cachedCalculation(cacheName):
    def wrap(method):
        def wrapper(self):
            fullCacheName = '_cached_' + cacheName
            if not hasattr(self, fullCacheName):
                setattr(self, fullCacheName, method(self))
            return getattr(self, fullCacheName)
        return wrapper
    return wrap