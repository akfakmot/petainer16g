import fhpp.client
from utility import modulo
from autologging import logged, traced

@logged
@traced
class FestoMotor:
    def __init__(self, host, port=502, isRotary=False, minimumRaw=None, maximumRaw=None):
        self.fhpp = fhpp.client.Client(host, port, minimumRaw, maximumRaw)
        self.isRotary = isRotary
        self.zeroOffset = 0

    def standby(self):
        self.fhpp.standby()
    
    def idle(self):
        #self.fhpp.reset()
        self.fhpp.idle()
    
    def jogp(self, speed=5):
        self.fhpp.jogp(speed)

    def jogn(self, speed=5):
        self.fhpp.jogn(speed)

    def jogcancel(self):
        self.fhpp.jogcancel()

    def halt(self):
        self.fhpp.halt()
    
    def clear(self):
        self.fhpp.clear()

    def homed(self):
        return self.fhpp.homed()

    def home(self):
        self.fhpp.home()
    
    def motionComplete(self):
        return self.fhpp.motionComplete()
    
    def waitMotionComplete(self, idleOp=None):
        while not self.motionComplete():
            if idleOp is not None:
                idleOp()

    def set(self, position, speed=100, relative=False):
        if not self.isRotary:
            self.fhpp.set(position, speed, relative)
        else:
            if relative:
                self.fhpp.set(position, speed, True)
            else:
                self.fhpp.set(modulo.nearestEquivalent(self.fhpp.get(), position, 100000), speed, False)
               
    def get(self):
        return self.fhpp.get()
                
                
