from time import time, sleep


class Timeout:
    def wait(self, seconds):
        sleep(seconds)
    
    def waitOrFail(self, condition, timeoutSeconds, message):
        start = time()
        while not condition():
            if time() - start >= timeoutSeconds:
                raise Exception(message)


