menuItems = (
                ('Měření', [
                    ('Produkce', 'production'), 
                    ('Výběr typu', 'production_type'), 
                    ('Exportovat záznamy', 'export_records'),
                    ('Vymazat záznamy', 'clear_records'),
                    ]),
                ('Systém', [
                    ('Talíř', 'control_plate'),
                    ('Výtah', 'control_lift'),
                    ('Vstupy, výstupy', 'control_io'),
                    ('Kamera středicí', 'control_cam_centering'),
                    ('Kamera výšková', 'control_cam_height'),
                    ('Operace', 'operations'),
                    ('Vypnout', 'shutdown'),
                ]),
                ('Konfigurace', [
                    ('Nastavení výroby', 'production_config'),
                    ('Přiřazení kamer', 'cam_config'),
                ]),
                ('Definice', [
                    ('Typy lahví', 'def_bottles'),
                    ('Uživatelé', 'def_users'),
                    ('Kalibrace průměru', 'def_calibration_r'),
                    ('Kalibrace výšky', 'def_calibration_h'),
                ]),
                ('Flashdisk', [
                    ('Vysunout', 'eject_flashdisk'),
                ]),
            )