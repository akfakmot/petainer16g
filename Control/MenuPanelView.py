from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
import qtutil
import functools
from autologging import *
import math


@traced
@logged
class MenuPanelView(QFrame):
    
    def __init__(self):
        super().__init__()
        self.buttons = []
        self.setLayout(QGridLayout())
        
    def setModel(self, model):
        self.model = model
        self.refresh()
        def refresh(event):
            self.refresh()
        self.model.items.attach(refresh)
        
    def itemsChangedHandler(self, sender, event, *args, **kwargs):
        self.refresh()

    def dispatchItemClicked(self, itemIndex, *args, **kwargs):
        self.model.itemSelected(itemIndex)

    def refresh(self):  
        qtutil.clearLayout(self.layout())

        rows = int(math.sqrt(len(self.model.items)))
        if rows > 0:
            cols = (len(self.model.items)-1)/rows+1
            i = 0
            for item in self.model.items:
                b = QPushButton()
                b.setText(item)
                b.setSizePolicy(QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding))
                b.clicked.connect(functools.partial(self.dispatchItemClicked, i))
                self.layout().addWidget(b, i/rows, i%rows)
                self.buttons.append(b)
                i += 1

    