from asyncupdatepanel import AsyncUpdatePanel
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from repositorycollectioneditor import *
from usereditor import *
from repositoryobject import *
from user import *
from calibrationheighteditor import *



class CalibrationHeightPanel(AsyncUpdatePanel):
    usesMachine = True
    def __init__(self, repository, hw, flash):
        super().__init__()
        self.repository = repository
        self.hw = hw
        self.flash = flash
        self.setLayout(QGridLayout())
        self.contentEditor = CalibrationHeightEditor(hw)

        importExport = [
            ('Import vše', self._import),
            ('Export vše', self._export)]

        self.editor = RepositoryCollectionEditor(
            self.contentEditor, 
            lambda: self.navigator.messageBox, 
            lambda x: '{0:.2f} mm'.format(x.data['calibrated']), 
            repository,  
            DictionaryEditAdapter(self.provideEmptyData), 
            sortKey=lambda x: x.data['calibrated'],
            title='Kalibrace výšky',
            customActions=importExport)

        self.layout().addWidget(self.editor)


    def provideEmptyData(self):
        return { 'height':0, 'constant':0, 'calibrated':0 }

    def activate(self):
        super().activate()
        self.contentEditor.navigator = self.navigator
        self.contentEditor.invoke = self.invoke
        self.contentEditor.invokeLater = self.invokeLater
        self.contentEditor.activate()
        self.hw.machine.binary.outputs.light.set()

    def deactivate(self):
        super().deactivate()
        self.contentEditor.deactivate()
        self.hw.machine.binary.outputs.light.clear()

    def _import(self):
        self.flash.transfer_from_flash(self.navigator.messageBox, '/mnt/hard/repos/calibration_h', 'Soubor k importu', '.tch', True)
        self.editor.refresh()

    def _export(self):
        self.flash.transfer_to_flash(self.navigator.messageBox, '/mnt/hard/repos/calibration_h', '_calib_height', 'Kalibrace výšky byla exportována', '.tch', True)