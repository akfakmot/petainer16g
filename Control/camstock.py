from camv4l2 import *
from camlist import *


class CamStock:
    def __init__(self):
        self.camList = camlist()
        self.camDevs = dict()

    def cameraList(self):
        return self.camList

    def camera(self, id_path, resolution):
        if id_path in self.camDevs and not all(self.camDevs[id_path]['resolution'][i] == resolution[i] for i in range(2)):
            print('camera resolution mismatch')
            self.camDevs[id_path]['dev'].release()
            self.camDevs.pop(id_path)

        if not id_path in self.camDevs:
            print('camera not yet registered')
            entry = next(filter(lambda c: c[1]['ID_PATH'] == id_path, self.camList.items()))
            dev = CamV4L2(entry[0], resolution[0], resolution[1])
            dev.setup()
            self.camDevs[id_path] = dict(dev=dev, resolution=resolution)

        return self.camDevs[id_path]['dev']