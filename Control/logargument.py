import autologging
import logging


def logLevelArg(argparser):
    argparser.add_argument('-l', '--log', default='DEBUG', nargs='?', choices=['TRACE','DEBUG','INFO','WARN','ERROR'], help='Logging level')

def logLevelChoice(args):
    level = args.log
    if level == 'TRACE':
        return autologging.TRACE
    else:
        return getattr(logging, level)
