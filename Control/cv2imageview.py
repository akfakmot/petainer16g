from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from asyncupdatepanel import *
import numpy as np
import time
import cv2
import math


class MouseDisplay(QLabel):
    def __init__(self):
        super().__init__()
        self.pointA = None
        self.pointB = None

    def mousePressEvent(self, e):
        if self.pixmap() is None: return
        point = tuple(int(x) for x in (e.x() / float(self.width()) * self.pixmap().size().width(), e.y() / float(self.height()) * self.pixmap().size().height()))
        if e.button() == Qt.LeftButton:
            self.pointA = point
        elif e.button() == Qt.RightButton:
            self.pointB = point


class Cv2ImageView(QFrame):
    def __init__(self):
        super().__init__()
        self.setLayout(QGridLayout())
        self.display = MouseDisplay()
        self.display.setScaledContents(True)
        self.display.setMinimumSize(1,1)
        self.layout().addWidget(self.display, 0, 0)
        infoLayout = QGridLayout()
        self.info = QLabel()
        self.info.setVisible(False)
        infoLayout.addWidget(self.info, 0, 1, 1, 1)
        spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        infoLayout.addItem(spacerItem, 0, 0, 1, 1)
        spacerItem1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        infoLayout.addItem(spacerItem1, 1, 1, 1, 1)
        
        #infoLayout.addItem(QSpacerItem(0,0,QSizePolicy.Expanding,QSizePolicy.Minimum),0,0,1,2)
        #infoLayout.addItem(QSpacerItem(0,0,QSizePolicy.Minimum,QSizePolicy.Expanding),1,1,2,1)
        self.layout().addLayout(infoLayout, 0, 0)
        

    def setImage(self, img):
        src = np.require(img, np.uint8, 'C')
        src = cv2.cvtColor(src, cv2.COLOR_BGR2RGB)
        if self.display.pointA is not None and self.display.pointB is not None:
            cv2.line(src, self.display.pointA, self.display.pointB, (0,255,0), 1)
            length = int(math.sqrt((self.display.pointA[0]-self.display.pointB[0])**2 + (self.display.pointA[1]-self.display.pointB[1])**2))
            self.info.setText('%s px' % int(length))
            self.info.setVisible(True)
        else:
            self.info.setVisible(False)
        self.display.setPixmap(QPixmap.fromImage(QImage(src.data, src.shape[1], src.shape[0], src.strides[0], QImage.Format_RGB888)))
        

    