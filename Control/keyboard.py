from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi


@loadUi('ui/keyboard.ui')
class Keyboard(QWindow):
    def __init__(self):
        super().__init__()
     
    def initUi(self):
        for btn in self.findChildren(QPushButton):
            btn.clicked.append(self.type)

    def open(self, callback, default):
        self.text = default
        self.callback = callback

    def type(self, *args, **kwargs):
        self.text += self.sender().text()
        self.callback(self.text)
    


