from shell import shellCommand
from glob import glob


def camlist():
    nodes = glob('/dev/video*')
    map = dict()
    for node in nodes:
        out, err = shellCommand('udevadm info -q property -n {0}'.format(node))
        props = dict((fields[0], fields[1]) for fields in (line.split('=') for line in out.decode('utf8').split('\n') if line.strip() != ''))
        map[node] = props

    return map