from quantity import *


class RadiusCalibrator(object):
    def __init__(self, profile):
        self.profile = sorted(profile, key=lambda point: point['height'].amount(Millimeter))

    def calibrate(self, height, rawValue):
        corAbs = self.interpolate('corAbs', height)
        corLinear = self.interpolate('corLin', height)

        res = rawValue + corAbs + rawValue * corLinear
        return res

    def interpolate(self, key, height):
        points = self.profile
        print('points[0][height]',points[0]['height'])
        if len(points) < 1:
            return None
        elif len(points) == 1:
            return points[0][key]
        elif height.amount(Millimeter) <= points[0]['height'].amount(Millimeter):
            return points[0][key]
        elif height.amount(Millimeter) >= points[-1]['height'].amount(Millimeter):
            return points[-1][key]
        else:
            for i in range(len(points)-1):
                if points[i]['height'].amount(Millimeter) <= height.amount(Millimeter) and height.amount(Millimeter) <= points[i+1]['height'].amount(Millimeter):
                    f = (height.amount(Millimeter) - points[i]['height'].amount(Millimeter)) / float(points[i+1]['height'].amount(Millimeter)-points[i]['height'].amount(Millimeter))
                    return points[i][key] * (1-f) + points[i+1][key] * f
            return None