from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from navigation import *
from functools import *
from configpanel import *
from user import *
import os

class ConjugatedPanelProvider:
    """ panels: sequence of tuple(key,panel) """
    def __init__(self, panels=()):
        self.panels = panels
        self.pulledPanels = []

    def pull(self, key):
        try:
            panelEntry = next(filter(lambda p: p[0] == key, self.panels))
            self.panels.remove(panelEntry)
            self.pulledPanels.append(panelEntry)
            return (panelEntry[1], panelEntry[2])
        except Exception as x:
            print(x)
            raise

    def push(self, panel):
        panelEntry = next(filter(lambda p: p[1] == panel, self.pulledPanels))
        self.pulledPanels.remove(panelEntry)
        self.panels.append(panelEntry)

    def keys(self):
        return map(lambda x: x[0], tuple(self.panels) + tuple(self.pulledPanels))


class PetNavigationService(NavigationService):
    def __init__(
        self, machine, lift, config_manager, registered_cams, height_cv, neck_cv, 
        hgauge, glue, unit_converter, calibrator, hw, prototype_repository, user_repository, session,
        filesystem, calibration_r_repository, calibration_h_repository, flash):
        super().__init__()

        self.machine = machine
        self.lift = lift
        self.config_manager = config_manager
        self.registered_cams = registered_cams
        self.calibrator = calibrator
        self.hw = hw
        self.session = session
        self.user_repository = user_repository
        self.flash = flash

        from manualplatepanel import ManualPlatePanel
        from manualiopanel import ManualIOPanel
        from manualliftpanel import ManualLiftPanel
        from imagepanel import ImagePanel
        from camv4l2 import CamV4L2
        from camconfigpanel import CamConfigPanel
        from centeringcampanel import CenteringCamPanel
        from heightcampanel import HeightCamPanel
        from operationspanel import OperationsPanel
        from prototypeeditorpanel import PrototypeEditorPanel
        from usereditorpanel import UserEditorPanel
        from productionpanel import ProductionPanel
        from productionconfigpanel import ProductionConfigPanel
        from calibrationradiuspanel import CalibrationRadiusPanel
        from calibrationheightpanel import CalibrationHeightPanel
        from flash import Flash
        from productiontypepanel import ProductionTypePanel

        self.panelFactory = { 'control_plate': (lambda: ManualPlatePanel(self.machine)),
                        'control_lift': (lambda: ManualLiftPanel(machine, lift, self.config_manager, 'lift')),
                        'control_io': (lambda: ManualIOPanel(machine)),
                        'control_cam_centering': (lambda: CenteringCamPanel(config_manager, 'centering_cam', registered_cams, neck_cv)), 
                        'control_cam_height': (lambda: HeightCamPanel(config_manager, 'height_cam', registered_cams, height_cv)), 
                        'cam_config': (lambda: CamConfigPanel(config_manager, 'cam_assignment', machine.cam_stock, (('centering', 'Středicí kamera'), ('height', 'Výšková kamera')), (320,240))),
                        'production_config': (lambda: ProductionConfigPanel(config_manager, 'production')),
                        'operations': (lambda: OperationsPanel(hgauge, lift, glue, machine, unit_converter, height_cv, hw, prototype_repository)),
                        'def_bottles': (lambda: PrototypeEditorPanel(flash, prototype_repository)),
                        'def_users': (lambda: UserEditorPanel(flash, user_repository)),
                        'def_calibration_r': (lambda: CalibrationRadiusPanel(config_manager, 'radius_calib', hw, flash)),
                        'def_calibration_h': (lambda: CalibrationHeightPanel(calibration_h_repository, hw, flash)),
                        'production': (lambda: ProductionPanel(hw, session, filesystem, prototype_repository, config_manager, 'production')),
                        'production_type': (lambda: ProductionTypePanel(prototype_repository, config_manager, 'production')),
                        }

        self.defaultRequiredRole = UserRole.Operator
        self.protection = { 'def_users': UserRole.Admin,
                          'control_plate' : UserRole.Admin,
                         'control_lift' : UserRole.Admin,
                         'control_io' : UserRole.Admin,
                         'control_cam_centering' : UserRole.Admin,
                         'control_cam_height' : UserRole.Admin,
                         'operations' : UserRole.Admin,
                         'production_config' : UserRole.Admin,
                         'cam_config' : UserRole.Admin,
                         'def_bottles' : UserRole.Admin,
                         'def_users' : UserRole.Admin,
                         'def_calibration_r' : UserRole.Admin,
                         'def_calibration_h' : UserRole.Admin,
                         'production_type' : UserRole.Foreman,
                         
                          }
        #self.protection = {}

        self.viewPopulation = dict()

    def panel(self, key):
        try:
            print('Picking panel from', self.panelFactory.keys())
            result = self.panelFactory[key]()
            return result
        except KeyError as e:
            print('KeyError', e)
            return None
        
    def redirect(self, view, request, showPanel, *args, **kwargs):
        if request in self.protection:
            requiredRole = self.protection[request]
        else:
            requiredRole = self.defaultRequiredRole
        if requiredRole is not None:
            print('self.session.user = ',self.session.user)
            print('self.session.user.role.value = ',self.session.user and self.session.user.role.value)
            print('requiredRole.value = ',requiredRole.value)

            if (not self.session.user) or (self.session.user.role.value < requiredRole.value):
                print('not sufficient')
                from loginpanel import LoginPanel
                showPanel(LoginPanel(self.session, self.user_repository, requiredRole, lambda: self.redirect(view, request, showPanel, *args, **kwargs)))
                return

        print('getting panel for request', request)
        panel = self.panel(request)
        print('got panel', panel)
        if panel is not None:
            if getattr(panel, 'usesMachine', False):
                if not self.hw.machine.setupFinished():
                    self.hw.machine.setup()
                    while not self.hw.machine.setupFinished():
                        pass
            owningView = list(filter(lambda kv: isinstance(kv[1], type(panel)), self.viewPopulation.items()))
            if len(owningView) != 0 and owningView[0][0] != view:
                showPanel( QLabel(text='Tento panel nelze zobrazit dvakrát') )
            else:
                self.viewPopulation[view] = panel
                showPanel(panel)
        else:
            self.viewPopulation[view] = None
            if hasattr(self, request):
                print('hasattr')
                panel = getattr(self, request)(*args, **kwargs)
                if panel is not None:
                    showPanel(panel)
                else:
                    pass
            else:
                print('hasnotattr')

                showPanel( QLabel(text='Neznámý požadavek') )

    def shutdown(self):
        self.machine.release()
        os.system('sudo shutdown now')
        return QLabel('Stanice se vypíná')

    def error(self, text):
        return QLabel('Chyba při vytváření panelu:\n' + text)
    
    def empty(self):
        return QLabel()

    def export_records(self):
        self.flash.transfer_to_flash(self.messageBox, '/mnt/hard/record.txt', '_mereni', 'Zaznamenaná data byla exportována', '.csv')

    def clear_records(self):
        if os.path.exists('/mnt/hard/record.txt'):
            os.remove('/mnt/hard/record.txt')
        self.messageBox('Záznamy měření byly smazány')

    def eject_flashdisk(self):
        self.flash.eject(self.messageBox)