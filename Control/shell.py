from subprocess import Popen, PIPE


def shellCommand(cmd):
    p = Popen(cmd, stdout=PIPE, shell=True)
    return p.communicate()

