from samplesection import *
from uniformangledirector import *
from utility.language import *


class SampleSectionFactory(object):
    speed = 30

    def __init__(self, hgauge, lift, glue, machine, unit_converter):
        self.hgauge = hgauge
        self.lift = lift
        self.glue = glue
        self.machine = machine
        self.unit_converter = unit_converter

    def create(self, height, division):
        return SampleSection(self.hgauge, self.lift, self.glue, self.machine, self.unit_converter, 
                                SampleSectionConfig(UniformAngleDirector(division), 
                                                    self.speed, True),
                                height)
        


