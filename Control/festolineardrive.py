from machine import *
from festomotor import FestoMotor
from quantity import *
from autologging import *
import pinject


class FestoLinearDriveConfig:
    def __init__(self, host=None, port=502, abs=0, lin=1, minimumRaw=None, maximumRaw=None):
        self.host = host
        self.port = port
        self.abs = abs
        self.lin = lin
        self.minimumRaw = minimumRaw
        self.maximumRaw = maximumRaw


@traced
@logged
class FestoLinearDrive(LinearDrive):
    @pinject.copy_args_to_internal_fields
    def __init__(self, config, unit_converter):
        super().__init__()
        self._movingDir = MovingDirection.Zero
        self._ready = False
        self.driver = FestoMotor(self._config.host, self._config.port, False, self._config.minimumRaw, self._config.maximumRaw)

    def setup(self):
        self.driver.standby()
        if not self.driver.homed():
            self.driver.home()
        self._ready = True

    def release(self):
        self.shut()

    def ready(self):
        return self._ready
    
    def jogp(self, speed=5):
        self.driver.jogp(speed)

    def jogn(self, speed=5):
        self.driver.jogn(speed)

    def jogcancel(self):
        self.driver.jogcancel()

    def cancel(self):
        self.driver.halt()

    def shut(self):
        self.driver.idle()
        self._ready = False
    
    def setupFinished(self):
        return self.driver.motionComplete()

    def set(self, position, speed=100, relative=False):
        assert isinstance(position, QuantityEntry)
        if not relative:
            current = self._unit_converter.convertQuantity(self.position(), Micrometer)
            target = self._unit_converter.convertQuantity(position, Micrometer)
            if current < target:
                self._movingDir = MovingDirection.Positive
            else:
                self._movingDir = MovingDirection.Negative

            target.setValue(self._config.abs + self._config.lin * target.value())
            self.driver.set(target.value(), speed, False)        
        else:
            self._movingDir = MovingDirection.Positive if position > QuantityLiteral(0,Millimeter) else MovingDirection.Negative
            
            self.driver.set(self._config.lin * position.amount(Micrometer), speed, True)        

    def position(self):
        return QuantityLiteral(value=(self.driver.get() - self._config.abs) / self._config.lin, unit=Micrometer)
    
    def movingDir(self):
        if self.driver.motionComplete():
            self._movingDir = MovingDirection.Zero
        return self._movingDir