from repositoryobject import RepositoryObject
from typing import Union, Callable, Any


class Repository:
    def __init__(self, primaryKeyGenerator: Callable[[],Any]):
        self.primaryKeyGenerator = primaryKeyGenerator
        
    def save(self, obj:Union[any,RepositoryObject]):
        if not isinstance(obj, RepositoryObject):
            wrappedObj = self.new()
            wrappedObj.data = obj
            obj = wrappedObj

        if obj.primaryKey is None:
            obj.primaryKey = self.primaryKeyGenerator()

        self._save(obj)
        return obj
    
    """Saves new or overwrites object identified by primary key"""
    def _save(self, obj:RepositoryObject):
        raise NotImplementedError()

    """Gets object identified by primary key or throws exception"""
    def get(self, primaryKey) -> RepositoryObject:
        raise NotImplementedError()

    """Returns new object with unique primary key"""
    def new(self) -> RepositoryObject:
        return RepositoryObject(primaryKey=self.primaryKeyGenerator(), repository=self)

    def delete(self, primaryKey):
        raise NotImplementedError()
    
    def find(self, predicate :Callable[[RepositoryObject],bool]) -> RepositoryObject:
        raise NotImplementedError()
        
    """Returns list of all primary keys"""
    def list(self):
        raise NotImplementedError()

    """Returns list of all objects (loaded)"""
    def all(self):
        raise NotImplementedError()