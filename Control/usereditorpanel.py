from panel import Panel
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from repositorycollectioneditor import *
from usereditor import *
from repositoryobject import *
from user import *


class UserEditAdapter:
    def prepareEdit(self, obj):
        return User(name=obj.name, pin=obj.pin,role=obj.role)

    def finishEdit(self, obj, data):
        obj.name = data.name
        obj.pin = data.pin
        obj.role = data.role

    def create(self):
        return User()


class UserEditorPanel(Panel):
    def __init__(self, flash, repository):
        super().__init__()
        self.flash = flash
        self.repository = repository
        self.setLayout(QGridLayout())

        importExport = [
            ('Import vše', self._import),
            ('Export vše', self._export)]

        self.editor = RepositoryCollectionEditor(
            UserEditor(), 
            lambda: self.navigator.messageBox, 
            lambda x: x.data.name, 
            repository,  
            UserEditAdapter(), 
            sortKey=lambda x: x.data.name,
            title='Uživatelé',
            customActions=importExport)

        self.layout().addWidget(self.editor)
    
    def _import(self):
        self.flash.transfer_from_flash(self.navigator.messageBox, '/mnt/hard/repos/users', 'Soubor k importu', '.tcu', True)
        self.editor.refresh()

    def _export(self):
        self.flash.transfer_to_flash(self.navigator.messageBox, '/mnt/hard/repos/users', '_users', 'Seznam uživatelů byl exportován', '.tcu', True)