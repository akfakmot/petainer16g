class UniformAngleDirector:
    def __init__(self, division):
        self.division = division
        self.reset()

    def next(self):
        angle = self.current
        if angle is None:
            return None
        self.current += self.division
        if self.current >= 360:
            self.current = None
        return angle

    def reset(self):
        self.current = 0
        


