from quantity import *
from autologging import logged, traced
import sys
import time
from stdsamplesection import *
import hwmonitor


@traced
@logged
class CenterSection(object):
    def __init__(self, hw, height, sampleCount, speed):
        self.hw = hw
        self.height = height
        self.sampleCount = sampleCount
        self.speed = speed

    def exec(self):
        print('sampling')
        sampler = StdSampleSection(self.hw, self.height, self.sampleCount, self.speed)
        sampler.exec()
        print('sampled')
        polarCenter = sampler.result.centerPolar()
        maxAngle = sampler.result.maxPeak()
        self.hw.glue.attach()
        self.hw.machine.plate.set(QuantityLiteral(maxAngle-90, Degrees))
        print('checkpoint 1')
        while self.hw.machine.plate.isMoving():
            hwmonitor.check()
        self.hw.glue.detach()
        print('maxAngle=',maxAngle)
        print('samples: ', sampler.result.samples())
        incDiam = sampler.result.diamAt(maxAngle)  #incident diameter
        print('incDiam=',incDiam)
        push = self.hw.machine.specs['plate']['radius']-incDiam*0.5
        print('push=',push)
        self.hw.machine.slider.set(push, 1)
        print('pushing...')
        print('checkpoint 2')
        while not self.hw.machine.slider.done():
            hwmonitor.check()
        print('homing...')
        self.hw.machine.slider.home()
        #while not self.hw.machine.slider.done():
        #    pass
       