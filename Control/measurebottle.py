﻿import pinject
from samplesection import *
from stdsamplesection import *
from centersection import *
from uniformangledirector import *
from centerneck import *
from section import *
import jsonpickle
import hwmonitor
import math


class MeasureBottle(object):
    def __init__(self, hw, prototype, showImg, hideImg):
        self.hw = hw
        self.prototype = prototype
        self.calibrateHeight = self.hw.calibrate_height
        self.showImg = showImg
        self.hideImg = hideImg

    rotSpeed = 70
    def exec(self, _report=None):
        print('exec',_report)
        def report(message):
            print('report... _report=',_report)
            _report and _report(message)

        self.hw.machine.binary.outputs.light.set()
        res = dict()
        res['perpend'] = QuantityLiteral(0, Millimeter)
        report('Hrubé středění')
        cter = CenterSection(self.hw, self.prototype['sections'][0]['height'], 90, 100)
        cter.exec()
        cter = CenterSection(self.hw, self.prototype['sections'][0]['height'], 90, 100)
        cter.exec()
        res['sections'] = []
        for section in sorted(self.prototype['sections'], key=lambda s: s['height']):
            report('Vzorkování průřezu h={0:.1f}'.format(section['height']))
            sampler = StdSampleSection(self.hw, section['height'], 90, 50)
            sampler.exec()
            res['sections'].append(sampler.result)
        
        
        #res['rim'] = Rim([(0,QuantityLiteral(10, Millimeter)), (180, QuantityLiteral(5,Millimeter))], self.calibrateHeight)
        #return res

        report('Hrubé měření výstřednosti hrdla')
        with self.hw.lift.unlock():
            self.hw.lift.reconfigureSpecial('HeightCam', bottleHeight=self.prototype['height'][1])
            while not self.hw.lift.done():
                hwmonitor.check()

        mn = MeasureNeckSideCam(self.hw, self.prototype, self.showImg)
        mn.exec()
        recenterThreshold = QuantityLiteral(0, Millimeter)
        #if mn.centerPolar[0].amount(Millimeter) > recenterThreshold.amount(Millimeter):
        report('Středění podle boční kamery')
        """Recenter"""
        report('Středění podle boční kamery\n   - Nastavování úhlu')
        ArrangeAngle(self.hw, angleFrom=mn.centerPolar[1], angleTo=QuantityLiteral(90, Degrees)).exec()
        report('Středění podle boční kamery\n   - Příjezd radlice')
        BringToTouchBuffered(self.hw, res['sections'][0]).exec()
        while not self.hw.lift.done():
            hwmonitor.check()
        self.hw.machine.binary.outputs.light.set()

        def cursor():
            _, c, frame, t = self.hw.height_cv.process(True)
            self.showImg(t)
            return c[0]
        report('Středění podle boční kamery\n   - Posun')
        pcSide = PushCursor(self.hw, cursor, mn.centerPolar[0], 25)
        pcSide.exec()
        
        
        
        report('Přejezd na měření výšky')
        bottleHeight = self.prototype['height'][1]
        print('bottleHeight=',bottleHeight)
        self.hw.lift.reconfigureSpecial('CenteringCam', bottleHeight=bottleHeight)
        while not self.hw.lift.done():
            hwmonitor.check()
        report('Měření výšky\n   - Vysouvání měřáku')
        self.hw.vgauge.activate()
        
        report('Měření výšky\n   - Přejezd na referenční čidlo')
        with self.hw.lift.unlock():
            self.hw.lift.reconfigureSpecial('Max')
            while not self.hw.lift.done():
                hwmonitor.check()

        time.sleep(self.hw.machine.specs['vfixed']['wait_seconds'])
        self.fixed = self.hw.machine.slider.auxSensor()
        #print('vfixed=', fixed)
        report('Měření výšky\n   - Přejezd do operační výšky')
        with self.hw.lift.unlock():
            self.hw.lift.reconfigureSpecial('CenteringCam', bottleHeight=bottleHeight)
            while not self.hw.lift.done():
                hwmonitor.check()
        
        time.sleep(0.1)
        mt = MeasureNeckTopCam(self.hw, self.prototype, self.showImg)
        mt.exec()
        topCenter = mt.center

        

        report('Středění podle horní kamery\n   - Nastavování úhlu')
        ArrangeAngle(self.hw, angleFrom=mt.centerPolar[1], angleTo=QuantityLiteral(90, Degrees)).exec()
        report('Středění podle horní kamery\n   - Příjezd radlice')
        BringToTouchBuffered(self.hw, res['sections'][0], QuantityLiteral(-(1.5 + pcSide.distance.amount(Millimeter)), Millimeter)).exec()
        while not self.hw.lift.done():
            hwmonitor.check()
        self.hw.machine.binary.outputs.light.set()
        for i in range(8):
            self.hw.neck_cv.process()
        def cursor():
            edge = None
            while edge is None:
                edge, edgePx, _, t = self.hw.neck_cv.process(True)
            self.showImg(t)
            print('edge=',edge)
            print('->cursor=',(QuantityLiteral(0, Millimeter)-edge))
            return QuantityLiteral(0, Millimeter)-(edge or QuantityLiteral(0, Millimeter))
        report('Středění podle horní kamery\n   - Posun')
        PushCursor(self.hw, cursor, mt.centerPolar[0], 25).exec()
        
        report('Vzorkování výšky')
        if Descend(self.hw).exec():
            hsampler = RadialSampler(RadialSamplerConfig(UniformAngleDirector(4), lambda: self.hw.machine.vsensor.value() + self.fixed + self.hw.lift.position(), self.rotSpeed, True), self.hw.machine, self.hw.unit_converter)
            self.hw.glue.attach()
            samples = hsampler.sample()
            res['rim'] = Rim(samples, self.calibrateHeight(self.hw.lift.position()))
            self.hw.glue.detach()
        else:
            res['rim'] = Rim([(float(i),QuantityLiteral(0,Millimeter)) for i in range(0,360,90)], QuantityLiteral(0, Millimeter))

        with self.hw.lift.unlock():
            self.hw.lift.reconfigureSpecial('CenteringCam', bottleHeight=bottleHeight)
            while not self.hw.lift.done():
                hwmonitor.check()
        
        time.sleep(0.1)
        mt = MeasureNeckTopCam(self.hw, self.prototype, self.showImg)
        mt.exec()
        topCenter = mt.center

        #with self.hw.lift.unlock():
        #    self.hw.lift.reconfigure(self.hw.lift.position() + QuantityLiteral(6, Millimeter))
                
        self.hw.vgauge.deactivate()
        
        #res += '\nNECK_CTR = %.2f mm / %.1f °' % (mn.centerPolar[0].amount(Millimeter), mn.centerPolar[1].amount(Degrees))
        #print('##############')
        #print('bottom center=',bottomCenter)
        #print('top center=',topCenter)
        
        self.hw.glue.attach()
        self.hw.machine.plate.set(RadialSampler.StartAngle)
        report('Vzorkování spodního průměru pro perpend')
        refSampler = StdSampleSection(self.hw, self.prototype['sections'][0]['height'], 90, self.rotSpeed)
        refSampler.exec()
        bottomCenter = refSampler.result.center()

        bc = tuple(x.amount(Millimeter) for x in bottomCenter)
        tc = tuple(x.amount(Millimeter) for x in topCenter)

        triA = bc[0] - tc[0]
        triB = bc[1] - tc[1]
        res['perpend'] = QuantityLiteral( math.sqrt(triA * triA + triB * triB), Millimeter)

        report('Návrat do nulové polohy')
        self.hw.lift.reconfigure(QuantityLiteral(0, Millimeter))
        while not self.hw.lift.done():
            hwmonitor.check()
        self.hw.machine.binary.outputs.light.clear()
        #print(jsonpickle.dumps(res))
        self.hideImg()
        return res


def formatMResult(res):
    fmt = ''
    fmt += 'h'.rjust(20) + str(res['rim'].max().cast(Millimeter)).rjust(30) + '\n'
    fmt += 'slant'.rjust(20) + str(res['rim'].finishSlant().cast(Millimeter)).rjust(30) + '\n'
    for section in res['sections']:
        fmt += ('diam(%s)' % int(section.height().amount(Millimeter))).rjust(20) + ('%.2f mm' % section.averageDiam().amount(Millimeter)).rjust(30) + '\n'
        fmt += ('oval(%s)' % int(section.height().amount(Millimeter))).rjust(20) + ('%.2f mm' % section.ovality().amount(Millimeter)).rjust(30) + '\n'
    return fmt
