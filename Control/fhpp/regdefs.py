from utility.named_bitfield import *


CCON = named_bitfield('CCON', (
                ('OPM', 2),
                ('LOCK', 1),
                ('reserved', 1),
                ('RESET', 1),
                ('BRAKE', 1),
                ('STOP', 1),
                ('ENABLE', 1),
            ), True)

CPOS = named_bitfield('CPOS', (
                ('CLEAR', 1),
                ('TEACH', 1),
                ('JOGN', 1),
                ('JOGP', 1),
                ('HOM', 1),
                ('START', 1),
                ('HALT', 1),
            ), True)

CDIR = named_bitfield('CDIR', (
                ('FUNC', 1),
                ('FAST', 1),
                ('XLIM', 1),
                ('CTOG', 1),
                ('CONT', 1),
                ('COM', 2),
                ('ABS', 1),
            ), True)


SCON = named_bitfield('SCON', (
                ('OPM', 2),
                ('FCT', 1),
                ('VLOAD', 1),
                ('FAULT', 1),
                ('WARN', 1),
                ('OPEN', 1),
                ('ENABLE', 1),
            ), True)

SPOS = named_bitfield('SPOS', (
                ('REF', 1),
                ('STILL', 1),
                ('FOLERR', 1),
                ('MOV', 1),
                ('TEACH', 1),
                ('MC', 1),
                ('ACK', 1),
                ('HALT', 1),
            ), True)

SDIR = named_bitfield('SDIR', (
                ('FUNC', 1),
                ('FAST', 1),
                ('XLIM', 1),
                ('REL', 1),
                ('CONT', 1),
                ('COM', 2),
                ('ABS', 1),
            ), True)