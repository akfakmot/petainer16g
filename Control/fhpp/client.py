import time
import ctypes as c
from .regdefs import *
from pyModbusTCP.client import ModbusClient
from autologging import *
from threading import *
import struct


def precheckFhppError(func):
    return func

    def wrapper(self, *args, **kwargs):
        self._readRegs()
        if self.iface.SCON.FAULT:
            raise Exception('FHPP target is in fault')

        func(self, *args, **kwargs)

    return wrapper

@traced
@logged
class Client:
    class Interface:
        def __init__(self):
            self.CCON = CCON()
            self.CPOS = CPOS()
            self.CDIR = CDIR()
            self.CPARAM1 = 0
            self.CPARAM2 = 0
            self.SCON = SCON()
            self.SPOS = SPOS()
            self.SDIR = SDIR()
            self.SPARAM1 = 0
            self.SPARAM2 = 0

    def __init__(self, host, port=502, minimumRaw=None, maximumRaw=None):
        self.minimumRaw = minimumRaw
        self.maximumRaw = maximumRaw
        self.modbus = ModbusClient(host=host, port=port, auto_open=True, debug=False, unit_id=0)
        self.iface = Client.Interface()
        self.iface.CCON.OPM = 1
        self.lock = RLock()

    def _writeRegs(self):
        data = [0] * 4
        data[0] = (int(self.iface.CCON) << 8) | int(self.iface.CPOS)
        data[1] = (int(self.iface.CDIR) << 8) | int(self.iface.CPARAM1)
        data[2] = (int(self.iface.CPARAM2) >> 16) & 0xFFFF
        data[3] = (int(self.iface.CPARAM2) ) & 0xFFFF
        if not self.modbus.write_multiple_registers(0, data):
            raise Exception('Error at writing modbus registers')
    
    def _readRegs(self):
        data = self.modbus.read_holding_registers(0, 4)
        if not data:
            raise Exception('Error at reading modbus registers')
        self.iface.SCON = SCON.fromint(data[0] >> 8)
        self.iface.SPOS = SPOS.fromint(data[0] & 0xFF)
        self.iface.SDIR = SDIR.fromint(data[1] >> 8)
        self.iface.SPARAM1 = data[1] & 0xFF
        self.iface.SPARAM2 = struct.unpack('<i', struct.pack('<I',(data[2] << 16) | data[3]))[0]

    def _positiveEdge(self, reg, bit, ackreg=None, ackbit=None):
        acknowledge = (ackreg is not None) and (ackbit is not None)

        setattr(getattr(self.iface, reg), bit, 0)
        self._writeRegs()
        if acknowledge:
            self._readRegs()
            while getattr(getattr(self.iface, ackreg), ackbit): self._readRegs()
        
        setattr(getattr(self.iface, reg), bit, 1)
        self._writeRegs()
        if acknowledge:
            self._readRegs()
            while not getattr(getattr(self.iface, ackreg), ackbit): self._readRegs()

        setattr(getattr(self.iface, reg), bit, 0)
        self._writeRegs()
        if acknowledge:
            self._readRegs()
            while getattr(getattr(self.iface, ackreg), ackbit): self._readRegs()


    def _negativeEdge(self, reg, bit, ackreg=None, ackbit=None):
        acknowledge = (ackreg is not None) and (ackbit is not None)

        setattr(getattr(self.iface, reg), bit, 1)
        self._writeRegs()
        if acknowledge:
            self._readRegs()
            while not getattr(getattr(self.iface, ackreg), ackbit): self._readRegs()
        
        setattr(getattr(self.iface, reg), bit, 0)
        self._writeRegs()
        if acknowledge:
            self._readRegs()
            while getattr(getattr(self.iface, ackreg), ackbit): self._readRegs()

        setattr(getattr(self.iface, reg), bit, 1)
        self._writeRegs()
        if acknowledge:
            self._readRegs()
            while not getattr(getattr(self.iface, ackreg), ackbit): self._readRegs()


    def standby(self):      
        with self.lock:
            self.iface.CCON.ENABLE = 1
            self.iface.CCON.STOP = 1
            self.iface.CPOS.HALT = 1
            self._writeRegs()
            self.reset()
            while not self.operationEnabled():
                pass#print(self.iface.SCON.FAULT)
    
    def operationEnabled(self):
        with self.lock:
            self._readRegs()
            return self.iface.SCON.OPEN and self.iface.SCON.ENABLE
    
    def enabled(self):
        with self.lock:
            self._readRegs()
            return self.iface.SCON.ENABLE

    @precheckFhppError
    def idle(self):
        with self.lock:
            self.iface.CCON.ENABLE = 0
            self.iface.CCON.STOP = 0
            self.iface.CPOS.HALT = 0
            self._writeRegs()
            while self.enabled() or self.operationEnabled(): pass

    @precheckFhppError
    def home(self):
        with self.lock:
            self._positiveEdge('CPOS','HOM','SPOS','ACK')

    def reset(self):
        with self.lock:
            self._readRegs()
            while self.iface.SCON.FAULT:
                print('Resetting fault')
                self.iface.CCON.RESET = 1
                self._writeRegs()
                self.iface.CCON.RESET = 0
                self._writeRegs()
                self._readRegs()
        
    def motionComplete(self):
        with self.lock:
            self._readRegs()
            return self.iface.SPOS.MC

    def homed(self):
        with self.lock:
            self._readRegs()
            return self.iface.SPOS.REF
    
    @precheckFhppError
    def set(self, setpoint2, setpoint1=100, relative=False):
        with self.lock:
            if self.minimumRaw is not None and setpoint2 < self.minimumRaw:
                setpoint2 = self.minimumRaw
            if self.maximumRaw is not None and setpoint2 > self.maximumRaw:
                setpoint2 = self.maximumRaw

            self.iface.CPARAM1 = setpoint1
            self.iface.CPARAM2 = setpoint2
            self.iface.CDIR.ABS = relative
            self._writeRegs()
            self._positiveEdge('CPOS','START','SPOS','ACK')

    def jogcancel(self):
        with self.lock:
            self.iface.CPOS.JOGN = 0
            self.iface.CPOS.JOGP = 0
            self._writeRegs()
            time.sleep(0.010)

    def jogp(self, speed=5):
        with self.lock:
            print('jogp')
            self.jogcancel()
            self.iface.CPARAM1=speed
            self.iface.CPOS.JOGN = 0
            self.iface.CPOS.JOGP = 1
            self._writeRegs()
            time.sleep(0.010)

    def jogn(self, speed=5):
        with self.lock:
            print('jogn')
            self.jogcancel()
            self.iface.CPARAM1=speed
            self.iface.CPOS.JOGN = 1
            self.iface.CPOS.JOGP = 0
            self._writeRegs()
            time.sleep(0.010)

    def halt(self):
        with self.lock:
            self.iface.CPOS.HALT = 0
            self._writeRegs()
            while self.iface.SPOS.HALT or self.iface.SPOS.MOV:
                self._readRegs()
        
            time.sleep(0.100)
            self.iface.CPOS.HALT = 1
            self._writeRegs()

            self.iface.CPOS.CLEAR = 1
            self._writeRegs()
            while not (self.iface.SPOS.HALT and self.iface.SPOS.MC):
                self._readRegs()

            self.iface.CPOS.CLEAR = 0
            self._writeRegs()

    def clear(self):
        raise NotImplementedError

    def get(self): 
        with self.lock:
            self._readRegs()
            return self.iface.SPARAM2

    def fault(self):
        with self.lock:
            self._readRegs()
            return self.iface.SCON.FAULT
