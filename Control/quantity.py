from typing import *
import quantityfunctions
from simpleeval import simple_eval
from autologging import *


class Quantity:
    def __init__(self, name:str=None):
        self.name = name

class QuantityUnit:
    def __init__(self, name:str=None, abbr:str=None):
        self.name = name
        self.abbr = abbr
    
    def convertTo(self, otherUnit:type, value):
        raise NotImplementedError()
        

class Millimeter(QuantityUnit):
    name = 'millimeter'
    abbr = 'mm'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class Micrometer(QuantityUnit):
    name = 'micrometer'
    abbr = 'um'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class Degrees(QuantityUnit):
    name = 'degrees'
    abbr = '\xb0'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class RawRotationUnit(QuantityUnit):
    name = 'raw rotation unit'
    abbr = 'rru'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class QuantityLiteral:
    pass

class QuantityEntry:
    def __init__(self, name:str=None):
        self.setName(name)

    def setName(self, name):
        self._name = name

    def name(self):
        return self._name

    def unit(self) -> QuantityUnit:
        raise NotImplementedError()
    
    def value(self):
        raise NotImplementedError()

    def amount(self, unit):
        return unitconverter.convertQuantity(self, unit).value()

    def cast(self, targetUnit):
        return QuantityLiteral(self.amount(targetUnit), targetUnit)

    def __str__(self):
        return '%.2f %s' % (self.value(), self.unit().abbr)
    
    def __repr__(self):
        return '%.2f %s' % (self.value(), self.unit().abbr)

    def __format__(self, format_spec):
        return '%s %s' % (self.value().__format__(format_spec), self.unit().abbr if self.unit() else '')

    def __eq__(self, other):
        return self.amount(self.unit()) == other.amount(self.unit())

    def __lt__(self, other):
        return self.amount(self.unit()) < other.amount(self.unit())

    def __gt__(self, other):
        return self.amount(self.unit()) > other.amount(self.unit())

    def __le__(self, other):
        return self.amount(self.unit()) <= other.amount(self.unit())

    def __ge__(self, other):
        print('ge other',other)
        return self.amount(self.unit()) >= other.amount(self.unit())

    def __add__(self, other):
        if not isinstance(other, QuantityLiteral):
            other = QuantityLiteral(float(other), self.unit())
        return QuantityLiteral( self.amount(self.unit()) + other.amount(self.unit()), self.unit() )

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        return QuantityLiteral( self.amount(self.unit()) - other.amount(self.unit()), self.unit() )

    def __mul__(self, other):
        if isinstance(other, QuantityEntry):
            return QuantityLiteral( self.amount(self.unit()) * other.amount(self.unit()), self.unit() )
        else:
            return QuantityLiteral( self.amount(self.unit()) * other, self.unit() )

    def __truediv__(self, other):
        return QuantityLiteral( self.amount(self.unit()) / float(other), self.unit() )

    
class QuantityLiteral(QuantityEntry):
    def __init__(self, value=None, unit:QuantityUnit=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setValue(value)
        self.setUnit(unit)

    def unit(self):
        return self._unit

    def value(self):
        return self._value

    def setUnit(self, unit):
        self._unit = unit
    
    def setValue(self, value):
        self._value = value


class QuantitySpec(Quantity):
    def __init__(self, min:QuantityEntry=None, nom:QuantityEntry=None, max:QuantityEntry=None, *args, **kwargs):
        self.min = min
        self.nom = nom
        self.max = max



@traced
@logged
class QuantityExpression(QuantityEntry):
    def __init__(self, expression=None, unit:QuantityUnit=None, variables:Mapping[str,QuantityEntry]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setExpression(expression)
        self.setUnit(unit)
        self.setVariables(variables)
    

    def value(self):
        return simple_eval(self._expr, names=dict((v[0],v[1].value()) for v in self.variables().items()), functions=quantityfunctions.functions)

    def unit(self):
        return self._unit

    def setUnit(self, unit):
        self._unit = unit
    
    def setExpression(self, expr):
        self._expr = expr
    
    def setVariables(self, variables):
        self._variables = variables

    def variables(self):
        return self._variables

    def expression(self):
        return self._expr





@traced
@logged
class QuantityCollection(QuantityEntry):
    def __init__(self, data:Sequence[QuantityEntry], unit:QuantityUnit=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setData(data)
        self.setUnit(unit)

    def unit(self):
        return self._unit

    def setUnit(self, unit):
        self._unit = unit

    def data(self):
        return self._data

    def setData(self, data):
        self._data = data

    def value(self):
        return [item.value() for item in self._data]
    