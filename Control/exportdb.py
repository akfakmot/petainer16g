import pymssql

def connection(server, user, password):
    #if system.get_setting('export.port', '') == '':
    #    server = system.get_setting('export.host')
    #else:
    #    server = '%s:%s' % (system.get_setting('export.host'), system.get_setting('export.port'))

    print('pymssql.connect(%s,%s,%s)' % (server, user, password))
    return pymssql.connect(server, user, password, timeout=6, login_timeout=6)


"""class Test:
    def record(self, operator, station_name, program, height, diams, perpend, slant):
        try:
            with open('/mnt/hard/record.txt', 'a'):
                f.write('%.2f;' % height)
                for i in range(0, len(diams)):
                    f.write('%.2f;' % diams[i][0])
                for i in range(0, len(diams)):
                    if diams[i][1] is not None:
                        f.write('%.2f;' % diams[i][1])
                f.write('%.2f;' % perpend)
                f.write('%.2f;' % slant)
                f.write('\n')
        except:
            pass

        server = '10.6.17.50:49776'
        user = 'taranis'
        password = 'antares'
        dbname = "LabTCELE"

        conn = pymssql.connect(server, user, password, dbname)
        cursor = conn.cursor()
        cursor.execute('USE [%s];' % dbname)
        cursor.execute(
            'INSERT INTO RECORDS([DATE], OPERATOR, HEIGHT, FINISHSLANT, PERPEND, STATION, XLSFILE, XLSLIST, XLSHEIGHT, XLSFINISHSLANT, XLSPERPEND) ' +
            ' VALUES (GETDATE(),%s,%s,%s,%s,%s,%s,%s,%s,%s,%s); SELECT SCOPE_IDENTITY();',
            (operator, height, slant, perpend, station_name, program.xlfile, program.xlsheet, program.heightcell,
             program.finishslantcell, program.perpendcell))
        record_id = int((cursor.fetchall()[0][0]))
        for i in range(0,len(diams)):
            cursor.execute('INSERT INTO RADIUSES(RECORD, HEIGHT, DIAMETER, OVALITY, XLSRADIUS, XLSOVALITY) VALUES (%s,%s,%s,%s,%s,%s);',
                           (record_id, program.radiuses[i].height, diams[i][0], diams[i][1], program.radiuses[i].valuecell, program.radiuses[i].ovalitycell))
        conn.commit()
        conn.close()


prog = typeprogram.TypeProgram()
prog.xlfile = r'C:\my_xls.xlsx'
prog.xlsheet = 'Sheet1'
prog.heightcell = 'G2'
prog.finishslantcell = None
prog.perpendcell = 'B7'
prog.radiuses = []
rad = typeprogram.Radius()
rad.height = 10
rad.valuecell = 'B1'
rad.ovalitycell = None
prog.radiuses.append(rad)

rad = typeprogram.Radius()
rad.height = 20
rad.valuecell = 'B2'
rad.ovalitycell = 'G2'
prog.radiuses.append(rad)

rad = typeprogram.Radius()
rad.height = 30
rad.valuecell = 'B3'
rad.ovalitycell = None
prog.radiuses.append(rad)

rad = typeprogram.Radius()
rad.height = 40
rad.valuecell = 'B4'
rad.ovalitycell = 'F11'
prog.radiuses.append(rad)

Test().record(333, 'PET_01', prog, 138.02, [(15.32,1.04),(3.18, None),(34.59, None),(130.89, 2.02)], 1.8, 0.3)"""