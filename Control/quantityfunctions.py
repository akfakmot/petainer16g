from quantity import *

functions = dict()

def register(f):
    functions[f.__name__] = f
    return f


@register
def avg(entry):
    return sum(entry) / float(len(entry))

functions['min'] = min
functions['max'] = max
