from festomotor import FestoMotor
import config
import threading
import logging
from autologging import logged, traced
from config import ConfigParser


class MotorConfig:
    def __init__(self, target=None, isRotary=False):
        self.target = target
        self.isRotary = isRotary


class HardwareConfiguration:
    def __init__(self):
        self.motors = []


@logged
@traced
class Hardware:
    def __init__(self, configuration):
        self.configuration = configuration

    def setupMotors(self):
        self.__log.info('Loading motors configuration')
        motorconfig = ConfigParser()
        self.__log.debug('Reading motors configuration from file %s' % self.config['files']['motors'])
        motorconfig.read(self.config['files']['motors'])
        for motorName in motorconfig.sections():
            self.__log.info('Definition of motor %s: %s', motorName, motorconfig.items(motorName))

"""
logging.info('Creating rotary drive ...')
r = FestoMotor(config.get('rotary', 'ip'), isRotary=True)
r.setZeroOffset(config.getFloat('rotary', 'zeroOffset'))
logging.info('Initializing rotary drive ...')

v = FestoMotor(config.get('vertical', 'ip'))
v.setZeroOffset(config.getFloat('vertical', 'zeroOffset'))

def init_rotary():
    logging.info('Initializing rotary drive ...')
    r.standby()
    if not r.homed(): r.home()
    r.set(0)

def init_vertical():
    logging.info('Initializing vertical drive ...')
    v.standby()
    if not v.homed(): v.home()
    v.set(0)


def init():
    logging.info('Initializing hardware ...')
    tr = threading.Thread(target=init_rotary)
    tv = threading.Thread(target=init_vertical)
    tr.start()
    tv.start()
    tr.join()
    tv.join()


def release():
    r.idle()
    v.idle()"""