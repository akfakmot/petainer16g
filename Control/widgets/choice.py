from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import *

@loadUi('ui/choicelist.ui')
class ChoiceList(QWidget):
    itemSelected = pyqtSignal(object)
    canceled = pyqtSignal()
    
    def __init__(self):
        super().__init__()

    def initUi(self):
        self.cancel.clicked.connect(self.canceled)

    def setItems(self, items, titleProjector=str):
        self.items = items
        while self.list.layout().count() != 0:
            self.list.layout().removeItem(self.list.layout().itemAt(0))
        for item in items:
            button = QPushButton(text=titleProjector(item))
            button.clicked.connect(self.itemClicked)
            button.setProperty('item', item)
            button.setMinimumSize(0,50)
            button.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
            self.list.layout().addWidget(button)
        self.list.layout().addItem(QSpacerItem(0,0,QSizePolicy.Minimum, QSizePolicy.Expanding))

    def itemClicked(self, *args, **kwargs):
        self.itemSelected.emit(self.sender().property('item'))

class Choice(QWidget):
    ActiveStyleSheet = 'background: yellow'
    InactiveStyleSheet = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setLayout(QHBoxLayout())
        self._selected = None

    def setItems(self, items, titleProjector=None):
        self._items = items
        self.titleProjector = titleProjector or str
        self.buildButtons()

    def items(self):
        return self._items

    def setSelected(self, item):
        self._selected = item
        self.refreshHighlight()

    def selected(self):
        return self._selected

    def refreshHighlight(self):
        try:
            index = self._items.index(self._selected)
        except ValueError:
            return
        for i in range(len(self.buttons)):
            self.buttons[i].setStyleSheet(self.ActiveStyleSheet if i == index else self.InactiveStyleSheet)

    def clearButtons(self):
        while self.layout().count() > 0:
            self.layout().removeItem(self.layout().itemAt(0))

    def buildButtons(self):
        self.clearButtons()

        self.buttons = []
        for item in self._items:
            button = QPushButton(text=self.titleProjector(item))
            button.clicked.connect(self.buttonClicked)
            self.buttons.append(button)
            self.layout().addWidget(button)

        self.refreshHighlight()

    def buttonClicked(self):
        self.setSelected(self._items[self.buttons.index(self.sender())])