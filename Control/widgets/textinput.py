import os
from decimal import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5 import uic

class MyFilter(QObject):
    def eventFilter(self,obj,event):
        if event.type() == QEvent.FocusOut:
            return True


class TextInput(QDialog):
    confirmed = pyqtSignal()
    cancelled = pyqtSignal()

    def __init__(self, integer=False, inputmask=None, dotsign='.'):
        super().__init__()
        uic.loadUi('ui/textinput.ui', self)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setStyleSheet('')
        #self.findChild(QPushButton,'btnLeft').clicked.connect(self.btnLeft_clicked)
        #self.findChild(QPushButton,'btnRight').clicked.connect(self.btnRight_clicked)
        self.btnOK.clicked.connect(self.confirmed)
        self.btnOK.clicked.connect(self.accept)
        self.btnBack.clicked.connect(self.cancelled)
        self.btnBack.clicked.connect(self.reject)
        self.findChild(QPushButton, 'btnBackspace').clicked.connect(self.btnBackspace_clicked)
        self.findChild(QPushButton, 'btnDel').clicked.connect(self.btnDel_clicked)
        self.frame = self.findChild(QFrame, 'frameButtons')

        self.shift = self.frame.findChild(QPushButton, 'btnShift')
        self.btnNumeric.clicked.connect(self.btnNumeric_clicked)
        self.defaultButtonStylesheet = self.shift.styleSheet()
        self.btnNumeric.setText('\u21b7')

        self.shiftToggled = False

        self.buttons = []
        for btn in self.frame.findChildren(QPushButton):
            if btn.objectName() == 'btnNumeric':
                continue
            if btn.objectName() != 'btnShift':
                btn.clicked.connect(self.appendChar)
                self.buttons.append(btn)
            else:
                btn.clicked.connect(self.shiftToggle)

        self.btnOK.raise_()
        self.btnBack.raise_()
        self.edit = self.findChild(QLineEdit, 'lineEdit_3')
        self.edit.installEventFilter(MyFilter())
        self.edit.selectionChanged.connect(self.selectionChanged)
        if inputmask is not None:
            self.edit.setInputMask(inputmask)
        self.selLen = 0
        self.limit = 10000
        self.keyboardType = 'alphabet'
        self.move(0,0)
        self.resize(800,480)

    def value(self):
        return self.edit.text()

    def setlimit(self, limit):
        self.limit = limit

    def setPrompt(self,text):
        self.findChild(QLabel,'lblPrompt_3').setText(text)
        #self.edit.setGeometry(190,self.edit.geometry().y(),self.edit.geometry().width()-190,self.edit.geometry().height())

    def setValue(self,text):
        self.edit.setText(text)

    def shiftToggle(self):
        self.shiftToggled = not self.shiftToggled
        self.setShift(self.shiftToggled)

    def setShift(self, value):
        self.shift.setStyleSheet('background-color:yellow' if value else self.defaultButtonStylesheet)
        if (value):
            for btn in self.buttons:
                btn.setText(btn.text().upper())
        else:
            for btn in self.buttons:
                btn.setText(btn.text().lower())

    def selectionChanged(self):
        self.selStart = self.edit.selectionStart()
        self.selLen = len(self.edit.selectedText())

    def btnLeft_clicked(self):
        self.edit.cursorBackward(False)
    def btnRight_clicked(self):
        self.edit.cursorForward(False)
    def btnBackspace_clicked(self):
        #if self.selLen == 0:
        self.edit.backspace()
        #else:
        #    self.cutSelection()

    def cutSelection(self):
        pass
        #self.edit.setText( + )

    def btnDel_clicked(self):
        #if self.selLen == 0:
        self.edit.setText('')
        #else:
        #    self.cutSelection()


    def appendChar(self):
        if len(self.edit.text()) < self.limit:
            char = self.sender().text()
            if (self.shiftToggled):
                char = char.upper()
            self.edit.insert(char)
            if (self.shiftToggled):
                self.shiftToggled = False
                self.setShift(False)

    def btnNumeric_clicked(self):
        if self.keyboardType == 'alphabet':
            letters = ['p', 'q', 'w', 'e', 'r', 't', 'z', 'u', 'i', 'o']
            for i in range(10):
                self.findChild(QPushButton, 'btn' + str(i)).setText(letters[i])
            self.btnSign1.setText('_')
            self.btnSign2.setText('@')
            self.btnSign3.setText(':')
            self.btnSign4.setText('-')

            diacritics = { 'btn2': 'ě', 'btn3': 'é', 'btn4': 'ř',
                           'btn5': 'ť', 'btn6': 'ž', 'btn7': 'ú',
                           'btn8': 'í', 'btnJ': 'ů', 'btn9': 'ó',
                           'btnA': 'á',  'btnS': 'š', 'btnD': 'ď',
                           'btnL': 'ľ', 'btnY': 'ý', 'btnC': 'č',
                           'btnN': 'ň', }
            for diabutton in diacritics:
                self.findChild(QPushButton, diabutton).setText(diacritics[diabutton])

            self.keyboardType = 'diacritics'
        elif self.keyboardType == 'diacritics':

            diacritics = {'btn2': 'w', 'btn3': 'e', 'btn4': 'r',
                          'btn5': 't', 'btn6': 'z', 'btn7': 'u',
                          'btn8': 'i', 'btnJ': 'j', 'btn9': 'o',
                          'btnA': 'a', 'btnS': 's', 'btnD': 'd',
                          'btnL': 'l', 'btnY': 'y', 'btnC': 'c',
                          'btnN': 'n', }

            for diabutton in diacritics:
                self.findChild(QPushButton, diabutton).setText(diacritics[diabutton])

            for i in range(10):
                self.findChild(QPushButton,'btn'+str(i)).setText(str(i))
            self.btnSign1.setText('.')
            self.btnSign2.setText('-')
            self.btnSign3.setText('*')
            self.btnSign4.setText('/')
            self.keyboardType = 'numeric'
        elif self.keyboardType == 'numeric':
            letters = ['p', 'q', 'w', 'e', 'r', 't', 'z', 'u', 'i', 'o']
            for i in range(10):
                self.findChild(QPushButton, 'btn' + str(i)).setText(letters[i])
            self.btnSign1.setText('.')
            self.btnSign2.setText(':')
            self.btnSign3.setText('\\')
            self.btnSign4.setText('-')
            self.keyboardType = 'alphabet'

        if self.shiftToggled:
            for btn in self.frame.findChildren(QPushButton):
                btn.setText(btn.text().upper())
