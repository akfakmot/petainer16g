from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from quantity import *


@loadUi('widgets/minmax.ui')
class MinMax(QWidget):
    fields = {'_min':0,'_nom':1,'_max':2}

    EmptyData = (None, None, None)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def min(self):
        return self.proc('MIN', self._min.text())
    
    def nom(self):
        return self.proc('NOM', self._nom.text())

    def max(self):
        return self.proc('MAX', self._max.text())

    def proc(self, name, text):
        if text == '': return None
        else: 
            try:
                return float(text)
            except ValueError:
                raise ValueError(name + ' musí být číslo')

    def all(self):
        return (self.min(), self.nom(), self.max())

    def setAll(self, data):
        if data is None:
            for field in self.fields: getattr(self, field).setText(None)
        else:
            for field in self.fields: 
                getattr(self, field).setText(('%.1f' % (data[self.fields[field]])).rstrip('0').rstrip('.') if data[self.fields[field]] is not None else None)

    def setFieldVisible(self, field, visible):
        fields = { 
            'min': (self.label, self._min), 
            'nom': (self.label_2, self._nom), 
            'max': (self.label_3, self._max)
            }
        for widget in fields[field]:
            widget.setVisible(visible)

    def clear(self):
        self._min.setText(None)
        self._nom.setText(None)
        self._max.setText(None)