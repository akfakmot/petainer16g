from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from widgets.textinput import *
from numericinput import *


class MyLineEdit(QLineEdit):
    clicked = pyqtSignal()
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def mousePressEvent(self, e):
        self.clicked.emit()

    def focusInEvent(self, *args, **kwargs):
        self.setStyleSheet('background:yellow')

    def focusOutEvent(self, *args, **kwargs):
        self.setStyleSheet(None)


