from quantity import *
from unitconverter import *

@traced
@logged
class QuantityInterpolation(QuantityExpression):
    def __init__(self, mapping1 :Tuple, mapping2 :Tuple, mappingUnit :QuantityUnit, *args, **kwargs):
        self.computeCoefficients(mapping1, mapping2)
        self.input = QuantityLiteral(name='x', value=0)
        self.mappingUnit = mappingUnit
        super().__init__(expression='lin * x + abs', variables=self.buildVariables(self.input), *args, **kwargs)
        
    def computeCoefficients(self, mapping1, mapping2):
        self.lin = QuantityLiteral(name='lin', value=(mapping2[1] - mapping1[1])/float(mapping2[0] - mapping1[0]))
        self.abs = QuantityLiteral(name='abs', value=mapping1[1] - mapping1[0] * self.lin.value())

    def buildVariables(self, input):
        return {'lin':self.lin, 'abs':self.abs, 'x':input}

    def setInput(self, value):
        assert isinstance(value, QuantityEntry)
        mapDomainValue = self._unit_converter.convertQuantity(value, self.mappingUnit)
        self.input = mapDomainValue
        self.setVariables(self.buildVariables(mapDomainValue))

    def unit(self):
        return self.input.unit()

    def setUnit(self, *args, **kwargs):
        pass