from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from sectioneditor import *
from collectioneditor import *
from widgets.minmax import MinMax
from quantity import *


@loadUi('ui/prototypeeditor.ui')
class PrototypeEditor(QWidget):
    def __init__(self):
        super().__init__()
    
    def initUi(self):
        editAdapter = DictionaryEditAdapter(provideEmptyData=lambda: dict(height=None, diam=MinMax.EmptyData, oval=MinMax.EmptyData))
        print('Creating CollectionEditor with editAdapter', editAdapter)
        self.sections = CollectionEditor(
            SectionEditor(), lambda: self.messageChannel(), 
            lambda x: 'h={0:.1f} mm'.format(x['height']),
            editAdapter,
            sortKey=lambda x: x['height'],
            title='Průřezy')
        self.sections.editModeChanged.connect(self.sections_editModeChanged)
        self.frameSections.layout().addWidget(self.sections)
        self.perpend.setFieldVisible('min', False)
        self.perpend.setFieldVisible('nom', False)
        self.finishslant.setFieldVisible('min', False)
        self.finishslant.setFieldVisible('nom', False)

    def sections_editModeChanged(self, editing):
        self.blockQuit(editing)

    def fill(self, data):
        self.name.setText(data and data['name'])
        self.height.setAll(data and data['height'])
        self.sections.setData(data and data['sections'])
        self.perpend.setAll(data and data['perpend'])
        self.finishslant.setAll(data and data['finishslant'])

    def read(self):
        return dict(
            name=self.name.text(), 
            height=self.height.all(), 
            sections=self.sections.data(),
            perpend=self.perpend.all(),
            finishslant=self.finishslant.all()
            )

    def clear(self):
        self.name.setText(None)
        self.height.clear()
        self.sections.setData(None)
        self.perpend.clear()
        self.finishslant.clear()