from machine import *
from festomotor import FestoMotor
from unitconverter import UnitConverter
from quantity import *
from autologging import *
import pinject


class FestoRotaryDriveConfig:
    def __init__(self, host=None, port=502, abs=0, lin=1, smoothFromRefScrewOffset=QuantityLiteral(180, Degrees)):
        self.host=host
        self.port=port
        self.abs=abs
        self.lin=lin
        self.smoothFromRefScrewOffset = smoothFromRefScrewOffset


@traced
@logged
class FestoRotaryDrive(RotaryDrive):
    @pinject.copy_args_to_internal_fields
    def __init__(self, config, unit_converter):
        self.unitConverter = unit_converter
        super().__init__()
        self._ready = False
        self.driver = FestoMotor(self._config.host, self._config.port, True)

    def setup(self):
        self.driver.standby()
        if not self.driver.homed():
            self.driver.home()
        self._ready = True

    def shut(self):
        self.driver.idle()
        self._ready = False

    def ready(self):
        return self._ready
    
    def setupFinished(self):
        return self.driver.motionComplete()

    def set(self, position, speed=100, relative=False):
        assert isinstance(position, QuantityEntry)
        raw = self._unit_converter.convertQuantity(position, RawRotationUnit).value()
        if relative:
            self.driver.set(raw * self._config.lin, speed, True)        
        else:
            self.driver.set(self._config.abs + raw * self._config.lin, speed, False)

    def setSmoothSection(self, position):
        print('set smooth section %s' % position)
        # compute raw position
        target = -90 + self.unitConverter.convertQuantity(self._config.smoothFromRefScrewOffset, Degrees).value() + self.unitConverter.convertQuantity(position, Degrees).value()
        target = self.unitConverter.convert(Degrees, RawRotationUnit, target)
        print('self.config.smoothfromref=%s' % self._config.smoothFromRefScrewOffset.value())
        print('target raw=%s'% target)
        
        # convert it to domain position
        print('self._config.lin=%s'%self._config.lin)
        target = (target - self._config.abs) / self._config.lin
        target = self.unitConverter.convert(RawRotationUnit, Degrees, target)
        print('target domain=%s'% target)
        self.set(QuantityLiteral(target, Degrees))

    def position(self):
        return QuantityLiteral(value=(self.driver.get()-self._config.abs)/self._config.lin % 100000.0, unit=RawRotationUnit)
    
    def isMoving(self):
        return not self.driver.motionComplete()

    def release(self):
        self.shut()

    def cancel(self):
        self.driver.halt()