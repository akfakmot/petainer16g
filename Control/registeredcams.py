import json


class RegisteredCams(object):
    def __init__(self, config_manager, config_id, cam_stock):
        self.config_manager = config_manager
        self.config_id = config_id
        self.cam_stock = cam_stock
        self.cam = dict()

    def camera(self, key, resolution):
        """print('****************** Registered cams request for cam', key, resolution)
        if key not in self.cam:
            self.cam = self.cam_stock.camera(self.config()[key], resolution)
        return self.cam"""
        return self.cam_stock.camera(self.config()[key], resolution)

    def config(self):
        return json.loads(self.config_manager.load(self.config_id).decode('utf8'))

        