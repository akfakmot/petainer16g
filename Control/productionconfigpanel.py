from panel import Panel
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5 import uic
from camlist import camlist
from configpanel import *
import json
import exportdb


class ProductionConfigPanel(ConfigPanel):
    """ specs = tuple(key, header, resolution) """
    def __init__(self, config_manager, config_id):
        super().__init__(config_manager, config_id)

    def buildUi(self, frame):
        frame.setLayout(QGridLayout())
        self.w = QWidget()
        uic.loadUi('ui/productionsettings.ui', self.w)
        self.w.exportTest.clicked.connect(self.exportTest)
        frame.layout().addWidget(self.w)

    def exportTest(self):
        try:
            conn = exportdb.connection(self.w.server.text(), self.w.user.text(), self.w.password.text())
            cursor = conn.cursor()
            cursor.execute('USE [%s];' % self.w.scheme.text())
            self.navigator.messageBox('Připojení se zdařilo')
        except Exception as e:
            self.navigator.messageBox('K databázi se nelze připojit: {0}'.format(str(e)))

    def fill(self, configGraph):
        if configGraph is None:
            return
        config = json.loads(configGraph.decode('utf8'))
        self.w.batch.setText(str(config['batch']))
        self.w.server.setText(config['export.server'])
        self.w.user.setText(config['export.user'])
        self.w.password.setText(config['export.password'])
        self.w.scheme.setText(config['export.scheme'])
        self.w.stationName.setText(config['stationName'])
        if config['export.all']:
            self.w.allMeasurements.setChecked(True)
        else:
            self.w.averageMeasurement.setChecked(True)
        self.w.exportOn.setChecked(config['export.on'])
            
    def dump(self):
        data = dict()
        try:
            data['batch'] = int(self.w.batch.text())
        except ValueError:
            raise Exception('Počet kusů v sérii musí být celé číslo')
        if data['batch'] <= 0 or data['batch'] >= 30:
            raise Exception('Počet kusů v sérii musí být v rozsahu 1 - 30')
        data['export.server'] = self.w.server.text()
        data['export.user'] = self.w.user.text()
        data['export.password'] = self.w.password.text()
        data['export.scheme'] = self.w.scheme.text()
        data['export.all'] = self.w.allMeasurements.isChecked()
        data['stationName'] = self.w.stationName.text()
        data['export.on'] = self.w.exportOn.isChecked()

        return json.dumps(data).encode('utf8')

    def showImage(self, camIndex):
        cam = self.cam_stock.camera(list(self.camlist.values())[camIndex]['ID_PATH'], self.resolution)
        self.display.setImage(cam.capture())