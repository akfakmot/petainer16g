import pinject
from machine import *
from time import time
import hwmonitor


class HGaugeConfig:
    def __init__(self, activationTime=None, sensorToOffTimeout=None, sensorToOnTimeout=None):
        self.activationTime = activationTime
        self.sensorToOffTimeout = sensorToOffTimeout
        self.sensorToOnTimeout = sensorToOnTimeout

class HGauge:
    def __init__(self, config, machine, timeout, lift):
        self.config = config
        self.machine = machine
        self.timeout = timeout
        self.lift = lift

    def activate(self):
        while self.machine.liftDrive.movingDir() != MovingDirection.Zero:
            hwmonitor.check()

        self.machine.binary.outputs.hgauge.set()
        def state():
            hwmonitor.check()
            return not self.machine.binary.inputs.hgauge_sensor.state()
        self.timeout.waitOrFail(state, self.config.sensorToOffTimeout, 'Horizontal gauge limit switch deassert timeout')
        self.timeout.wait(self.config.activationTime)

    def deactivate(self):
        self.machine.binary.outputs.hgauge.clear()
        def state():
            hwmonitor.check()
            return self.machine.binary.inputs.hgauge_sensor.state()
        self.timeout.waitOrFail(state, self.config.sensorToOnTimeout, 'Horizontal gauge limit switch assert timeout')
    
    def value(self):
        return self.machine.hsensor.value()