from cv2imageview import *
from configpanel import *
import time
from cvalgo import *
from qt_ui_load_decorator import loadUi
from PyQt5.QtWidgets import *
import PyQt5.uic as uic
import json


class HeightCamPanel(ConfigPanel):
    usesMachine = True
    def __init__(self, config_manager, config_id, registered_cams, cv):
        self.registered_cams = registered_cams
        self.config_manager = config_manager
        self.config_id = config_id
        self.cv = cv
        super().__init__(config_manager, config_id)
    
    def buildUi(self, widget):
        uic.loadUi('ui/heightcampanel.ui', widget)
        self.content = widget
        self.addLoop(self.loop)
        self.setLayout(QHBoxLayout())
        self.displayRaw = Cv2ImageView()
        self.displayProc = Cv2ImageView()
        self.content.frameRaw.layout().addWidget(self.displayRaw)
        self.content.frameProc.layout().addWidget(self.displayProc)
        self.content.collapse.clicked.connect(self.collapse)
        self.exposures = (4,9,19,39,78,156,312,624)
        for exp in self.exposures:
            self.content.exposure.addItem(str(exp))
        
    def collapse(self):
        newVisibility = not self.content.paramsLayout.itemAt(0).widget().isVisible()
        for i in range(self.content.paramsLayout.count()):
            self.content.paramsLayout.itemAt(i).widget().setVisible(newVisibility)
        self.content.collapse.setText('v' if newVisibility else '^')

    def fill(self, graph):
        if graph is None:
            self.content.exposure.setCurrentIndex(3)
            self.content.focus.setValue(0)
            self.content.zoom.setValue(0)
            self.content.pan.setValue(0)
            self.content.tilt.setValue(0)
        else:
            print('GRAPH',graph)
            config = json.loads(graph.decode('utf8'))
            try:
                self.content.exposure.setCurrentIndex(self.exposures.index(config['exposure']))
            except ValueError:
                self.content.exposure.setCurrentIndex(0)
        
            self.content.focus.setValue(config['focus'])
            self.content.pan.setValue(config['pan'])
            self.content.tilt.setValue(config['tilt'])
            self.content.zoom.setValue(config['zoom'])
            self.content.threshold.setValue(config['threshold'])
            self.content.roix.setValue(config['roi'][0])
            self.content.roiy.setValue(config['roi'][1])
            self.content.roiw.setValue(config['roi'][2])
            self.content.roih.setValue(config['roi'][3])
            self.content.scaleum.setValue(config['scale'][0])
            self.content.scalepx.setValue(config['scale'][1])


    def dump(self):
        return json.dumps(self.pullParams()).encode('utf8')

    def refreshParams(self):
        self.params = self.pullParams()

    def pullParams(self):
        return dict(exposure=int(self.content.exposure.currentText()),
                            focus=self.content.focus.value(),
                            zoom=self.content.zoom.value(),
                            threshold=self.content.threshold.value(),
                            roi=tuple(getattr(getattr(self.content,_input),'value')() for _input in ('roi'+var for var in ('x','y','w','h')) ),
                            scale=(self.content.scaleum.value(), self.content.scalepx.value()),
                            pan=self.content.pan.value(),
                            tilt=self.content.tilt.value())
        
    def loop(self):
        self.invoke(self.refreshParams)
        print('set1=',self.params.items())
        print('set2=',self.cv.params().items())
        match = True
        for key in self.params:
            if key not in self.cv.params():
                match = False
                break
            else:
                if self.params[key] != self.cv.params()[key]:
                    match = False
                    break

        #paramsMatched = len(set(self.params.items()) ^ set(self.cv.params().items())) == 0
        if not match:
            self.cv.setParams(self.params)
        cornerPx, self.corner, self.raw, self.proc = self.cv.process(True)
        if self.corner is not None:
            cv2.line(self.proc, (0, cornerPx[1]), (9999999, cornerPx[1]), (0,255,0), 1)
            cv2.line(self.proc, (cornerPx[0], 0), (cornerPx[0], 9999999), (0,255,0), 1)
        #edge, self.frame = neck(raw, True)
        self.invoke(self.update)
        time.sleep(0.100)

    def update(self):
        self.displayRaw.setImage(self.raw)
        self.displayProc.setImage(self.proc)



