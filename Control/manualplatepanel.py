from panel import Panel
from functools import *
from quantity import *
from threading import Lock
from PyQt5 import QtCore

from qt_ui_load_decorator import loadUi
from PyQt5.QtWidgets import *


@loadUi('ui/manualplate.ui')
class ManualPlatePanel(Panel):
    usesMachine = True
    def __init__(self, machine, title=''):
        super().__init__()
        self.machine = machine
        self.title = title
        
    def initUi(self):
        def rotate90():
            self.machine.plate.set(QuantityLiteral(90, Degrees), relative=True)
        self.rotate90.clicked.connect(rotate90)
        self.power.clicked.connect(self.power_clicked)
        self.zero.clicked.connect(self.zero_clicked)
        from PyQt5.QtCore import QTimer
        self.timer = QTimer(self)
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.refresh, QtCore.Qt.QueuedConnection)

        self.lock = Lock()    


    def zero_clicked(self):
        self.machine.plate.set(QuantityLiteral(0, Degrees))

    def power_clicked(self):
        if self.power.isChecked():
            self.machine.plate.setup()
        else:
            self.machine.plate.shut()
        self.refreshPower()
        
    def activate(self):
        super().activate()
        self.refreshPower()
        self.timer.start()

    def deactivate(self):
        self.timer.stop()

    def refresh(self):
        self.position.setText('%.2f °' % self.machine.plate.position().amount(Degrees))

    def refreshPower(self):
        self.power.setChecked(self.machine.plate.ready())
        self.controlFrame.setEnabled(self.machine.plate.ready())
