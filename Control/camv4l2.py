import cv2 as cv
import ctypes as c
import numpy as np


CID_BASE = 0x00980000 | 0x900
CID_CAMERA_BASE = 0x009a0000 | 0x900

CID_EXPOSURE_AUTO = CID_CAMERA_BASE + 1
CID_EXPOSURE_ABSOLUTE = CID_CAMERA_BASE + 2
CID_FOCUS_AUTO = CID_CAMERA_BASE + 12
CID_FOCUS_ABSOLUTE = CID_CAMERA_BASE + 10
CID_ZOOM_ABSOLUTE = CID_CAMERA_BASE + 13
CID_PAN_ABSOLUTE = CID_CAMERA_BASE + 8
CID_TILT_ABSOLUTE = CID_CAMERA_BASE + 9

EXPOSURE_AUTO = 0
EXPOSURE_MANUAL = 1
EXPOSURE_SHUTTER_PRIORITY = 2
EXPOSURE_APERTURE_PRIORITY = 3



lib = c.CDLL('../Driver/Debug/Driver.so')
lib.open_cam.argtypes = [c.c_char_p, c.c_int, c.c_int]
lib.open_cam.restype = c.c_int

class CamV4L2(object):
    def __init__(self, devPath, width, height):
        self.devPath = devPath
        self.width = width
        self.height = height

    def setup(self):
        self.hcam = lib.open_cam(self.devPath.encode('ascii'), self.width, self.height)
        if self.hcam < 0:
            raise Exception('Camera open failed')
        buf_from_mem = c.pythonapi.PyMemoryView_FromMemory
        buf_from_mem.restype = c.py_object
        buf_from_mem.argtypes = (c.c_void_p, c.c_int, c.c_int)
        cbuf = buf_from_mem(lib.get_cam_buf(self.hcam), lib.get_cam_buf_len(self.hcam), 0x100)

        self.buffer = np.ndarray((self.height,self.width,2), np.uint8, cbuf, order='C')
        self.frame = np.ndarray((self.height,self.width,3), np.uint8)

    def setupFinished(self):
        return True

    def release(self):
        lib.close_cam(self.hcam)

    def capture(self):
        lib.capture_cam(self.hcam)
        cv.cvtColor(self.buffer, cv.COLOR_YUV2BGR_YUY2, self.frame)
        return self.frame

    def setControl(self, id, value):
        lib.set_ctrl(self.hcam, id, value)

