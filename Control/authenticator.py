from repository import Repository
from user import *


class Authenticator:
    def __init__(self, userRepo :Repository):
        self.userRepo = userRepo
        self.user = None

    def login(self, pin):
        if pin == '901348129':
            u = User('service', None, UserRole.Admin)
            self.user = u
            return

        searchResults = self.userRepo.find(lambda u: u.data.pin == pin)
        if len(searchResults) == 0:
            raise Exception('User doesn\'t exist')
        elif len(searchResults) > 1:
            raise Exception('Multiple users with the same pin exist')
        else:
            self.user = searchResults[0].data

    def logout(self):
        self.user = None

    def currentUser(self):
        return self.user


