from repository import Repository
from repositoryobject import RepositoryObject
import os
from typing import Callable


class FileRepositoryConfig:
    def __init__(self, folder=None):
        self.folder = folder


class FileRepository(Repository):
    def __init__(self, serializer, config:FileRepositoryConfig, filesystem, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.serializer = serializer
        self.config = config
        self.filesystem = filesystem
        if not filesystem.exists(self.config.folder):
            filesystem.makedir(self.config.folder)

    def filepath(self, primaryKey):
        return self.filesystem.combine(self.config.folder, primaryKey)

    """Saves new or overwrites object identified by primary key"""
    def _save(self, obj:RepositoryObject):
        self.filesystem.writeall(self.filepath(obj.primaryKey), self.serializer.serialize(obj.data))

    """Gets object identified by primary key or throws exception"""
    def get(self, primaryKey) -> RepositoryObject:
        try:
            graph = self.filesystem.readall(self.filepath(primaryKey))
        except FileNotFoundError:
            raise KeyError
        return RepositoryObject(data=self.serializer.deserialize(graph), primaryKey=primaryKey, repository=self)
    
    def delete(self, primaryKey):
        assert isinstance(primaryKey, str) or isinstance(primaryKey, bytes)
        if self.filesystem.exists(self.filepath(primaryKey)):
            self.filesystem.delete(self.filepath(primaryKey))
    
    def find(self, predicate :Callable[[RepositoryObject],bool]) -> RepositoryObject:
        return list(map(lambda filename: self.get(filename), filter(lambda filename: predicate(self.get(filename)), 
                        self.filesystem.listfiles(self.config.folder))))

    def list(self):
        return self.filesystem.listfiles(self.config.folder)
            
    def all(self):
        return [self.get(pk) for pk in self.list()]
