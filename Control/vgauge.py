import pinject
from machine import *
from time import time
import hwmonitor


class VGaugeConfig:
    def __init__(self, activationTime=None, sensorToOffTimeout=None, sensorToOnTimeout=None):
        self.activationTime = activationTime
        self.sensorToOffTimeout = sensorToOffTimeout
        self.sensorToOnTimeout = sensorToOnTimeout

class VGauge:
    def __init__(self, config, machine, timeout):
        self.config = config
        self.machine = machine
        self.timeout = timeout


    def activate(self):
        if self.machine.liftDrive.movingDir() != MovingDirection.Zero:
            raise Exception('Gauge-lift hazard')

        self.machine.binary.outputs.vgauge.set()
        def state():
            hwmonitor.check()
            return not self.machine.binary.inputs.vgauge_sensor.state()
        self.timeout.waitOrFail(state, self.config.sensorToOffTimeout, 'Vertical gauge limit switch deassert timeout')
        self.timeout.wait(self.config.activationTime)

    def deactivate(self):
        self.machine.binary.outputs.vgauge.clear()
        def state():
            hwmonitor.check()
            return self.machine.binary.inputs.vgauge_sensor.state()
        self.timeout.waitOrFail(state, self.config.sensorToOnTimeout, 'Vertical gauge limit switch assert timeout')
