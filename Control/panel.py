from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class Panel(QWidget):
    def __init__(self, title=None):
        super().__init__()
        self.setStyleSheet('font: 14pt Tahoma')
        self.isCloseable = True
        self.title = ''
        self.activated = []

    def activate(self):
        for handler in self.activated:
            handler(self)
        from widgets.mylineedit import MyLineEdit
        for child in self.findChildren(MyLineEdit):
            child.clicked.connect(self.child_clicked)

    def child_clicked(self, *args, **kwargs):
        if self.sender().property('numeric'):
            from numericinput import NumericInput
            input = NumericInput(hash_char='.')
            input.set_value(self.sender().text())
            if input.exec():
                self.sender().setText(str(input.value()))
        else:
            from widgets.textinput import TextInput
            input = TextInput()
            input.setValue(self.sender().text())
            if input.exec():
                self.sender().setText(input.value())

    def deactivate(self):
        pass