from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class Session(QObject):
    changed = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.user = None

    def start(self, user):
        if user != self.user:
            self.user = user
            self.changed.emit()

    def stop(self):
        if self.user is not None:
            self.user = None
            self.changed.emit()



