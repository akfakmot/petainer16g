from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.Qt import pyqtSignal,pyqtSlot,QTimer
from PyQt5 import uic


class NumericInput(QDialog):
    def __init__(self, prompt='', title='', disable_closing=True, mask_character=None, hash_char='#', star_char='*'):
        self.disable_closing = disable_closing
        self.mask_character = mask_character
        super().__init__()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.move(0,0)
        self.resize(800,480)
        uic.loadUi('ui/numkeyboard.ui', self)

        for button in self.findChildren(QPushButton): # type: QPushButton
            tag = button.property('tag')
            if tag == 'type':
                button.clicked.connect(self.typebutton_clicked)
                if button.text() == '#':
                    button.setText(hash_char)
                elif button.text() == '*':
                    button.setText(star_char)
            elif tag == 'enter':
                button.clicked.connect(self.accepted)
                button.clicked.connect(self.accept)
            elif tag == 'escape':
                button.clicked.connect(self.rejected)
                button.clicked.connect(self.reject)
            elif tag == 'backspace':
                button.clicked.connect(self.backspace_clicked)

        self.lblPrompt.setText(prompt)
        self.value_ = ''
        self.cursor_shown = False
        #self.blink_timer = QTimer()
        #self.blink_timer.setInterval(200)
        #self.blink_timer.timeout.connect(self.blink_timeout)
        #self.blink_timer.start()
        self.refresh_display()
        self.setWindowTitle(title)

    accepted = pyqtSignal()
    rejected = pyqtSignal()

    def value(self):
        return self.value_

    def set_value(self, value):
        self.value_ = value
        self.refresh_display()

    def refresh_display(self):
        blinking_cursor = '█' if self.cursor_shown else ''
        text = self.value_ if self.mask_character is None else self.mask_character * len(self.value_)
        self.display.setText(text + blinking_cursor)

    def blink_timeout(self):
        self.cursor_shown = not self.cursor_shown
        self.refresh_display()

    def typebutton_clicked(self):
        self.value_ += self.sender().text()
        self.refresh_display()

    def backspace_clicked(self):
        if len(self.value_) > 0:
            self.value_ = self.value_[:len(self.value_)-1]
            self.refresh_display()

    def closeEvent(self, event):
        if self.disable_closing:
            event.ignore()


class NumericInputWidget(QWidget):
    def __init__(self, prompt='', title='', disable_closing=True, mask_character=None, hash_char='#', star_char='*'):
        self.disable_closing = disable_closing
        self.mask_character = mask_character
        super().__init__()
        uic.loadUi('ui/numkeyboardwidget.ui', self)

        for button in self.findChildren(QPushButton): # type: QPushButton
            tag = button.property('tag')
            if tag == 'type':
                button.clicked.connect(self.typebutton_clicked)
                if button.text() == '#':
                    button.setText(hash_char)
                elif button.text() == '*':
                    button.setText(star_char)
            elif tag == 'enter':
                button.clicked.connect(self.accepted)
            elif tag == 'escape':
                button.clicked.connect(self.rejected)
            elif tag == 'backspace':
                button.clicked.connect(self.backspace_clicked)

        self.lblPrompt.setText(prompt)
        self.value_ = ''
        self.cursor_shown = True
        self.blink_timer = QTimer()
        self.blink_timer.setInterval(200)
        self.blink_timer.timeout.connect(self.blink_timeout)
        self.blink_timer.start()
        self.refresh_display()
        self.setWindowTitle(title)

    accepted = pyqtSignal()
    rejected = pyqtSignal()

    def value(self):
        return self.value_

    def set_value(self, value):
        self.value_ = value
        self.refresh_display()

    def refresh_display(self):
        blinking_cursor = '█' if self.cursor_shown else ''
        text = self.value_ if self.mask_character is None else self.mask_character * len(self.value_)
        self.display.setText(text + blinking_cursor)

    def blink_timeout(self):
        self.cursor_shown = not self.cursor_shown
        self.refresh_display()

    def typebutton_clicked(self):
        self.value_ += self.sender().text()
        self.refresh_display()

    def backspace_clicked(self):
        if len(self.value_) > 0:
            self.value_ = self.value_[:len(self.value_)-1]
            self.refresh_display()

    def closeEvent(self, event):
        if self.disable_closing:
            event.ignore()