from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from widgets.minmax import MinMax


@loadUi('ui/sectioneditor.ui')
class SectionEditor(QWidget):
    def initUi(self):
        self.oval.setFieldVisible('min', False)
        self.oval.setFieldVisible('nom', False)

    def fill(self, data):
        self.height.setText('{0:.1f}'.format(data['height']) if data and (data['height'] is not None) else None)
        self.diam.setAll(data['diam'] if data is not None else None)
        self.oval.setAll(data['oval'] if data is not None else None)

    def read(self):
        try:
            h = float(self.height.text())
        except:
            raise Exception('Výška musí být číslo')

        return dict(height=h, diam=self.diam.all(), oval=self.oval.all())

    def clear(self):
        self.height.setText(None)
        self.diam.setAll(MinMax.EmptyData)
        self.oval.setAll(MinMax.EmptyData)
