﻿# -*- coding: utf-8 -*-
from asyncupdatepanel import AsyncUpdatePanel
from qt_ui_load_decorator import loadUi
from samplesection import *
from centersection import *
from uniformangledirector import *
from quantity import *
import pinject
from threading import *
import traceback
from measurebottle import *
from centerneck import *
import inspect
from widgets.choice import *
import hwmonitor


@loadUi('ui/operations.ui')
class OperationsPanel(AsyncUpdatePanel):
    usesMachine = True
    @pinject.copy_args_to_public_fields
    def __init__(self, hgauge, lift, glue, machine, unit_converter, height_cv, hw, prototype_repository):
        super().__init__()
        self.selectedType = None
        self.prototype_repository = prototype_repository
        self.measuring = False

    def initUi(self):
        self.sampleSection.clicked.connect(self.tryOp(self.opSampleSection))
        self.sampleSectionDiams.clicked.connect(self.tryOp(self.opSampleSectionDiams))
        self.sampleSectionEllipse.clicked.connect(self.tryOp(self.opSampleSectionEllipse))
        self.centerSection.clicked.connect(self.tryOp(self.opCenterSection))
        self.twist.clicked.connect(self.tryOp(self.opTwist))
        self.devcustom.clicked.connect(self.tryOp(self.opDevcustom))
        self.measure.clicked.connect(self.tryOp(self.opMeasure))
        self.centerNeck.clicked.connect(self.tryOp(self.opCenterNeck))
        self.selectTypeButton.clicked.connect(self.selectTypeButton_clicked)

    def typeSelected(self, type):
        self.selectedType = type
        self.typeLabel.setText(type['name'])
        self.navigator.unstack()

    def selectTypeButton_clicked(self, *args, **kwargs):
        list = ChoiceList() 
        list.setItems(sorted(map(lambda x: x.data, self.prototype_repository.all()), key=lambda x:x['name']), lambda x: x['name'])
        list.itemSelected.connect(self.typeSelected)
        list.canceled.connect(lambda: self.navigator.unstack())
        self.navigator.stack(list)

    def opCenterNeck(self):
        CenterNeck(self.hw, self.prototype()).exec()

    def tryOp(self, op):
        return lambda: self.tryOpExecute(op)

    def setResult(self, result):
        self.result.setText(result)
        self.result.verticalScrollBar().setValue(self.result.verticalScrollBar().maximum())

    def tryOpExecute(self, op):
        def threadProcedure():
            #try:
            start = time.time()
            self.invoke(lambda: self.status.setText('Operace probíhá ...'))
            result = op()
            if result is not None:
                self.invoke(lambda: self.setResult(result))
            self.invoke(lambda: self.status.setText('Operace dokončena za {0:.1f} s'.format(time.time()-start)))
            #except Exception as e:
            #    self.invoke(lambda: self.navigator.messageBox(str(e)))
        
        Thread(target=threadProcedure).start()

    def sectionSampler(self):
        return SampleSection(self.hw, 
                        SampleSectionConfig(UniformAngleDirector(360/float(self.sampleCount())), self.sectionSpeed(), True),
                        self.sectionHeight())
    
    def opDevcustom(self):
        pass
        #sampler = RadialSampler(RadialSamplerConfig(UniformAngleDirector(4), lambda: self.hw.machine.vsensor.value(), 70, True), self.hw.machine, self.hw.unit_converter)
        #samples = sampler.sample()
        #return '\n'.join('%.1f -> %.2f mm' % (s[0], s[1].amount(Millimeter)) for s in samples)
                    
        
    def opTwist(self):
        if self.sectionSpeed() > 0:
            self.glue.attach()
        self.machine.plate.set(QuantityLiteral(3*360, Degrees), abs(self.sectionSpeed()), relative=True)
        while self.machine.plate.isMoving():
            pass
        self.machine.plate.set(QuantityLiteral(-3*360, Degrees), abs(self.sectionSpeed()), relative=True)
        while self.machine.plate.isMoving():
            pass
        self.glue.detach()

    def prototype(self):
        if self.selectedType is None:
            raise Exception('Není vybrán typ lahve')
        return self.selectedType

    def opMeasure(self):
        if not self.measuring:
            self.measuring = True
            self.measure.setText('Zrušit měření')
            m = None
            with hwmonitor.HardwareAbortContext(self.hw):
                agent = MeasureBottle(self.hw, self.prototype())
                m = agent.exec(lambda msg: self.invoke(lambda: self.status.setText(msg)))
            self.measuring = False
            self.measure.setText('Změřit lahev')
            return formatMResult(m)
        else:
            hwmonitor.abort()

    def opCenterSection(self):
        centerer = CenterSection(self.hw, self.sectionHeight(), self.sampleCount(), self.sectionSpeed())
        centerer.exec()

    def opSampleSection(self):
        sampler = self.sectionSampler()
        sampler.exec()
        return '\n'.join('{0:.2f}° -> {1:.2f} mm'.format(x[0], x[1].amount(Millimeter)) for x in sampler.result.samples())

    def opSampleSectionEllipse(self):
        sampler = self.sectionSampler()
        sampler.exec()
        ctrPolar = sampler.result.centerPolar()
        return 'CTR CARTESIAN=({0:.2f},{1:.2f}) mm'.format(*tuple(x.amount(Millimeter) for x in sampler.result.center())) + \
                '\nCTR POLAR=({0:.2f} mm, {1:.2f} °)'.format(ctrPolar[0].amount(Millimeter), ctrPolar[1].amount(Degrees))

    def opSampleSectionDiams(self):
        if self.sampleCount() % 2 != 0:
            raise Exception('Počet vzorků při měření průměrů musí být sudý')
        sampler = self.sectionSampler()
        sampler.exec()
        return '\n'.join('{0:.2f}° -> {1:.2f} mm'.format(x[0], x[1].amount(Millimeter)) for x in sampler.result.diams()) + \
                '\nAVG={0:.2f} mm'.format(sampler.result.averageDiam().amount(Millimeter)) + \
                '\nOVAL={0:.2f} mm'.format(sampler.result.ovality().amount(Millimeter))

    def sectionHeight(self):
        return QuantityLiteral(float(self._sectionHeight.text()), Millimeter)

    def sampleCount(self):
        return int(self._sampleCount.text())

    def sectionSpeed(self):
        return int(self._sectionSpeed.text())