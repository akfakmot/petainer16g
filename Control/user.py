from enum import Enum


class UserRole(Enum):
    Operator = 1
    Foreman = 2
    Admin = 3

    def displayString(self):
        if self == self.Operator:
            return 'Operátor'
        elif self == self.Foreman:
            return 'Předák'
        elif self == self.Admin:
            return 'Administrátor'
        else:
            raise NotImplementedError

class User:
    def __init__(self, name :str =None, pin :str =None, role :UserRole =None):
        self.name = name
        self.pin = pin
        self.role = role

    def __eq__(self, other):
        return other and self.__dict__ == other.__dict__

