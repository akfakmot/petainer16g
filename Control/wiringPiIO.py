from wiringpi import *
from machine import *

wiringPiSetupPhys()

class WpiBinaryInput(BinaryInput):
    def __init__(self, physicalPin :int, logicInverted=False, pullup=False, description=None):
        self.pin = physicalPin
        self.pullup = pullup
        self.logicInverted = logicInverted
        self.description = description

    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)
        pinMode(self.pin, INPUT)
        pullUpDnControl(self.pin, PUD_UP if self.pullup else PUD_OFF)

    def logicValue(self, raw):
        if not self.logicInverted:
            return raw
        else:
            return not raw

    def state(self):
        return self.logicValue(digitalRead(self.pin))

    def prettyString(self):
        return 'Fyzický pin %s\n%s' % (self.pin, self.description)

class WpiBinaryOutput(BinaryOutput):
    def __init__(self, physicalPin :int, logicInverted=False, description=None):
        self.pin = physicalPin
        self.logicInverted = logicInverted
        self.description = description
    
    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)
        pinMode(self.pin, OUTPUT)
        #self.clear()

    def clear(self):
        digitalWrite(self.pin, self.rawValue(0))
        self._state = 0
    
    def set(self):
        digitalWrite(self.pin, self.rawValue(1))
        self._state = 1

    def rawValue(self, logic):
        if not self.logicInverted:
            return logic
        else:
            return not logic

    def state(self):
        return self._state

    def prettyString(self):
        return 'Fyzický pin %s\n%s' % (self.pin, self.description)