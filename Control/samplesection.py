from radialsampler import *
from section import Section
from quantity import *
import hwmonitor
import time

class SampleSectionConfig(object):
    def __init__(self, angle_director, speed, continuous):
        self.angleDirector = angle_director
        self.speed = speed
        self.continuous = continuous
        

class SampleSection(object):
    def __init__(self, hw, config, height):
        self.hw = hw
        self.hgauge = hw.hgauge
        self.lift = hw.lift
        self.glue = hw.glue
        self.machine = hw.machine
        self.unit_converter = hw.unit_converter
        self.calibrateDiamFactory = hw.calibrate_diam_factory

        self.config = config
        self.height = height
        radialSamplerConfig = RadialSamplerConfig(self.config.angleDirector, 
                                                lambda: self.hgauge.value(), 
                                                self.config.speed, self.config.continuous)
        self.radialSampler = RadialSampler(radialSamplerConfig, hw.machine, hw.unit_converter)
        self.samples = None

    def exec(self):
        self.lift.reconfigure(self.height)
        print('waiting for lift setup')
        #while not self.lift.done():
        #    hwmonitor.check()
        self.glue.attach()
        self.hw.machine.plate.set(RadialSampler.StartAngle)
        print('waiting for plate setup')
        while (not self.lift.done()) and self.hw.machine.plate.isMoving():
            hwmonitor.check()
        time.sleep(0.2)
        self.hgauge.activate()
        calibrateDiam = self.calibrateDiamFactory
        print('calibrateDiam=',calibrateDiam)
        self.result = Section(self.radialSampler.sample(), calibrateDiam, self.height)
        self.hgauge.deactivate()
        self.glue.detach()
        


