from machine import *
from target import *
import pinject


class MachineTarget(Target):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class LiftReposition(MachineTarget):
    @pinject.copy_args_to_public_fields
    def __init__(self, machine):
        super().__init__()
        

class CenterRefXsec(MachineTarget):
    @pinject.copy_args_to_public_fields
    def __init__(self, machine, lift_reposition):
        super().__init__(preconditions=[lift_reposition])