from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from navigationview import NavigationView
from observablecollections.observablelist import ObservableList


def printEvent(event):
    print('Source %s Action %s' % (event.source, event.action))


def exec(navigation):
    navigation.run()