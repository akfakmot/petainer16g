import cv2
from camv4l2 import *
from shell import shellCommand
import time
import ctypes as c
import numpy as np
from quantity import *


class Neck(object):
    def __init__(self, cam):
        self.cam = cam
        self.scale = 3000.0 / 308.0
    def init(self):
        pass
        
    def process(self, debug=False):
        frame = self.cam.capture()
        ret, t = cv2.threshold(cv2.cvtColor(frame[2:-2,2:-2,:], cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        if not debug:
            frame = None
        im2, contours, hierarchy = cv2.findContours(t,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
        
        #cv2.drawContours(t, contours, -1, (0,255,0), 2)
    
        position = 999999999
        edge = None
        for c in contours:
            box = cv2.boundingRect(c)
            contourArea = cv2.contourArea(c)
            if contourArea < 0.08 * t.shape[0] * t.shape[1]: continue
            if box[1] < position:
                position = box[1]
                edge = box[1]+box[3]
        if debug:
            t = cv2.cvtColor(t, cv2.COLOR_GRAY2BGR)
            if edge is not None:
                cv2.line(t, (0,edge), (99999999,edge), (0,255,0), 3)
            #cv2.imshow('neck', t)
            #cv2.waitKey(1)
        else:
            t = None
        return (edge and QuantityLiteral(edge * self.scale, Micrometer)), edge, frame, t



class Height(object):
    def __init__(self, cam):
        self.cam = cam
        self.cam.setControl(CID_EXPOSURE_AUTO, EXPOSURE_MANUAL)
        time.sleep(0.100)
        self.cam.setControl(CID_FOCUS_AUTO, 0)
        time.sleep(0.100)
        self._params = dict()
        self.threshold = 0
        self.clib = c.CDLL('../Driver/Debug/Driver.so')

    def init(self):
        pass

    def setParams(self, params):
        print(params)
        self.cam.setControl(CID_EXPOSURE_ABSOLUTE, params['exposure'])
        self.cam.setControl(CID_FOCUS_ABSOLUTE, params['focus'])
        self.cam.setControl(CID_ZOOM_ABSOLUTE, params['zoom'])
        self.cam.setControl(CID_ZOOM_ABSOLUTE, params['zoom'])
        self.cam.setControl(CID_PAN_ABSOLUTE, params['pan'])
        self.cam.setControl(CID_TILT_ABSOLUTE, params['tilt'])
        self.threshold = params['threshold']
        self.roi = params['roi']
        if params['scale'][1] == 0:
            self.scale = 1
        else:
            self.scale = params['scale'][0]/float(params['scale'][1])
        self._params = dict(params)
        
    def params(self):
        return self._params

    def corner(self, thresh):
        self.clib.scan(thresh.ctypes.data_as(c.c_char_p), thresh.ctypes.strides[0], thresh.shape[1], thresh.shape[0])
        point = (self.clib.scanResX(), self.clib.scanResY())
        if point[0] == -1 or point[1] == -1:
            return None
        else:
            return point

    def process(self, debug=False):
        frame = self.cam.capture()
        if self.roi[3] < 5 or self.roi[2] < 5:
            t = frame
            corner = None
        else:
            gray = cv2.cvtColor(frame[2:-2,2:-2,:], cv2.COLOR_BGR2GRAY)
            gray = gray[self.roi[1]:(self.roi[1]+self.roi[3]), self.roi[0]:(self.roi[0]+self.roi[2])]
            t = cv2.Sobel(gray, cv2.CV_8U, 1, 0, ksize=3, scale=0.6)
            ret, t = cv2.threshold(t, self.threshold, 255, cv2.THRESH_BINARY)
            cornerPx = self.corner(t)
            corner = cornerPx and (QuantityLiteral(cornerPx[0] * self.scale, Micrometer), QuantityLiteral(cornerPx[1] * self.scale, Micrometer))
            
            if debug:
                if cornerPx is not None:
                    t = cv2.cvtColor(t, cv2.COLOR_GRAY2BGR)
                    cv2.line(t, (0, cornerPx[1]), (9999999, cornerPx[1]), (0,255,0), 3)
                    cv2.line(t, (cornerPx[0], 0), (cornerPx[0], 9999999), (0,255,0), 3)
                #cv2.imshow('height', t)
                #cv2.waitKey(1)
            else:
                t = None
            if debug:
                cv2.rectangle(frame, (self.roi[0],self.roi[1]), (self.roi[0]+self.roi[2], self.roi[1]+self.roi[3]), (255,0,255), 2)
            else:
                frame = None
        return cornerPx, corner, frame, t