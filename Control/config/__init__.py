from configparser import ConfigParser


print('Initializing config module')
config = ConfigParser()
config.read('petainer.ini')

def checkKeyExists(section, key):
    if (section not in config) or (key not in config[section]):
        raise Exception('Key "%s" in section "%s" not defined' % (key, section))

def get(section, key):
    checkKeyExists(section, key)
    return config[section][key]

def getBool(section, key):
    checkKeyExists(section, key)
    return config[section].getboolean(key)

def getFloat(section, key):
    checkKeyExists(section, key)
    return float(config[section].get(key))