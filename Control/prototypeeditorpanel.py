from panel import Panel
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from qt_ui_load_decorator import loadUi
from repositorycollectioneditor import *
from prototypeeditor import *
from repositoryobject import *
from widgets.minmax import *


class PrototypeEditAdapter(object):
    def prepareEdit(self, obj):
        prep = dict()
        prep['name'] = obj['name']
        prep['height'] = self.prepareMinMax(obj['height'])
        prep['perpend'] = self.prepareMinMax(obj['perpend'])
        prep['finishslant'] = self.prepareMinMax(obj['finishslant'])
        prep['sections'] = []
        for section in obj['sections']:
            prepSection = dict()
            prepSection['height'] = section['height'] and section['height'].amount(Millimeter)
            prepSection['diam'] = self.prepareMinMax(section['diam'])
            prepSection['oval'] = self.prepareMinMax(section['oval'])
            prep['sections'].append(prepSection)
        return prep

    def prepareMinMax(self, value):
        return tuple(map(lambda x: x and x.amount(Millimeter), value))

    def finishMinMax(self, value):
        return tuple(map(lambda x: QuantityLiteral(x, Millimeter) if x is not None else None, value))

    def finishEdit(self, obj, data):
        obj['name'] = data['name']
        obj['height'] = self.finishMinMax(data['height'])
        obj['perpend'] = self.finishMinMax(data['perpend'])
        obj['finishslant'] = self.finishMinMax(data['finishslant'])
        obj['sections'] = []
        for sectionData in data['sections']:
            section = dict()
            section['height'] = QuantityLiteral(sectionData['height'], Millimeter)
            section['diam'] = self.finishMinMax(sectionData['diam'])
            section['oval'] = self.finishMinMax(sectionData['oval'])
            obj['sections'].append(section)

    def create(self):
        return dict(name=None, height=MinMax.EmptyData, sections=[], perpend=MinMax.EmptyData, finishslant=MinMax.EmptyData)


class PrototypeEditorPanel(Panel):
    def __init__(self, flash, repository):
        super().__init__()
        self.repository = repository
        self.flash = flash
        self.setLayout(QGridLayout())

        importExport = [
            ('Import vše', self._import),
            ('Export vše', self._export)]

        self.editor = RepositoryCollectionEditor(
            PrototypeEditor(), 
            lambda: self.navigator.messageBox, 
            lambda x: x.data['name'], 
            repository,  
            PrototypeEditAdapter(), 
            sortKey=lambda x: x.data['name'],
            title='Typy lahví',
            customActions=importExport)

        self.layout().addWidget(self.editor)

    def _import(self):
        self.flash.transfer_from_flash(self.navigator.messageBox, '/mnt/hard/repos/prototypes', 'Soubor k importu', '.tct', True)
        self.editor.refresh()

    def _export(self):
        self.flash.transfer_to_flash(self.navigator.messageBox, '/mnt/hard/repos/prototypes', '_types', 'Definice typů byly exportovány', '.tct', True)