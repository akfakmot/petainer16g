import ctypes as c
import time
import struct
from machine import Slider
from quantity import *
from autologging import *
import multiprocessing as mp
from functools import *


class SPISliderConfig:
    def __init__(self, spi_port, spi_speed, abs, lin, auxLin):
        self.spi_port = spi_port
        self.spi_speed = spi_speed
        self.abs = abs
        self.lin = lin
        self.auxLin = auxLin


@traced
@logged
class SPISlider(Slider):
    def __init__(self, config):
        self._config = config
        self.pool = mp.Pool(processes=1)

    def setup(self):
        self.spi = c.CDLL('../Driver/Debug/Driver.so')
        self.spi.SpiOpenPort(self._config.spi_port, self._config.spi_speed)
        self.cmdbuf = (c.c_byte * 100)()
        self.spi.SpiWriteAndRead(0, self.cmdbuf, len(self.cmdbuf))
        self.home()

    def setupFinished(self):
        self._position = 0
        return self.done()

    def transfer(self, data, awaitResponse):
        for i in range(len(data)):
            self.cmdbuf[i] = data[i]
        self.spi.SpiWriteAndRead(0, self.cmdbuf, len(data))
        if awaitResponse:
            time.sleep(0.050)
            buf1 = (c.c_byte * 2)()
            self.spi.SpiWriteAndRead(0, buf1, 2)
            cnt = buf1[1]
            cnt = min(cnt, 50)
            respbuf = (c.c_byte * 100)()
            self.spi.SpiWriteAndRead(0, respbuf, cnt)
            part1 = list(respbuf[:int(cnt/2)])
            part2 = list(respbuf[int(cnt/2):(int(cnt/2)*2)])
            #spi.SpiWriteAndRead(0, respbuf, cnt)
            #part2 = list(respbuf[:cnt])
            #print('%s --- %s' % (part1, part2))
            if not (len(part1) == len(part2) and all(part1[i] == part2[i] for i in range(len(part1)))):
                raise Exception
            return part1
        else:
            return None


    def command(self, payload, awaitResponse):
        resp = self.transfer([len(payload)] + list(payload), awaitResponse)
        return resp

    def textCommand(self, payload, awaitResponse):
        resp = self.command(payload, awaitResponse)
        respText = bytes(resp).split(b'\0')[0].decode('ascii')
        return respText

    def steps(self, count, speed=1):
        cmd = [0x30, 1 if count > 0 else 0] + list(struct.pack('<II', abs(count), speed))
        self.command(cmd, 0)

    def done(self):
        try:
            resp = self.command([0x31],True)
            respText = bytes(resp).split(b'\0')[0].decode('ascii')
            print(respText)
            if respText == 'Stopped':
                res = True
            elif respText == 'Running':
                res = False
            else:
                raise Exception
            if res:
                self._running = False
            return res
        except:
            return False
    
    def home(self):
        self._running = True
        self.steps(-2000)
        self._position = 0
        
    def set(self, depth, speed):
        depth = depth.amount(Millimeter)
        print('depth=',depth)
        if self._running:
            if not self.done():
                raise Exception('Cannot set while running')

        self._running = True
        target = int(self._config.abs + depth * self._config.lin)
        steps = target - self._position
        self.steps(steps, speed)
        self._position = target

    def auxSensor(self):
        resp = ''
        try:
            resp = self.textCommand([0x34],True)
            return QuantityLiteral( int(resp) * self._config.auxLin, Micrometer)
        except:
            self.__log.warn('SPI bus problem: resp=%s' % resp)
            raise


