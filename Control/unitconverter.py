import quantity
from quantity import *

class UnitConverter:
    def convert(self, sourceUnit:QuantityUnit, targetUnit:QuantityUnit, value):
        if sourceUnit == targetUnit:
            return value

        if sourceUnit == Millimeter and targetUnit == Micrometer:
            return value * 1000
        if sourceUnit == Micrometer and targetUnit == Millimeter:
            return value / 1000.0

        if sourceUnit == RawRotationUnit and targetUnit == Degrees:
            return value / 100000.0 * 360.0
        if sourceUnit == Degrees and targetUnit == RawRotationUnit:
            return value / 360.0 * 100000.0

        raise NotImplementedError

    def convertQuantity(self, entry:QuantityEntry, targetUnit:QuantityUnit):
        return QuantityLiteral(value=self.convert(entry.unit(), targetUnit, entry.value()),
                                    unit=targetUnit)


def convertQuantity(quantity, targetUnits):
    return UnitConverter().convertQuantity(quantity, targetUnits)


quantity.unitconverter = UnitConverter()