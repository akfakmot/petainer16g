from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class PageWidget(QWidget):

    Horizontal = 1
    Vertical = 2

    def __init__(self, parent=None, orientation=Vertical, tabs=[], feedContent=None, returnContent=None):
        super().__init__(parent)

        self.tabs = tabs
        self.feedContent = feedContent
        self.returnContent = returnContent

        self.bodyLayout = QGridLayout()

        if orientation == self.Vertical:
            self.tabLayout = QVBoxLayout()
            #self.tabLayout.addItem(QSpacerItem(20, 20, QSizePolicy.Minimum, QSizePolicy.Expanding))
            layout = QHBoxLayout()
        elif orientation == self.Horizontal:
            self.tabLayout = QHBoxLayout()
            #self.tabLayout.addItem(QSpacerItem(20, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
            layout = QVBoxLayout()

        for tab in self.tabs:
            if isinstance(tab,str):
                tabWidget = self.tabWidget(tab)
            elif isinstance(tab,tuple):
                tabWidget = self.tabWidget(tab[0])
            self.tabLayout.insertWidget(self.tabLayout.count(), tabWidget)

        layout.addLayout(self.tabLayout, 0)
        layout.addLayout(self.bodyLayout, 1)
        self.contentLayout = QGridLayout()
        self.bodyLayout.addLayout(self.contentLayout)
        
        self.menuLayout = QHBoxLayout()
        self.bodyLayout.addLayout(self.contentLayout)

        self.contentLayout = QGridLayout()
        contentFrame = QFrame()
        contentFrame.setLayout(self.contentLayout)
        self.bodyLayout.addWidget(contentFrame)
        
        self.setLayout(layout)

    def tabWidget(self, header):
        w = QPushButton(self)
        w.setText(header)
        w.clicked.connect(self.tabWidget_clicked)
        w.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        return w

    def tabWidget_clicked(self, *args, **kwargs):
        if self.feedContent is not None:
            content = self.feedContent(self.sender().text())
            if self.bodyLayout.count() != 0:
                current = self.bodyLayout.itemAt(0)
                self.bodyLayout.removeItem(current)
                current.widget().setParent(None)
                if self.returnContent is not None:
                    self.returnContent(current.widget())
            self.bodyLayout.addWidget(content)
        self.updateGeometry()


