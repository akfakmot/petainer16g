from machine import *
from gt import GT
from quantity import *


class GTSensorConfig:
    def __init__(self, host=None, channel=None, port=44818, abs=0, lin=1):
        self.host = host
        self.channel = channel
        self.port = port
        self.abs = abs
        self.lin = lin


class GTSensor(Sensor):
    def __init__(self, config):
        self._config = config

    def setup(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.gt = GT(self._config.host, self._config.port)
        self.gt.init()
    
    def value(self, channel=None):
        raw = self.gt.value(channel or self._config.channel)
        return raw and QuantityLiteral(
                    (raw * self._config.lin) + self._config.abs,
                    Micrometer)
        