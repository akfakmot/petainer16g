import os
import errno


class Filesystem:
    def exists(self, path):
        return os.path.exists(path)

    def combine(self, *args):
        return os.path.join(*args)

    def makedir(self, path):
        os.makedirs(path, exist_ok=True)

    def writeall(self, path, content :bytes):
        with open(path, 'wb') as f:
            f.write(content)

    def readall(self, path) -> bytes:
        with open(path, 'rb') as f:
            return f.read()

    def delete(self, path):
        try:
            os.remove(path)
        except OSError as e: # this would be "except OSError, e:" before Python 2.6
            if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
                raise # re-raise exception if a different error occurred
    
    def listfiles(self, folder):
        if os.path.exists(folder) and os.path.isdir(folder):
            return os.listdir(folder)
        else:
            return []
