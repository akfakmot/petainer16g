import os
from mount import *


def diskpaths():
    res = dict()
    for diskpath in os.listdir('/dev/disk/by-path'):
        path = os.path.join('/dev/disk/by-path',diskpath)
        if os.path.islink(path):
            filepath = os.path.abspath(os.path.join('/dev/disk/by-path',os.readlink(path)))
            res[filepath] = diskpath
    return res

def get_path(diskpath):
    diskpaths_ = diskpaths()
    l = list(filter(lambda x: diskpaths_[x] == diskpath, list_media_devices()))
    print('usb_flashdisk_mount -> l=%s'%l)
    if len(l) == 0:
        return None
    else:
        return l[0]

def usb_flashdisk_mount(diskpath):
    path = get_path(diskpath)
    if path is None:
        return None
    else:
        mount(path)
        #print('2 returning join(/media,%s) = %s'% (l[0],os.path.join('/media',l[0])))
        #print('l[0]=%s'%l[0])
        #print('basename=%s'%os.path.basename(l[0]))
        return '/mnt/usb'

def usb_flashdisk_unmount(path):
    unmount('/mnt/usb')

