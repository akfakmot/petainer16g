from asyncupdatepanel import *
from filerepository import *
from functools import *


class ConfigPanel(AsyncUpdatePanel):
    def __init__(self, config_manager, config_id):
        super().__init__()
        self.config_manager = config_manager
        self.setLayout(QVBoxLayout())
        self.content = QFrame()
        self.layout().addWidget(self.content, 1)
        buttonsLayout = QHBoxLayout()
        saveButton = QPushButton(text='Uložit')
        saveButton.clicked.connect(self.save)
        cancelButton = QPushButton(text='Vrátit')
        cancelButton.clicked.connect(lambda: self.fill(self.config_manager.load(config_id)))
        buttonsLayout.addWidget(cancelButton)
        buttonsLayout.addWidget(saveButton)
        self.layout().addLayout(buttonsLayout, 0)
        self.buildUi(self.content)
        self.fill(self.config_manager.load(config_id))
        self.config_id = config_id

    def buildUi(self, content):
        pass

    def fill(self, config):
        pass

    def dump(self):
        pass

    def save(self):
        try:
            self.config_manager.save(self.config_id, self.dump())
        except Exception as e:
            self.navigator.messageBox('Konfiguraci se nepodařilo uložit z následujícího důvodu:\n' + str(e))