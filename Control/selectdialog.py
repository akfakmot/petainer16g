from PyQt5.QtWidgets import *
from PyQt5.Qt import pyqtSignal,pyqtSlot,QTimer,QSize
from PyQt5.QtCore import *
from PyQt5 import uic
from qt_ui_load_decorator import *


@loadUi('ui/selectdialog2.ui')
class SelectDialog(QDialog):

    def __init__(self, prompt='', title='', disable_closing=True):
        super().__init__()
        self.prompt = prompt
        self.title = title
        self.disable_closing=disable_closing
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.move(0,0)
        self.resize(800,480)

    def initUi(self):
        self.list = self.list        # type:QListWidget
        self.btnCancel.clicked.connect(self.cancelled)
        self.lblPrompt.setText(self.prompt)

    def set_items(self, items):
        self.list.clear()
        for item in items:
            listitem = QListWidgetItem()
            listitem.setSizeHint(QSize(0, 70))
            self.list.addItem(listitem)
            btn = QPushButton()
            btn.setText(item[0])
            btn.setProperty('value', item[1])
            btn.setStyleSheet('text-align:left; padding-left:20; font:16pt;')
            if len(item) >= 3 and item[2] == 1:
                btn.setStyleSheet(btn.styleSheet() + 'background-color:lime;')
                self.list.scrollToItem(listitem)
            btn.clicked.connect(self.item_clicked)
            self.list.setItemWidget(listitem, btn)

    def item_clicked(self):
        self.item_selected.emit(self.sender().property('value'))

    item_selected = pyqtSignal(object)
    cancelled = pyqtSignal()