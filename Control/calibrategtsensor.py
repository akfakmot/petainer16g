from quantity import *


class CalibrateGtsensor:
    def __init__(self, machine, hgauge, lift, plate, unit_converter):
        self.hgauge = hgauge
        self.lift = lift
        self.plate = plate
        self.unit_converter = unit_converter
        self.machine = machine

    def prepare(self):
        self.plate.setSmoothSection(QuantityLiteral(0, Degrees))
        while self.plate.isMoving(): pass
        self.lift.reconfigureSpecial('PlateCenter')
        while not self.lift.done(): pass
        self.hgauge.activate()

    """ 'value' is horizontal sensor raw value """
    def teach(self, config, value):
        abs = self.machine.specs['plate']['radius'] - value
        config.abs = abs.amount(Micrometer)
        return config

