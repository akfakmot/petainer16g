from PyQt5.QtWidgets import QWidget


def clearLayout(layout):
    while True:
        w = layout.takeAt(0)
        if w is not None:
            del w
        else:
            break