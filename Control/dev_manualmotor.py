from festomotor import FestoMotor
import ptvsd
ptvsd.enable_attach('my_secret')
#ptvsd.wait_for_attach()

r = FestoMotor('10.6.17.12', isRotary=True)
r.setUnits('deg', 360.0 / 100000.0)
r.standby()

if not r.homed():
    r.home()
    while not r.motionComplete(): pass

# drive motor to a few turns forward to test if positioning works fine even when not in base range
r.set(2000000000+3*100000+25000, raw=True)
r.waitMotionComplete()

while True:
    line = input('Next position (q=quit, suffix r=relative) > ')
    if line == 'q': break
    
    if line.endswith('r'):
        relative = True
        line = line[:-1]
    else:
        relative = False

    try:
        pos = int(line)
    except:
        print('Invalid number')
        continue

    r.set(pos, relative=relative) 