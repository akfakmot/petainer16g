from machine import *
import json


class Lift:
    def __init__(self, machine, unit_converter, config_manager, config_id):
        self.machine = machine
        self.unitConverter = unit_converter
        self.vgauge_lock = True
        self.config_manager = config_manager
        self.config_id = config_id

    def config(self):
        return json.loads(self.config_manager.load(self.config_id).decode('utf8'))

    def reconfigure(self, position, speed=100, relative=False):
        if self.machine.binary.outputs.hgauge.state():
            raise Exception('Cannot move lift while horizontal gauge is activated')
        if self.vgauge_lock and self.machine.binary.outputs.vgauge.state():
            raise Exception('Cannot move lift while vertical gauge is activated')

        self.machine.liftDrive.set(position, speed, relative=relative)
    
    def unlock(self):
        class Unlock(object):
            def __init__(self, lift):
                self.lift = lift
            def __enter__(self):
                print('unlocking vgauge_lock')
                self.lift.vgauge_lock = False
            def __exit__(self, exc_type, exc_val, exc_tb):
                print('locking vgauge_lock')
                self.lift.vgauge_lock = True
        return Unlock(self)

    def specialPosition(self, positionName, *args, **kwargs):
        if positionName == 'PlateCenter':
            return QuantityLiteral(self.unitConverter.convertQuantity( self.machine.specs['plate']['height'] , Micrometer ).value() / 2.0, Micrometer)
        elif positionName == 'Max':
            return QuantityLiteral(100000000, Micrometer)
        elif positionName == 'Min':
            return QuantityLiteral(-100000000, Micrometer)
        elif positionName == 'CenteringCam':
            return QuantityLiteral(self.config()['centeringCam'], Millimeter) + kwargs['bottleHeight']
        elif positionName == 'HeightCam':
            return QuantityLiteral(self.config()['heightCam'], Millimeter) + kwargs['bottleHeight']
        elif positionName == 'BelowFixedVerticalSensor':
            return QuantityLiteral(287, Millimeter)
        else:
            raise Exception('Lift special position "%s" not defined' % name)

    def reconfigureSpecial(self, positionName, speed=100, *args, **kwargs):
        pos = self.specialPosition(positionName, *args, **kwargs)
        print('specialPosition=',pos)
        self.reconfigure(pos, speed)

    def position(self):
        return self.machine.liftDrive.position()

    def jogp(self, speed=5):
        self.reconfigureSpecial('Max', speed)
    
    def jogn(self, speed=5):
        self.reconfigureSpecial('Min', speed)

    def jogcancel(self):
        self.cancel()

    def cancel(self):
        self.machine.liftDrive.cancel()

    def done(self):
        return self.machine.liftDrive.movingDir() == MovingDirection.Zero
