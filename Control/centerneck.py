import time
from quantity import *
import math
from samplesection import *
from uniformangledirector import *
from stdsamplesection import *
from autologging import *
import hwmonitor


class CenterByCursor(object):
    def __init__(self, hw, cursor):
        self.hw = hw
        self.cursor = cursor

    def exec(self):
        self.hw.machine.slider.set(QuantityLiteral(20, Millimeter), 10)
        while not self.hw.machine.slider.done():
            pass

@traced
@logged
class MeasureByCursor(object):
    def __init__(self, hw, cursor, height):
        self.hw = hw
        self.cursor = cursor
        self.height = height

    def exec(self):
        self.hw.glue.attach()
        coords = []
        for i in range(4):
            self.hw.machine.plate.set(QuantityLiteral(90*i, Degrees))
            while self.hw.machine.plate.isMoving():
                hwmonitor.check()
            coords.append(self.cursor())
        halfTilt = QuantityLiteral(0.035 / 50.0 * self.height.amount(Millimeter), Millimeter)
        coords[1] = coords[1] - halfTilt
        coords[3] = coords[3] + halfTilt
        self.center = ((coords[1] - coords[3])*0.5 , (coords[2]- coords[0])*0.5)
        self.centerPolar = self.toPolar(self.center)
        self.hw.glue.detach()

    def toPolar(self, cartes):
        return (QuantityLiteral(math.sqrt((cartes[0].amount(Millimeter) * cartes[0].amount(Millimeter) + cartes[1].amount(Millimeter) * cartes[1].amount(Millimeter))), Millimeter), QuantityLiteral(math.atan2(cartes[1].amount(Millimeter), cartes[0].amount(Millimeter)) / math.pi * 180.0, Degrees))


class ArrangeAngle(object):
    def __init__(self, hw, angleFrom, angleTo):
        self.hw = hw
        self.angleFrom = angleFrom
        self.angleTo = angleTo

    def exec(self):
        self.hw.glue.attach()
        self.hw.machine.plate.set(self.angleFrom-self.angleTo)
        while self.hw.machine.plate.isMoving():
            hwmonitor.check()
        self.hw.glue.detach()


class BringToTouch(object):
    def __init__(self, hw, prototype, tolerance=QuantityLiteral(-1, Millimeter)):
        self.hw = hw
        self.tolerance = tolerance
        self.prototype = prototype

    def exec(self):
        self.hw.lift.reconfigure(self.prototype['sections'][0]['height'])
        while not self.hw.lift.done(): hwmonitor.check()
        self.hw.glue.attach()
        self.hw.machine.plate.setSync(QuantityLiteral(90, Degrees), relative=True)
        self.hw.hgauge.activate()
        touchpoint = self.hw.hgauge.value()
        print('touchpoint=',touchpoint)
        self.hw.hgauge.deactivate()
        self.hw.machine.plate.setSync(QuantityLiteral(-90, Degrees), relative=True)
        self.hw.glue.detach()
        target = self.hw.machine.specs['plate']['radius'] - touchpoint + self.tolerance
        print('slider target',target)
        self.hw.machine.slider.set(target, 1)
        while not self.hw.machine.slider.done(): hwmonitor.check()


class BringToTouchBuffered(object):
    def __init__(self, hw, section, tolerance=QuantityLiteral(-1.5, Millimeter)):
        self.hw = hw
        self.section = section
        self.tolerance = tolerance

    def exec(self):
        touchpoint = self.section.sampleAt(self.hw.machine.plate.position().amount(Degrees) + 90)
        target = self.hw.machine.specs['plate']['radius'] - touchpoint + self.tolerance
        self.hw.machine.slider.set(target, 1)
        while not self.hw.machine.slider.done():
            hwmonitor.check()



class MeasureNeckAbstract(object):
    def __init__(self, hw, cursor, prototype):
        self.hw = hw
        self.cursor = cursor
        self.prototype = prototype

    def exec(self):
        self.hw.machine.binary.outputs.light.set()
        for i in range(8):
            self.hw.neck_cv.process()
        m = MeasureByCursor(self.hw, self.cursor, self.prototype['height'][1])
        m.exec()

        """ArrangeAngle(self.hw, angleFrom=m.centerPolar[1]+QuantityLiteral(180,Degrees), angleTo=QuantityLiteral(90, Degrees)).exec()
        _, pt1, _, _ = self.hw.height_cv.process()
        ArrangeAngle(self.hw, angleFrom=m.centerPolar[1], angleTo=QuantityLiteral(90, Degrees)).exec()
        _, pt2, _, _ = self.hw.height_cv.process()
        print('pt1=',pt1[0],'pt2=',pt2[0])
        avg = (pt1[0] + pt2[0]) * 0.5 - QuantityLiteral(300, Micrometer)
        print('avg=',avg)"""

        self.hw.machine.binary.outputs.light.clear()
        self.center = m.center
        self.centerPolar = m.centerPolar


class MeasureNeckSideCam(MeasureNeckAbstract):
    def __init__(self, hw, prototype, showImg):
        def cursor():
            dist = 9999
            while dist > 4:
                coordPx, coord, f, t = hw.height_cv.process(True)
                coordPx2, _, _, _ = hw.height_cv.process(False)
                if coordPx is None:
                    continue
                if coordPx2 is None:
                    continue
                dist = abs(coordPx[0] - coordPx2[0])
            showImg(t)
            return coord[0]

        super().__init__(hw, cursor, prototype)


class MeasureNeckTopCam(MeasureNeckAbstract):
    def __init__(self, hw, prototype, showImg):
        def cursor():
            dist = 9999
            while dist > 4:
                edge, edgePx, _, t = hw.neck_cv.process(True)
                edge2, edgePx2, _, _ = hw.neck_cv.process(False)
                dist = abs(edgePx - edgePx2)
            
            showImg(t)
            return QuantityLiteral(0, Millimeter)-(edge or QuantityLiteral(0, Millimeter))

        super().__init__(hw, cursor, prototype)

class Descend(object):
    def __init__(self, hw):
        self.hw = hw

    def exec(self):
        with self.hw.lift.unlock():
            self.hw.lift.reconfigure(self.hw.lift.position() - self.hw.machine.specs['vgauge']['descend'], 5)
            while True:
                if self.hw.lift.done():
                    return False
                val = self.hw.machine.vsensor.value()
                if val and val >= QuantityLiteral(0.3, Millimeter):
                    self.hw.lift.cancel()
                    time.sleep(1)
                    return True
                hwmonitor.check()

class PushCursor(object):
    def __init__(self, hw, cursor, target, speed):
        self.hw = hw
        self.cursor = cursor
        self.target = target
        self.speed = speed
        self.distance = 0

    def exec(self):
        print('pushing target=',self.target,'...')
        self.hw.machine.slider.set(QuantityLiteral(20, Millimeter), self.speed)
        start = self.cursor() 
        print('start=',start)
        target = start + self.target
        print('target=',target)
        while True:
            cur = self.cursor() 
            print('cur=',cur)
            if cur.amount(Millimeter) >= target.amount(Millimeter):
                self.distance = cur - start
                break
            if self.hw.machine.slider.done():
                raise Exception('Nepodařilo se vystředit lahev')
            hwmonitor.check()
        self.hw.machine.slider.home()


class CenterNeck(object):
    def __init__(self, hw, prototype):
        self.hw = hw
        self.prototype = prototype

    def exec(self):
        mn = MeasureNeckSideCam(self.hw, self.prototype)
        mn.exec()

        refSampler = StdSampleSection(self.hw, self.prototype['sections'][0]['height'], 90, 70)        
        refSampler.exec()

        self.hw.lift.reconfigureSpecial('HeightCam', bottleHeight=self.prototype['height'][1])
        ArrangeAngle(self.hw, angleFrom=mn.centerPolar[1], angleTo=QuantityLiteral(90, Degrees)).exec()
        BringToTouchBuffered(self.hw, refSampler.result).exec()
        while not self.hw.lift.done(): pass
        self.hw.machine.binary.outputs.light.set()

        def cursor():
            _, c, _, _ = self.hw.height_cv.process()
            return c[0]
        PushCursor(self.hw, cursor, mn.centerPolar[0], 50).exec()
        self.hw.machine.binary.outputs.light.clear()
        
        return
        BringToTouch(self.hw, self.prototype).exec()
        time.sleep(5)
        self.hw.machine.slider.home()
        """self.hw.glue.attach()
        self.hw.machine.plate.set(QuantityLiteral(m.centerPolar[1]-90, Degrees))
        while self.hw.machine.plate.isMoving():
            pass
        self.hw.glue.detach()
        
        refSampler = StdSampleSection(self.hw, self.prototype['ref_sec_height'], 90, 70)
        refSampler.exec()"""
        
        #self.hw.machine.binary.outputs.light.clear()

    


