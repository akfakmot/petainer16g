from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from functools import *


class MenuGroup(QWidget):
    def __init__(self, title, children, dummy=False):
        super().__init__()
        self.dummy = dummy
        self.setStyleSheet('QPushButton { font: 15pt Tahoma }')
        self.setLayout(self.createLayout(QVBoxLayout))
        self.layout().addItem(QSpacerItem(5,5,QSizePolicy.Minimum, QSizePolicy.Expanding))
        frame = QFrame()
        layout = self.createLayout(QVBoxLayout)
        frame.setLayout(layout)
        self.layout().addWidget(frame)
        self.childWidgets = []
        for child in children:
            childWidget = self.childItemWidget(child)
            self.childWidgets.append(childWidget)
            layout.addWidget(childWidget)
            childWidget.hide()
        layout.addWidget(self.parentItemWidget(title))
        self.isOpen = False

    def parentItemWidget(self, title):
        btn = QPushButton()
        btn.setMinimumHeight(40)
        btn.setText(title)
        btn.clicked.connect(self.toggle)
        return btn

    def childItemWidget(self, item):
        btn = QPushButton()
        btn.setMinimumHeight(40)
        btn.setText(item[0])
        if not self.dummy:
            if item[1] is not None:
                print('connecting', item)
                btn.clicked.connect(partial(self.closeAndExecute, item[1]))
        return btn

    def closeAndExecute(self, callable):
        print('closeAndExecute')
        self.close()
        callable()

    def toggle(self):
        if self.isOpen:
            self.close()
        else:
            self.open()
    
    def open(self):
        self.setUpdatesEnabled(False)
        for w in self.childWidgets:
            w.show()
        self.setUpdatesEnabled(True)
        self.isOpen = True
        if hasattr(self, 'opened'):
            self.opened(self)
    
    def close(self):
        self.setUpdatesEnabled(False)
        for w in self.childWidgets:
            w.hide()
        self.setUpdatesEnabled(True)
        self.isOpen = False
        if hasattr(self, 'closed'):
            self.closed(self)

    def createLayout(self, cls):
        layout = cls(self)
        layout.setSpacing(2)
        layout.setContentsMargins(2,2,2,2)
        return layout


from panel import *

class MenuWidget(QWidget):
    def __init__(self, groupDefs=None):
        super().__init__() 
        
        if groupDefs is not None:
            self.setGroupDefs(groupDefs)

    def setGroupDefs(self, groupDefs):
        groupDefs = tuple(groupDefs)
        print('******* ', groupDefs)
        self.setLayout(self.createLayout(QGridLayout))
        
        """ Two overlayed layouts: content in background, menu in foreground
            Both comprise vertically expanding content area and minimum height menu area.
            Content layout has a dummy copy of menu (disabled buttons) to arrange for the right menu height """
        contentTopLayout = self.createLayout(QVBoxLayout)
        self.layout().addLayout(contentTopLayout, 0, 0)

        menuTopLayout = self.createLayout(QHBoxLayout)
        self.layout().addLayout(menuTopLayout, 0, 0)
        
        """ Frame for actual content """
        self.contentFrame = QFrame()
        self.contentFrame.setLayout(self.createLayout(QGridLayout))
        self.contentFrameWrapper = QFrame()
        self.contentFrameWrapper.setLayout(self.createLayout(QGridLayout))
        self.contentFrameWrapper.layout().addWidget(self.contentFrame,0,0)
        self.stackFrame = QFrame()
        self.stackFrame.setLayout(self.createLayout(QGridLayout))
        self.stackFrame.hide()
        self.contentFrameWrapper.layout().addWidget(self.stackFrame,0,0)
        contentTopLayout.addWidget(self.contentFrameWrapper, 1)
        
        """ Area in front of content frame (in menuTopLayout), stretched to match size of content frame """
        menuTopLayout.addItem(QSpacerItem(0, 480, QSizePolicy.Minimum, QSizePolicy.Expanding))

        self.groupDefs = groupDefs
        """ Dummy menu in background """
        contentTopLayout.addWidget(MenuGroup(*(groupDefs[0]), dummy=True), 0)
        """ Real menu in front """
        self.addMenuFrame(menuTopLayout, False)


    def createTopLayout(self):
        return self.createLayout(QVBoxLayout)

    def createLayout(self, cls):
        layout = cls(self)
        layout.setSpacing(0)
        layout.setContentsMargins(0,0,0,0)
        return layout

    def addMenuFrame(self, layout, dummy):
        self.groupWidgets = list(map(lambda d: MenuGroup(d[0], d[1]), self.groupDefs))
        for group in self.groupWidgets:
            column = self.createLayout(QVBoxLayout)
            column.addItem(QSpacerItem(0, 480, QSizePolicy.Minimum, QSizePolicy.Expanding))
            menuFrame = QFrame()
            menuFrame.setStyleSheet('background:#b7c9e8')
            menuFrame.setEnabled(not dummy)
            column.addWidget(menuFrame, 0)
            layout.addLayout(column)
            menuFrame.setLayout(self.createLayout(QVBoxLayout))
            menuFrame.layout().addWidget(group)
            group.show()
            if not dummy:
                group.opened = self.closeOtherGroups
    
    def closeOtherGroups(self, origin):
        otherGroups = filter(lambda g: g != origin, self.groupWidgets)
        for group in otherGroups:
            group.close()

    def setContent(self, content):
        #if self.contentFrame.layout().count() > 0 and content == self.contentFrame.layout().itemAt(0).widget():
        #    return
        
        print('self.contentFrame.layout().addWidget(',content)
        self.contentFrame.layout().addWidget(content)

    def clearContent(self):
        if self.contentFrame.layout().count() != 0:
            current = self.contentFrame.layout().itemAt(0)
            self.contentFrame.layout().removeItem(current)
            if current.widget() is not None:
                if hasattr(current.widget(), 'deactivate'):
                    current.widget().deactivate()
                current.widget().setParent(None)

        while self.unstack() is not None:
            pass

    def stack(self, widget):
        frame = QFrame()
        frame.setLayout(QGridLayout())
        frame.layout().addWidget(widget, 0, 0)
        self.stackFrame.layout().addWidget(frame, 0, 0)
        self.stackFrame.show()

    def unstack(self):
        if self.stackFrame.layout().count() == 0:
            return None
        else:
            widgetItem = self.stackFrame.layout().itemAt(self.stackFrame.layout().count()-1)
            self.stackFrame.layout().removeItem(widgetItem)
            if self.stackFrame.layout().count() == 0:
                self.stackFrame.hide()
            widgetItem.widget().setParent(None)
            return widgetItem.widget()

class NavigationService:
    def redirect(self, request, showPanel, *args, **kwargs):
        raise NotImplementedError



class NavigationWidget(MenuWidget):
    def __init__(self, navigation_service=None, menu_items=None):
        super().__init__()
        if menu_items is not None:
            defaultedItems = tuple((groupHeader, tuple(self.defaultToNavigation(item) for item in groupItems)) for groupHeader, groupItems in menu_items)
            print('////////// ', list(defaultedItems))
            self.setGroupDefs(defaultedItems)
        navigation_service.messageBox = self.messageBox
        self._navigation_service = navigation_service
        self._currentPanel = None

    """ if item action is not callable, consider it a navigation request """
    def defaultToNavigation(self, menuItemDef):
        print('defaultToNavigation', menuItemDef)
        if hasattr(menuItemDef[1], '__call__'):
            return menuItemDef
        elif isinstance(menuItemDef[1], tuple):
            return self.navigationMenuItem(menuItemDef[0], *menuItemDef[1])
        else:
            #print('defaultToNavigation returns', (menuItemDef[0], self.navigationMenuItem(menuItemDef[1])))
            return self.navigationMenuItem(menuItemDef[0], menuItemDef[1])

    def setMenuItems(self, menu_items):
        self.setGroupDefs(menu_items)

    def redirect(self, request, *args, **kwargs):
        """if self._currentPanel is not None:
            if hasattr(self._currentPanel, 'deactivate'):
                self._currentPanel.deactivate()
            self._currentPanel.navigator = None"""

        def showPanel(panel):
            print('showpanel called', panel)
            self.clearContent()
            
            if panel is not None:
                panel.navigator = self
                if hasattr(panel, 'activate'):
                    """if activate returns a cancel request"""
                    print('   ========= CALLING panel.activate ')
                    if panel.activate():    
                        return
            self.setContent(panel)
            self._currentPanel = panel

        self._navigation_service.redirect(self, request, showPanel, *args, **kwargs)

    def navigationMenuItem(self, header, request, *args, **kwargs):
        print('navigationMenuItem(',header,request,'returns',(header, lambda: self.redirect(request, *args, **kwargs)))
        return (header, lambda: self.redirect(request, *args, **kwargs))

    def navigationMenu(self, menuHeader, children):
        return (menuHeader, (nav.navigationMenuItem(x[0],x[1]) for x in children))
    """
    def activate(self):
        if self._currentPanel is not None:
            if hasattr(self._currentPanel, 'activate'):
                self._currentPanel.activate()
    
    def deactivate(self):
        if self._currentPanel is not None:
            if hasattr(self._currentPanel, 'deactivate'):
                self._currentPanel.deactivate()"""

    def messageBox(self, message):
        win = QDialog()
        win.resize(800,480)
        win.move(0,0)
        win.setWindowFlags(Qt.FramelessWindowHint)
        win.setStyleSheet('font:14pt Tahoma')
        win.setLayout(QVBoxLayout())
        label = QLabel(text=message)
        label.setAlignment(Qt.AlignCenter)
        label.setWordWrap(True)
        win.layout().addWidget(label, 1)
        okButton = QPushButton(text='OK')
        okButton.setMinimumSize(0, 100)
        okButton.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        okButton.clicked.connect(lambda: win.accept())
        win.layout().addWidget(okButton, 0)
        win.exec()

    def floatInput(self, prompt):
        win = QDialog(self)
        win.setLayout(QVBoxLayout())
        label = QLabel(text=prompt)
        win.layout().addWidget(label, 1)
        inputBox = QLineEdit()
        win.layout().addWidget(inputBox, 1)
        okButton = QPushButton(text='OK')
        okButton.clicked.connect(lambda: win.accept())
        win.layout().addWidget(okButton, 0)
        cancelButton = QPushButton(text='Zrušit')
        cancelButton.clicked.connect(lambda: win.reject())
        win.layout().addWidget(cancelButton, 0)
        if win.exec():
            return float(inputBox.text())
        else:
            return None

class MainWindow(QDialog):
    def __init__(self, navigation_service, menu_items, screen_size, session):
        super().__init__()
        self.setStyleSheet('. { background: #eeeeee }')
        self.navigation_service = navigation_service
        self.menu_items = menu_items

        self.userLabel = QLabel()
        self.session = session
        self.session.changed.connect(self.session_changed)

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.move(0,0)
        self.setFixedSize(*screen_size)
        self.setLayout(self.navigationWidgetLayout(QVBoxLayout))
        topBar = QFrame()
        topBar.setStyleSheet('background:#b7c9e8')
        topBar.setLayout(self.navigationWidgetLayout(QHBoxLayout))
        splitButton = QPushButton(text='Rozdělit')
        splitButton.clicked.connect(lambda:self.split(True))
        mergeButton = QPushButton(text='Sloučit')
        mergeButton.clicked.connect(lambda:self.split(False))
        topBar.layout().addWidget(splitButton)
        topBar.layout().addWidget(mergeButton)
        topBar.layout().addItem(QSpacerItem(0,0,QSizePolicy.Expanding,QSizePolicy.Minimum))
        topBar.layout().addWidget(self.userLabel)
        self.logoutButton = QPushButton(text='Odhlásit')
        self.logoutButton.clicked.connect(self.logout)
        topBar.layout().addWidget(self.logoutButton)
        self.layout().addWidget(topBar, 0)
        contentLayout = self.navigationWidgetLayout(QHBoxLayout)
        self.layout().addLayout(contentLayout,1)
        self.subwins = (self.createNavigationWidget(), self.createNavigationWidget())

        self.session_changed()

        for subwin in self.subwins:
            subwin.setMinimumWidth(800/2.0)
            contentLayout.addWidget(subwin, 1)
        
        self.subwins[1].hide()

    def createNavigationWidget(self):
        border = QFrame()
        border.setFrameShape(QFrame.Panel)
        border.setFrameStyle(QFrame.Sunken)
        border.setLineWidth(2)
        border.setStyleSheet('QFrame { background:#ddd }')
        border.setLayout(self.navigationWidgetLayout(QGridLayout))
        w = NavigationWidget(self.navigation_service, self.menu_items)
        border.layout().addWidget(w)
        return border

    def split(self, splitted):
        if splitted:
            self.subwins[1].show()
            #if self.subwins[1].layout().count() != 0:
            #    self.subwins[1].layout().itemAt(0).widget().activate()
        else:
            self.subwins[1].hide()
            self.subwins[1].layout().itemAt(0).widget().redirect('empty')
            #if self.subwins[1].layout().count() != 0:
            #    self.subwins[1].layout().itemAt(0).widget().deactivate()

    def navigationWidgetLayout(self, cls):
        layout = cls(self)
        layout.setSpacing(3)
        layout.setContentsMargins(0,0,0,0)
        return layout

    def session_changed(self):
        if self.session.user is None:
            self.userLabel.setText('Uživatel nepřihlášen')
            self.logoutButton.hide()
        else:
            self.userLabel.setText('{0} ({1})'.format(self.session.user.name, self.session.user.role.displayString()))
            self.logoutButton.show()

    def logout(self):
        self.session.stop()
